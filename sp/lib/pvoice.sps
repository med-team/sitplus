#
#	playingwithvoice
#
# Output pins
#	out (float)
#

type playingwithvoice
name _"Playing with voice"

# Create pvoice component
create play_with_voice pvoice --data-dir \"$SP_DATA_DIR$/pd\"
# pvoice provides data at a 90Hz rate, so we reduce it by a factor of 3
create freductor freductor -r 3 -a
connect pvoice in_envelope freductor in

# Adjust sensitivity & range
create fdiv fdiv -v 100
connect freductor out fdiv a

# Threshold
import around_zero_threshold.sps
create around_zero_threshold thres -lsld _"Sound threshold" -lchk _"Sound detected" -max 1 -v 0.15
connect fdiv result thres in

# Sensitivity adjust
create fmul fmul_sens
connect thres out fmul_sens a
create widget_slider sldsens --min 0 --max 5 -v 1 -l _"Sound sensitivity" --log
connect sldsens value fmul_sens b

export_opin fmul_sens result out

begin_gui_layout
	layout_begin vbox
		component pvoice
		component sldsens
		layout_begin collapsible _"Advanced"
			component thres
		layout_end
	layout_end
end_gui_layout