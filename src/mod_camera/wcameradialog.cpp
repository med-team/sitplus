/////////////////////////////////////////////////////////////////////////////
// Name:        cameradialog2.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     27/01/2011 10:52:27
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "cameradialog2.h"

////@begin XPM images
////@end XPM images


/*!
 * CameraDialog type definition
 */

IMPLEMENT_DYNAMIC_CLASS( CameraDialog, wxPanel )


/*!
 * CameraDialog event table definition
 */

BEGIN_EVENT_TABLE( CameraDialog, wxPanel )

////@begin CameraDialog event table entries
    EVT_CLOSE( CameraDialog::OnCloseWindow )

////@end CameraDialog event table entries

END_EVENT_TABLE()


/*!
 * CameraDialog constructors
 */

CameraDialog::CameraDialog()
{
    Init();
}

CameraDialog::CameraDialog( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
    Init();
    Create(parent, id, pos, size, style);
}


/*!
 * CameraDialog creator
 */

bool CameraDialog::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
////@begin CameraDialog creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    Centre();
////@end CameraDialog creation
    return true;
}


/*!
 * CameraDialog destructor
 */

CameraDialog::~CameraDialog()
{
////@begin CameraDialog destruction
////@end CameraDialog destruction
}


/*!
 * Member initialisation
 */

void CameraDialog::Init()
{
////@begin CameraDialog member initialisation
    m_camWindow = NULL;
////@end CameraDialog member initialisation
}


/*!
 * Control creation for CameraDialog
 */

void CameraDialog::CreateControls()
{    
////@begin CameraDialog content construction
    CameraDialog* itemPanel1 = this;

    m_camWindow = new CCamWindow2;
    m_camWindow->Create( itemPanel1, ID_PANEL3, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER|wxTAB_TRAVERSAL );

////@end CameraDialog content construction
}


/*!
 * wxEVT_CLOSE_WINDOW event handler for ID_CAMERADIALOG
 */

void CameraDialog::OnCloseWindow( wxCloseEvent& event )
{
////@begin wxEVT_CLOSE_WINDOW event handler for ID_CAMERADIALOG in CameraDialog.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_CLOSE_WINDOW event handler for ID_CAMERADIALOG in CameraDialog. 
}


/*!
 * Should we show tooltips?
 */

bool CameraDialog::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap CameraDialog::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin CameraDialog bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end CameraDialog bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon CameraDialog::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CameraDialog icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end CameraDialog icon retrieval
}
