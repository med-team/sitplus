type virtualinstrument
name "Virtual Instrument"

# Camera source
create camera_grabber grabber

# Motion tracker
import lib/camera_oflow_motion.sps
create camera_oflow_motion tracker
connect grabber image tracker image

# MIDI player
import lib/motion_midi_player.sps
create motion_midi_player player
connect tracker motion player motion

# Collage
import lib/motion_collage.sps
create motion_collage motion_collage
create send_main_async to_main

#connect tracker motion to_main in
create fcast fcast
create fmul fmul_gate 
connect player notes_played fcast in
connect fcast out fmul_gate b
connect tracker motion fmul_gate a
connect fmul_gate result to_main in

#connect fcast out to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox _"Camera Tracker"
			component tracker
		layout_end
		layout_begin vbox _"MIDI Sound"
			component player
		layout_end
		layout_begin vbox _"Graphics"
			component motion_collage			
		layout_end
	layout_end
end_gui_layout