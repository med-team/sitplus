/////////////////////////////////////////////////////////////////////////////
// Name:       	playwithvoice.cpp
// Purpose:  
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Modified by: 
// Created:     
// Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes
#include "playwithvoice.h"
#include <wx/bitmap.h>

////@begin XPM images
#include "bitmaps/disable_icon.xpm"
////@end XPM images

#include <iostream>

using namespace spcore;
using namespace std;

namespace mod_puredata {

PlayWithVoiceComponent::PlayWithVoiceComponent (const char * name, int argc, const char * argv[])
: CComponentAdapter(name, argc, argv)
, m_robot (false)
, m_phone (false)
, m_howlingReduction (true)
, m_running(false)
, m_panel(NULL)
, m_oscOut(50001)
, m_oscIn(50002, this)
, m_micInputControl (100,0,500,0)
, m_outputControl (25,0,100,0)
, m_reverb (50,0,100,0)
, m_chorus (0,0,4,0)
, m_pitchShift (0, -1500, 1500, 0)
, m_distorsion (0, 0, 100, 0)
, m_echoDelay (0, 0, 1000, 0)
, m_echoPitchShift (0, -1500, 1500,0)
{
	// Parse arguments
	for (int iarg= 0; iarg< argc; ++iarg) {
		if (strcmp(argv[iarg], "--data-dir")== 0) {
			if (++iarg< argc) {
				m_patchPath= argv[iarg];
				m_patchPath+= "/";				
			}
			else
				throw std::runtime_error("play_with_voice: not enough arguments for --data-dir");
		}
		else {
			string msg("play_with_voice: unexpected argument ");
			msg+= argv[iarg];
			throw std::runtime_error(msg);
		}
	}
	m_patchPath+= "pvoice.pd";

	m_oPinInEnvelope= CTypeFloat::CreateOutputPin("in_envelope");
	assert (m_oPinInEnvelope.get());
	RegisterOutputPin(*m_oPinInEnvelope);

	m_oPinOutEnvelope= CTypeFloat::CreateOutputPin("out_envelope");
	assert (m_oPinOutEnvelope.get());
	RegisterOutputPin(*m_oPinOutEnvelope);

	m_inEnvelope= CTypeFloat::CreateInstance();
	m_outEnvelope= CTypeFloat::CreateInstance();
}

PlayWithVoiceComponent::~PlayWithVoiceComponent()
{
	assert (wxIsMainThread());
	Finish();
	if (m_panel) {
		m_panel->SetComponent(NULL);
		m_panel->Close();
		m_panel= NULL;
	}
}

void PlayWithVoiceComponent::OnPanelDestroyed ()
{
	assert (wxIsMainThread());
	m_panel= NULL;	
}

wxWindow* PlayWithVoiceComponent::GetGUI(wxWindow * parent)
{
	assert (wxIsMainThread());	
	if (m_panel) {
		// Already created
		assert (false);
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, "panel alredy open", "puredata_config");
		return NULL;
	}
	else {
		m_panel= new PlayWithVoicePanel();
		m_panel->SetComponent(this);
		m_panel->Create(parent);
	}
	return m_panel;
}

int PlayWithVoiceComponent::DoStart()
{
	if (m_running) return 0;

	try {
		// Register this patch (which starts pd)
		PureDataController::getInstance()->RegisterPatch(this);

		// Open outbound socket
		m_oscOut.Open();

		// Open inbound socket and starts listening to it
		m_oscIn.Open(); 

		m_running= true;

		// Send current values to patch
		setMicInputControl(m_micInputControl.getValue());
		setOutputControl( m_outputControl.getValue());
		setReverb(m_reverb.getValue());
		setChorus(m_chorus.getValue());
		setPitchShift(m_pitchShift.getValue());
		setDistorsion(m_distorsion.getValue());
		setRobot (m_robot);
		setPhone (m_phone);
		setHowlingReduction (m_howlingReduction);
		setEchoDelay( m_echoDelay.getValue());
		setEchoPitchShift(m_echoPitchShift.getValue());
	}
	catch (std::exception& e) {
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, e.what(),  GetTypeName());
		try { PureDataController::getInstance()->UnregisterPatch(this);	} catch(...) {}
		return -1;
	}

	return 0;
}

void PlayWithVoiceComponent::DoStop()
{
	if (!m_running) return;

	// Close outbound socket
	m_oscOut.Close();

	// Stop listening messages
	m_oscIn.Close();
	
	try {
		PureDataController::getInstance()->UnregisterPatch(this);
		m_running= false;
	}
	catch (std::exception& e) {
		assert (false);
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, e.what(),  GetTypeName());		
	}
}

void PlayWithVoiceComponent::NotifyStatus (PDStatus)
{
	/*
	if (m_panel && e== IPdPatch::PD_STOPED) {		
		m_panel->NotifyComponentUpdate();
	}*/	
}
	
const char* PlayWithVoiceComponent::GetPatchFileName() const 
{
	return m_patchPath.c_str();		
}

void PlayWithVoiceComponent::SendSimpleMessageManaged (const char *name, float v)
{
	try {
		if (m_running)
			m_oscOut.SendSimpleMessage (name, v);
	}
	catch (std::exception& e) {		
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, e.what(),  GetTypeName());		
	}
}
	
void PlayWithVoiceComponent::ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& )
{
	try {
		if(strcmp( m.AddressPattern(), "/pvoice" )== 0 ) {
			float micInput, micOutput, pitch;
			Linear2ExpMapping converter(0, 0, 120.0f, 120.0f, 1);
			bool attack;

			osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
			if (arg->IsInt32())
				micInput= (float) (arg++)->AsInt32();
			else
				micInput= (arg++)->AsFloat();
			m_inEnvelope->setValue(converter.ToExp(micInput));

			if (arg->IsInt32())
				micOutput= (float) (arg++)->AsInt32();
			else
				micOutput= (arg++)->AsFloat();
			m_outEnvelope->setValue(converter.ToExp(micOutput));
			
			if (arg->IsInt32())
				pitch = (float) (arg++)->AsInt32();
			else
				pitch = (arg++)->AsFloat();
			     
			attack= ((arg++)->AsInt32()!= 0);

			m_panel->NotifyComponentUpdate();

			// Send result to output pins
			m_oPinInEnvelope->Send(m_inEnvelope);
			m_oPinOutEnvelope->Send(m_outEnvelope);

			/*
			Sample code on how to convert in input envelope (in dB) back to 
			a linear scale and remove the modification introduced by the input
			volumen adjustment

			float linearEnvelope= 0.0f;
			if (m_micInputControl.getValue()> 5) {
				float exp= micInput - 100;
				linearEnvelope= powf(10, exp);
				// Scale taking into account input volume adjustment
				linearEnvelope= linearEnvelope / (float) m_micInputControl.getValue();
			}
			std::cout << "input" << micInput << "\t" << m_inEnvelope->getValue() << "\t" << linearEnvelope << std::endl;
			*/

			//std::cout << "input" << micInput << "\t" << m_inEnvelope->getValue() << std::endl;
			//std::cout << "out" << micOutput << "\t" <<  m_outEnvelope->getValue() << std::endl;
        }
		else {
			string msg= string("Unknown message received") + m.AddressPattern();
			getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_DEBUG, msg.c_str(), GetTypeName());
		}		
	}
	catch ( osc::Exception& e ) {
        // any parsing errors such as unexpected argument types, or 
        // missing arguments get thrown as exceptions.
		string msg= string("Error while parsing message") + m.AddressPattern() + ":" + e.what();
		getSpCoreRuntime()->LogMessage(ICoreRuntime::LOG_ERROR, msg.c_str(), GetTypeName());
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

DECLARE_LOCAL_EVENT_TYPE(wxEVT_COMPONENT_PVOICE_UPDATE, -1)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_COMPONENT_PVOICE_UPDATE)

/*!
 * PlayWithVoicePanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( PlayWithVoicePanel, wxPanel )


/*!
 * PlayWithVoicePanel event table definition
 */

BEGIN_EVENT_TABLE( PlayWithVoicePanel, wxPanel )

////@begin PlayWithVoicePanel event table entries
#if defined(__WXOS2__)
    EVT_BUTTON( ID_BITMAPBUTTON_MICINPUT, PlayWithVoicePanel::OnBitmapbuttonMicinputClick )
#endif

#if defined(__WXOS2__)
    EVT_SLIDER( ID_SLIDER_MIC_INPUT, PlayWithVoicePanel::OnSliderMicInputUpdated )
#endif

    EVT_BUTTON( ID_BITMAPBUTTON_SOUNDOUTPUT, PlayWithVoicePanel::OnBitmapbuttonSoundoutputClick )

    EVT_SLIDER( ID_SLIDER_OUTPUT, PlayWithVoicePanel::OnSliderOutputUpdated )

    EVT_CHECKBOX( ID_CHECKBOX_MICBOOST, PlayWithVoicePanel::OnCheckboxMicboostClick )

    EVT_BUTTON( ID_BITMAPBUTTON_REVERB, PlayWithVoicePanel::OnBitmapbuttonReverbClick )

    EVT_SLIDER( ID_SLIDER_REVERB, PlayWithVoicePanel::OnSliderReverbUpdated )

    EVT_BUTTON( ID_BITMAPBUTTON_CHORUS, PlayWithVoicePanel::OnBitmapbuttonChorusClick )

    EVT_SLIDER( ID_SLIDER_CHORUS, PlayWithVoicePanel::OnSliderChorusUpdated )

    EVT_BUTTON( ID_BITMAPBUTTON_PITCHSHIFT, PlayWithVoicePanel::OnBitmapbuttonPitchshiftClick )

    EVT_SLIDER( ID_SLIDER_PITCHSHIFT, PlayWithVoicePanel::OnSliderPitchshiftUpdated )

    EVT_BUTTON( ID_BITMAPBUTTON_DISTORSION, PlayWithVoicePanel::OnBitmapbuttonDistorsionClick )

    EVT_SLIDER( ID_SLIDER_DISTORSION, PlayWithVoicePanel::OnSliderDistorsionUpdated )

#if defined(__WXOS2__)
    EVT_CHECKBOX( ID_CHECKBOX_ROBOT, PlayWithVoicePanel::OnCheckboxRobotClick )
#endif

    EVT_CHECKBOX( ID_CHECKBOX_HOWLINGREDUCTION, PlayWithVoicePanel::OnCheckboxHowlingreductionClick )

#if defined(__WXOS2__)
    EVT_CHECKBOX( ID_CHECKBOX_PHONE, PlayWithVoicePanel::OnCheckboxPhoneClick )
#endif

    EVT_BUTTON( ID_BITMAPBUTTON_ECHODELAY, PlayWithVoicePanel::OnBitmapbuttonEchodelayClick )

    EVT_SLIDER( ID_SLIDER_ECHODELAY, PlayWithVoicePanel::OnSliderEchodelayUpdated )

    EVT_BUTTON( ID_BITMAPBUTTON_ECHOPITCHSHIFT, PlayWithVoicePanel::OnBitmapbuttonEchopitchshiftClick )

    EVT_SLIDER( ID_SLIDER_ECHOPITCHSHIFT, PlayWithVoicePanel::OnSliderEchopitchshiftUpdated )

////@end PlayWithVoicePanel event table entries

	EVT_COMMAND(wxID_ANY, wxEVT_COMPONENT_PVOICE_UPDATE, PlayWithVoicePanel::OnComponentUpdated)

END_EVENT_TABLE()

/*!
 * PlayWithVoicePanel constructors
 */

PlayWithVoicePanel::PlayWithVoicePanel()  
{
    Init();
}

PlayWithVoicePanel::PlayWithVoicePanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
{
    Init();
    Create(parent, id, pos, size, style, name);
}


/*!
 * PlayWithVoiceDialog creator
 */
    
bool PlayWithVoicePanel::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
////@begin PlayWithVoicePanel creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
////@end PlayWithVoicePanel creation
    return true;
}


/*!
 * PlayWithVoicePanel destructor
 */

PlayWithVoicePanel::~PlayWithVoicePanel()
{
	if (m_component!= NULL) {
		m_component->OnPanelDestroyed();
		m_component= NULL;
	}
////@begin PlayWithVoicePanel destruction
////@end PlayWithVoicePanel destruction
}


/*!
 * Member initialisation
 */

void PlayWithVoicePanel::Init()
{
////@begin PlayWithVoicePanel member initialisation
#if defined(__WXOS2__)
    m_btnMicInput = NULL;
#endif
#if defined(__WXOS2__)
    m_sldMicInput = NULL;
#endif
#if defined(__WXOS2__)
    m_gaugeMicInput = NULL;
#endif
    m_btnSoundOutput = NULL;
    m_sldOutput = NULL;
    m_gaugeOutput = NULL;
    m_btnReverb = NULL;
    m_sldReverb = NULL;
    m_txtReverb = NULL;
    m_sldChorus = NULL;
    m_txtChorus = NULL;
    m_sldPitchShift = NULL;
    m_txtPitchShift = NULL;
    m_sldDistorsion = NULL;
    m_txtDistorsion = NULL;
#if defined(__WXOS2__)
    m_chkRobot = NULL;
#endif
    m_chkHowlingReduction = NULL;
#if defined(__WXOS2__)
    m_chkPhone = NULL;
#endif
    m_sldEchoDelay = NULL;
    m_txtEchoDelay = NULL;
    m_sldEchoPitchShift = NULL;
    m_txtEchoPitchShift = NULL;
////@end PlayWithVoicePanel member initialisation
	m_component= NULL;	
}


/*!
 * Control creation for PlayWithVoiceDialog
 */

void PlayWithVoicePanel::CreateControls()
{    
////@begin PlayWithVoicePanel content construction
    PlayWithVoicePanel* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemPanel1->SetSizer(itemBoxSizer2);

#if defined(__WXOS2__)
    wxStaticBox* itemStaticBoxSizer3Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Mic input"));
    wxStaticBoxSizer* itemStaticBoxSizer3 = new wxStaticBoxSizer(itemStaticBoxSizer3Static, wxVERTICAL);
    itemBoxSizer2->Add(itemStaticBoxSizer3, 0, wxGROW|wxALL, 5);

    wxGridBagSizer* itemGridBagSizer4 = new wxGridBagSizer(0, 0);
    itemGridBagSizer4->SetEmptyCellSize(wxSize(10, 20));
    itemStaticBoxSizer3->Add(itemGridBagSizer4, 0, wxGROW|wxALL, 5);

    m_btnMicInput = new wxBitmapButton;
    m_btnMicInput->Create( itemPanel1, ID_BITMAPBUTTON_MICINPUT, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer4->Add(m_btnMicInput, wxGBPosition(0, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldMicInput = new wxSlider;
    m_sldMicInput->Create( itemPanel1, ID_SLIDER_MIC_INPUT, 0, 0, 200, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
    itemGridBagSizer4->Add(m_sldMicInput, wxGBPosition(0, 1), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxLEFT|wxTOP, 5);

    m_gaugeMicInput = new wxGauge;
    m_gaugeMicInput->Create( itemPanel1, ID_GAUGE_MIC_INPUT, 100, wxDefaultPosition, wxSize(-1, 15), wxGA_HORIZONTAL );
    m_gaugeMicInput->SetValue(0);
    itemGridBagSizer4->Add(m_gaugeMicInput, wxGBPosition(1, 0), wxGBSpan(1, 2), wxGROW|wxALIGN_CENTER_VERTICAL|wxRIGHT|wxTOP|wxBOTTOM, 5);

    itemGridBagSizer4->AddGrowableCol(1);

#endif

    wxStaticBox* itemStaticBoxSizer8Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Volume"));
    wxStaticBoxSizer* itemStaticBoxSizer8 = new wxStaticBoxSizer(itemStaticBoxSizer8Static, wxVERTICAL);
    itemBoxSizer2->Add(itemStaticBoxSizer8, 0, wxGROW, 5);

    wxGridBagSizer* itemGridBagSizer9 = new wxGridBagSizer(0, 0);
    itemGridBagSizer9->SetEmptyCellSize(wxSize(10, 20));
    itemStaticBoxSizer8->Add(itemGridBagSizer9, 0, wxGROW|wxALL, 5);

    m_btnSoundOutput = new wxBitmapButton;
    m_btnSoundOutput->Create( itemPanel1, ID_BITMAPBUTTON_SOUNDOUTPUT, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer9->Add(m_btnSoundOutput, wxGBPosition(0, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldOutput = new wxSlider;
    m_sldOutput->Create( itemPanel1, ID_SLIDER_OUTPUT, 0, 0, 200, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
    itemGridBagSizer9->Add(m_sldOutput, wxGBPosition(0, 1), wxGBSpan(1, 1), wxGROW|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT|wxTOP, 5);

    m_gaugeOutput = new wxGauge;
    m_gaugeOutput->Create( itemPanel1, ID_GAUGE_OUTPUT, 100, wxDefaultPosition, wxSize(-1, 15), wxGA_HORIZONTAL );
    m_gaugeOutput->SetValue(1);
    itemGridBagSizer9->Add(m_gaugeOutput, wxGBPosition(1, 0), wxGBSpan(1, 2), wxGROW|wxALIGN_CENTER_VERTICAL|wxRIGHT|wxTOP|wxBOTTOM, 5);

    wxCheckBox* itemCheckBox13 = new wxCheckBox;
    itemCheckBox13->Create( itemPanel1, ID_CHECKBOX_MICBOOST, _("Mic boost"), wxDefaultPosition, wxDefaultSize, 0 );
    itemCheckBox13->SetValue(false);
    itemGridBagSizer9->Add(itemCheckBox13, wxGBPosition(2, 0), wxGBSpan(1, 2), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    itemGridBagSizer9->AddGrowableCol(1);

    wxStaticBox* itemStaticBoxSizer14Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Effects"));
    wxStaticBoxSizer* itemStaticBoxSizer14 = new wxStaticBoxSizer(itemStaticBoxSizer14Static, wxVERTICAL);
    itemBoxSizer2->Add(itemStaticBoxSizer14, 0, wxGROW|wxTOP, 5);

    wxGridBagSizer* itemGridBagSizer15 = new wxGridBagSizer(0, 0);
    itemGridBagSizer15->SetEmptyCellSize(wxSize(10, 20));
    itemStaticBoxSizer14->Add(itemGridBagSizer15, 0, wxGROW|wxALL, 5);

    wxStaticText* itemStaticText16 = new wxStaticText;
    itemStaticText16->Create( itemPanel1, wxID_STATIC, _("Reverb:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer15->Add(itemStaticText16, wxGBPosition(0, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_btnReverb = new wxBitmapButton;
    m_btnReverb->Create( itemPanel1, ID_BITMAPBUTTON_REVERB, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer15->Add(m_btnReverb, wxGBPosition(0, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldReverb = new wxSlider;
    m_sldReverb->Create( itemPanel1, ID_SLIDER_REVERB, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer15->Add(m_sldReverb, wxGBPosition(0, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtReverb = new wxTextCtrl;
    m_txtReverb->Create( itemPanel1, ID_TEXTCTRL_REVERB, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer15->Add(m_txtReverb, wxGBPosition(0, 3), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    wxStaticText* itemStaticText20 = new wxStaticText;
    itemStaticText20->Create( itemPanel1, wxID_STATIC, _("Chorus:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer15->Add(itemStaticText20, wxGBPosition(1, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBitmapButton* itemBitmapButton21 = new wxBitmapButton;
    itemBitmapButton21->Create( itemPanel1, ID_BITMAPBUTTON_CHORUS, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer15->Add(itemBitmapButton21, wxGBPosition(1, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldChorus = new wxSlider;
    m_sldChorus->Create( itemPanel1, ID_SLIDER_CHORUS, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer15->Add(m_sldChorus, wxGBPosition(1, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtChorus = new wxTextCtrl;
    m_txtChorus->Create( itemPanel1, ID_TEXTCTRL_CHORUS, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer15->Add(m_txtChorus, wxGBPosition(1, 3), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    wxStaticText* itemStaticText24 = new wxStaticText;
    itemStaticText24->Create( itemPanel1, wxID_STATIC, _("Pitch shift:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer15->Add(itemStaticText24, wxGBPosition(2, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBitmapButton* itemBitmapButton25 = new wxBitmapButton;
    itemBitmapButton25->Create( itemPanel1, ID_BITMAPBUTTON_PITCHSHIFT, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer15->Add(itemBitmapButton25, wxGBPosition(2, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldPitchShift = new wxSlider;
    m_sldPitchShift->Create( itemPanel1, ID_SLIDER_PITCHSHIFT, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer15->Add(m_sldPitchShift, wxGBPosition(2, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtPitchShift = new wxTextCtrl;
    m_txtPitchShift->Create( itemPanel1, ID_TEXTCTRL_PITCHSHIFT, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer15->Add(m_txtPitchShift, wxGBPosition(2, 3), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    wxStaticText* itemStaticText28 = new wxStaticText;
    itemStaticText28->Create( itemPanel1, wxID_STATIC, _("Distorsion:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer15->Add(itemStaticText28, wxGBPosition(3, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBitmapButton* itemBitmapButton29 = new wxBitmapButton;
    itemBitmapButton29->Create( itemPanel1, ID_BITMAPBUTTON_DISTORSION, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer15->Add(itemBitmapButton29, wxGBPosition(3, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldDistorsion = new wxSlider;
    m_sldDistorsion->Create( itemPanel1, ID_SLIDER_DISTORSION, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer15->Add(m_sldDistorsion, wxGBPosition(3, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtDistorsion = new wxTextCtrl;
    m_txtDistorsion->Create( itemPanel1, ID_TEXTCTRL_DISTORSION, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer15->Add(m_txtDistorsion, wxGBPosition(3, 3), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    itemGridBagSizer15->AddGrowableCol(2);

    wxGridSizer* itemGridSizer32 = new wxGridSizer(0, 1, 0, 0);
    itemStaticBoxSizer14->Add(itemGridSizer32, 0, wxGROW|wxALL, 5);

#if defined(__WXOS2__)
    m_chkRobot = new wxCheckBox;
    m_chkRobot->Create( itemPanel1, ID_CHECKBOX_ROBOT, _("Robot"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkRobot->SetValue(false);
    itemGridSizer32->Add(m_chkRobot, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

#endif

    m_chkHowlingReduction = new wxCheckBox;
    m_chkHowlingReduction->Create( itemPanel1, ID_CHECKBOX_HOWLINGREDUCTION, _("Howling reduction"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkHowlingReduction->SetValue(false);
    itemGridSizer32->Add(m_chkHowlingReduction, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

#if defined(__WXOS2__)
    m_chkPhone = new wxCheckBox;
    m_chkPhone->Create( itemPanel1, ID_CHECKBOX_PHONE, _("Phone sound"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkPhone->SetValue(false);
    itemGridSizer32->Add(m_chkPhone, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

#endif

    wxGridBagSizer* itemGridBagSizer36 = new wxGridBagSizer(0, 0);
    itemGridBagSizer36->SetEmptyCellSize(wxSize(10, 20));
    itemStaticBoxSizer14->Add(itemGridBagSizer36, 0, wxGROW|wxLEFT|wxRIGHT|wxBOTTOM, 5);

    wxStaticText* itemStaticText37 = new wxStaticText;
    itemStaticText37->Create( itemPanel1, wxID_STATIC, _("Echo"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer36->Add(itemStaticText37, wxGBPosition(0, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticLine* itemStaticLine38 = new wxStaticLine;
    itemStaticLine38->Create( itemPanel1, wxID_STATIC, wxDefaultPosition, wxSize(-1, 1), wxLI_HORIZONTAL );
    itemGridBagSizer36->Add(itemStaticLine38, wxGBPosition(1, 0), wxGBSpan(1, 5), wxGROW|wxALIGN_TOP|wxALL|wxFIXED_MINSIZE, 5);

    wxStaticText* itemStaticText39 = new wxStaticText;
    itemStaticText39->Create( itemPanel1, wxID_STATIC, _("Delay:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer36->Add(itemStaticText39, wxGBPosition(2, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBitmapButton* itemBitmapButton40 = new wxBitmapButton;
    itemBitmapButton40->Create( itemPanel1, ID_BITMAPBUTTON_ECHODELAY, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer36->Add(itemBitmapButton40, wxGBPosition(2, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldEchoDelay = new wxSlider;
    m_sldEchoDelay->Create( itemPanel1, ID_SLIDER_ECHODELAY, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer36->Add(m_sldEchoDelay, wxGBPosition(2, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtEchoDelay = new wxTextCtrl;
    m_txtEchoDelay->Create( itemPanel1, ID_TEXTCTRL_ECHODELAY, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer36->Add(m_txtEchoDelay, wxGBPosition(2, 3), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    wxStaticText* itemStaticText43 = new wxStaticText;
    itemStaticText43->Create( itemPanel1, wxID_STATIC, _("s"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer36->Add(itemStaticText43, wxGBPosition(2, 4), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxRIGHT|wxTOP|wxBOTTOM, 5);

    wxStaticText* itemStaticText44 = new wxStaticText;
    itemStaticText44->Create( itemPanel1, wxID_STATIC, _("Pitch shift:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemGridBagSizer36->Add(itemStaticText44, wxGBPosition(3, 0), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBitmapButton* itemBitmapButton45 = new wxBitmapButton;
    itemBitmapButton45->Create( itemPanel1, ID_BITMAPBUTTON_ECHOPITCHSHIFT, itemPanel1->GetBitmapResource(wxT("bitmaps/disable_icon.xpm")), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
    itemGridBagSizer36->Add(itemBitmapButton45, wxGBPosition(3, 1), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_sldEchoPitchShift = new wxSlider;
    m_sldEchoPitchShift->Create( itemPanel1, ID_SLIDER_ECHOPITCHSHIFT, 0, 0, 200, wxDefaultPosition, wxSize(100, -1), wxSL_HORIZONTAL );
    itemGridBagSizer36->Add(m_sldEchoPitchShift, wxGBPosition(3, 2), wxGBSpan(1, 1), wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxLEFT, 0);

    m_txtEchoPitchShift = new wxTextCtrl;
    m_txtEchoPitchShift->Create( itemPanel1, ID_TEXTCTRL_ECHOPITCHSHIFT, wxEmptyString, wxDefaultPosition, wxSize(40, -1), wxTE_READONLY|wxTE_RIGHT );
    itemGridBagSizer36->Add(m_txtEchoPitchShift, wxGBPosition(3, 3), wxGBSpan(1, 1), wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5);

    itemGridBagSizer36->AddGrowableCol(2);

////@end PlayWithVoicePanel content construction

	assert (m_component);

#if 0
	m_sldMicInput->SetRange (m_component->getMicInputControl().getMin(), m_component->getMicInputControl().getMax());
	m_sldMicInput->SetValue (m_component->getMicInputControl().getValue());
	//UpdateSliderEvent(ID_SLIDER_MIC_INPUT);
	m_gaugeMicInput->SetRange (100);
#endif

	m_sldOutput->SetRange (m_component->getOutputControl().getMin(), m_component->getOutputControl().getMax());
	m_sldOutput->SetValue (m_component->getOutputControl().getValue());
//	UpdateSliderEvent(ID_SLIDER_OUTPUT);	
	m_gaugeOutput->SetRange (100);

	m_sldReverb->SetRange (m_component->getReverb().getMin(), m_component->getReverb().getMax());
	m_sldReverb->SetValue (m_component->getReverb().getValue());
	UpdateSliderEvent(ID_SLIDER_REVERB);	

	m_sldChorus->SetRange (m_component->getChorus().getMin(), m_component->getChorus().getMax());
	m_sldChorus->SetValue (m_component->getChorus().getValue());
	UpdateSliderEvent(ID_SLIDER_CHORUS);

	m_sldPitchShift->SetRange (m_component->getPitchShift().getMin(), m_component->getPitchShift().getMax());
	m_sldPitchShift->SetValue (m_component->getPitchShift().getValue());
	UpdateSliderEvent(ID_SLIDER_PITCHSHIFT);

	m_sldDistorsion->SetRange (m_component->getDistorsion().getMin(), m_component->getDistorsion().getMax());
	m_sldDistorsion->SetValue (m_component->getDistorsion().getValue());
	UpdateSliderEvent(ID_SLIDER_DISTORSION);

#if 0
	m_chkRobot->SetValue (m_component->getRobot());
	m_chkPhone->SetValue (m_component->getPhone());
#endif
	m_chkHowlingReduction->SetValue (m_component->getHowlingReduction());

	m_sldEchoDelay->SetRange (m_component->getEchoDelay().getMin(), m_component->getEchoDelay().getMax());

	m_logExpRangeEchoDelay.SetParams (0.0f, 0.0f, (float) m_component->getEchoDelay().getMax(), 
		(float) m_component->getEchoDelay().getMax(), 0.0f, 4.0f);

	m_logExpRangeEchoDelay.SetExpValue((float) m_component->getEchoDelay().getValue());
	m_sldEchoDelay->SetValue ( (int) m_logExpRangeEchoDelay.GetLinearValue() );
	UpdateSliderEvent(ID_SLIDER_ECHODELAY);
	
	m_sldEchoPitchShift->SetRange (m_component->getEchoPitchShift().getMin(), m_component->getEchoPitchShift().getMax());
	m_sldEchoPitchShift->SetValue (m_component->getEchoPitchShift().getValue());
	UpdateSliderEvent(ID_SLIDER_ECHOPITCHSHIFT);
}


/*!
 * Should we show tooltips?
 */

bool PlayWithVoicePanel::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap PlayWithVoicePanel::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin PlayWithVoicePanel bitmap retrieval
    wxUnusedVar(name);
    if (name == _T("bitmaps/disable_icon.xpm"))
    {
        wxBitmap bitmap(disable_icon);
        return bitmap;
    }
    return wxNullBitmap;
////@end PlayWithVoicePanel bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon PlayWithVoicePanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin PlayWithVoicePanel icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end PlayWithVoicePanel icon retrieval
}

void PlayWithVoicePanel::NotifyComponentUpdate()
{
	wxCommandEvent event(wxEVT_COMPONENT_PVOICE_UPDATE);
	wxPostEvent(this, event);
}

void PlayWithVoicePanel::OnComponentUpdated( wxCommandEvent& event )
{
	assert (m_component);
	if (m_component) {
#if 0
		m_gaugeMicInput->SetValue ((int) m_component->getMicInputEnvelope());
#endif
		m_gaugeOutput->SetValue ((int) m_component->getOutputEnvelope());
	}

	event.Skip(false);
}


// Send a wxECVT_COMMAND_SLIDER_UPDATED event
void PlayWithVoicePanel::UpdateSliderEvent(int id)
{
	wxCommandEvent evt (wxEVT_COMMAND_SLIDER_UPDATED, id);
	GetEventHandler()->ProcessEvent( evt );
}

// Show a float number in a text box
void PlayWithVoicePanel::TextCtrlF (float v, wxTextCtrl* tb)
{
	wxString str;
	str.Printf (_T("%.2f"), v);
	tb->ChangeValue (str);	
}

// Put the value of a slider into a text box
void PlayWithVoicePanel::Slider2TextCtrl(wxSlider* slider, wxTextCtrl* tb)
{
	wxString str;
	str.Printf (_T("%d"), slider->GetValue());
	tb->ChangeValue (str);	
}

// Put the value of a slider into a text box. Float version with optional scaling factor
void PlayWithVoicePanel::Slider2TextCtrlF(wxSlider* slider, wxTextCtrl* tb, float fact)
{
	TextCtrlF (((float) slider->GetValue()) * fact, tb);
}

/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_MIC_INPUT
 */
#if defined(__WXOS2__)
void PlayWithVoicePanel::OnSliderMicInputUpdated( wxCommandEvent& event )
{
	m_component->setMicInputControl (m_sldMicInput->GetValue());

	event.Skip (false);
}
#endif

/*!
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_MICBOOST
 */

void PlayWithVoicePanel::OnCheckboxMicboostClick( wxCommandEvent& event )
{
	if (event.IsChecked())
		m_component->setMicInputControl (500);
	else
		m_component->setMicInputControl (100);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_OUTPUT
 */

void PlayWithVoicePanel::OnSliderOutputUpdated( wxCommandEvent& event )
{
	m_component->setOutputControl (m_sldOutput->GetValue());

	event.Skip (false);
}

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_REVERB
 */

void PlayWithVoicePanel::OnBitmapbuttonReverbClick( wxCommandEvent& event )
{
	m_sldReverb->SetValue (m_component->getReverb().getDefault());
	UpdateSliderEvent(ID_SLIDER_REVERB); 

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_REVERB
 */

void PlayWithVoicePanel::OnSliderReverbUpdated( wxCommandEvent& event )
{
	Slider2TextCtrl(m_sldReverb, m_txtReverb);
	m_component->setReverb (m_sldReverb->GetValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_CHORUS
 */

void PlayWithVoicePanel::OnSliderChorusUpdated( wxCommandEvent& event )
{
	Slider2TextCtrl(m_sldChorus, m_txtChorus);
	m_component->setChorus (m_sldChorus->GetValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_CHORUS
 */

void PlayWithVoicePanel::OnBitmapbuttonChorusClick( wxCommandEvent& event )
{
	m_sldChorus->SetValue (m_component->getChorus().getDefault());
	UpdateSliderEvent(ID_SLIDER_CHORUS); 

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_PITCHSHIFT
 */

void PlayWithVoicePanel::OnBitmapbuttonPitchshiftClick( wxCommandEvent& event )
{
	m_sldPitchShift->SetValue (m_component->getPitchShift().getDefault());
	UpdateSliderEvent(ID_SLIDER_PITCHSHIFT); 

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_PITCHSHIFT
 */

void PlayWithVoicePanel::OnSliderPitchshiftUpdated( wxCommandEvent& event )
{
	Slider2TextCtrlF(m_sldPitchShift, m_txtPitchShift, 0.01f);
	m_component->setPitchShift (m_sldPitchShift->GetValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_ROBOT
 */
#if defined(__WXOS2__)
void PlayWithVoicePanel::OnCheckboxRobotClick( wxCommandEvent& event )
{
	m_component->setRobot (m_chkRobot->GetValue());

	event.Skip (false);
}
#endif

/*!
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_PHONE
 */
#if defined(__WXOS2__)
void PlayWithVoicePanel::OnCheckboxPhoneClick( wxCommandEvent& event )
{
	m_component->setPhone (m_chkPhone->GetValue());

	event.Skip (false);
}
#endif

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_ECHODELAY
 */

void PlayWithVoicePanel::OnBitmapbuttonEchodelayClick( wxCommandEvent& event )
{
	m_logExpRangeEchoDelay.SetExpValue ((float) m_component->getEchoDelay().getDefault());
	m_sldEchoDelay->SetValue ( (int) m_logExpRangeEchoDelay.GetLinearValue() );
	UpdateSliderEvent(ID_SLIDER_ECHODELAY); 

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_ECHODELAY
 */

void PlayWithVoicePanel::OnSliderEchodelayUpdated( wxCommandEvent& event )
{
	m_logExpRangeEchoDelay.SetLinearValue ((float) m_sldEchoDelay->GetValue());
	TextCtrlF (m_logExpRangeEchoDelay.GetExpValue()/100.0f, m_txtEchoDelay);
	m_component->setEchoDelay ((int) m_logExpRangeEchoDelay.GetExpValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_ECHOPITCHSHIFT
 */

void PlayWithVoicePanel::OnBitmapbuttonEchopitchshiftClick( wxCommandEvent& event )
{
	m_sldEchoPitchShift->SetValue (m_component->getEchoPitchShift().getDefault());
	UpdateSliderEvent(ID_SLIDER_ECHOPITCHSHIFT); 

	event.Skip (false); 
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_ECHOPITCHSHIFT
 */

void PlayWithVoicePanel::OnSliderEchopitchshiftUpdated( wxCommandEvent& event )
{
	Slider2TextCtrlF(m_sldEchoPitchShift, m_txtEchoPitchShift, 0.01f);
	m_component->setEchoPitchShift (m_sldEchoPitchShift->GetValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON
 */
#if defined(__WXOS2__)
void PlayWithVoicePanel::OnBitmapbuttonMicinputClick( wxCommandEvent& event )
{
	m_sldMicInput->SetValue (m_component->getMicInputControl().getDefault());
	UpdateSliderEvent(ID_SLIDER_MIC_INPUT); 

	event.Skip (false);
}
#endif

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_SOUNDOUTPUT
 */

void PlayWithVoicePanel::OnBitmapbuttonSoundoutputClick( wxCommandEvent& event )
{
	m_sldOutput->SetValue (m_component->getOutputControl().getDefault());
	UpdateSliderEvent(ID_SLIDER_OUTPUT);

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BITMAPBUTTON_DISTORSION
 */

void PlayWithVoicePanel::OnBitmapbuttonDistorsionClick( wxCommandEvent& event )
{
	m_sldDistorsion->SetValue (m_component->getDistorsion().getDefault());
	UpdateSliderEvent(ID_SLIDER_DISTORSION);

    event.Skip(false);
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_DISTORSION
 */

void PlayWithVoicePanel::OnSliderDistorsionUpdated( wxCommandEvent& event )
{
	Slider2TextCtrl(m_sldDistorsion, m_txtDistorsion);
	m_component->setDistorsion (m_sldDistorsion->GetValue());

	event.Skip (false);
}


/*!
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_HOWLINGREDUCTION
 */

void PlayWithVoicePanel::OnCheckboxHowlingreductionClick( wxCommandEvent& event )
{
	m_component->setHowlingReduction (m_chkHowlingReduction->GetValue());

	event.Skip (false);
}

}



