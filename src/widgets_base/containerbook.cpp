/////////////////////////////////////////////////////////////////////////////
// Name:        custombook.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     23/05/2011 18:51:40
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif


////@begin includes
#include "wx/imaglist.h"
////@end includes

#include "widgets_base/containerbook.h"

////@begin XPM images
////@end XPM images

//namespace widgets_base {

/*
 * ContainerBook type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ContainerBook, wxNotebook )


/*
 * ContainerBook event table definition
 */

BEGIN_EVENT_TABLE( ContainerBook, wxNotebook )

////@begin ContainerBook event table entries
    EVT_SIZE( ContainerBook::OnSize )

////@end ContainerBook event table entries

END_EVENT_TABLE()


/*
 * ContainerBook constructors
 */

ContainerBook::ContainerBook()
{
    Init();
}

ContainerBook::ContainerBook(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
    Init();
    Create(parent, id, pos, size, style);
}


/*
 * CustomBook creator
 */

bool ContainerBook::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
////@begin ContainerBook creation
    wxNotebook::Create(parent, id, pos, size, style);
    CreateControls();
////@end ContainerBook creation
    return true;
}


/*
 * ContainerBook destructor
 */

ContainerBook::~ContainerBook()
{
////@begin ContainerBook destruction
////@end ContainerBook destruction
}


/*
 * Member initialisation
 */

void ContainerBook::Init()
{
////@begin ContainerBook member initialisation
////@end ContainerBook member initialisation
}


/*
 * Control creation for CustomBook
 */

void ContainerBook::CreateControls()
{    
////@begin ContainerBook content construction
////@end ContainerBook content construction
}


/*
 * Should we show tooltips?
 */

bool ContainerBook::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ContainerBook::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ContainerBook bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end ContainerBook bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ContainerBook::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ContainerBook icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end ContainerBook icon retrieval
}


/*
 * wxEVT_SIZE event handler for ID_NOTEBOOK
 */

void ContainerBook::OnSize( wxSizeEvent& event )
{
	if (event.GetSize().GetX() || event.GetSize().GetY())
		wxNotebook::OnSize(event);
	else {
//		wxSize sbef= GetSize();
		Layout();
		Fit();

//		wxSize safter= GetSize();

//		if (sbef!= safter) {
			wxSizeEvent evt;
			wxPostEvent (GetParent(), evt);
//		}
		event.Skip(false);
	}
}

//};

