#
# switch viacam
#
# Input pins
#	image (iplimage)
#
# Output pins
# 	motion (float)
#

type switch_viacam
name "Switch Viacam"

# Camera source. 
create camera_grabber grabber

# Camera panel 
create camera_viewer viewer
connect grabber image viewer image

# Virtual switch logic
import lib/virtual_switch.sps

create virtual_switch vs1 -x 0.15 -y 0.2 -c 16711680
connect vs1 roi viewer roi
connect viewer roi vs1 roi
connect grabber image vs1 image

create virtual_switch vs2 -x 0.85 -y 0.2 -c 19711680
connect vs2 roi viewer roi
connect viewer roi vs2 roi
connect grabber image vs2 image

# 
# Mouse output
#

create print p
connect vs1 action_fired p in
connect vs2 action_fired p in

create mouse_output mouse
connect vs1 action_fired mouse right_click
connect vs2 action_fired mouse left_click

#
# Click sound
#
create wav_player player -p \"$SP_DATA_DIR$/audio/click.wav\"
connect vs1 action_fired player play
connect vs2 action_fired player play

begin_gui_layout
	layout_begin hbox
		component viewer
		layout_begin vbox "Right click"
			component vs1 
		layout_end
		layout_begin vbox "Left click"
			component vs2 
		layout_end
	layout_end
end_gui_layout
