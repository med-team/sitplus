#
# Translates and IPL image into a SDL surface and provides 
# a checkbox to enable/disable such a feature
#
# Input pins
#	image (iplimage)
#
# Output pins
# 	surface (sdl_surface)
#

type ipl2sdl_chk
name "IPL2SDL"

# IPL image source. Create a forwarder to export the pin ...
create forward ipin_image

# and export it as "image"
export_ipin ipin_image in image iplimage

# enable/disable checkbox
create widget_checkbox enable_disable -l _"Show camera image as background" -v true
connect enable_disable value ipin_image gate

# Send to main
create send_main_async to_main
connect ipin_image out to_main in

# IPL2SDL
create ipl2sdl ipl2sdl
connect to_main out ipl2sdl in

# Export sdl pin
export_opin ipl2sdl out surface

begin_gui_layout
	layout_begin vbox
		component enable_disable	
	layout_end
end_gui_layout
