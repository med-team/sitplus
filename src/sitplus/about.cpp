/////////////////////////////////////////////////////////////////////////////
// Name:        about.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     20/06/2011 11:18:13
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

#include "about.h"
#include "spcore/coreruntime.h"
#include "spcore/paths.h"
#include "version.h"

////@begin includes
////@end includes
#include <wx/textfile.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/icon.h>
#include <wx/utils.h>

////@begin XPM images
////@end XPM images


/*
 * About type definition
 */

IMPLEMENT_DYNAMIC_CLASS( About, wxDialog )


/*
 * About event table definition
 */

BEGIN_EVENT_TABLE( About, wxDialog )

////@begin About event table entries
    EVT_HTML_LINK_CLICKED( ID_HTMLWINDOW, About::OnHtmlwindowLinkClicked )

    EVT_BUTTON( ID_BUTTON_CLOSE_ABOUT, About::OnButtonCloseAboutClick )

////@end About event table entries

END_EVENT_TABLE()


/*
 * About constructors
 */

About::About()
{
    Init();
}

About::About( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, caption, pos, size, style);
}


/*
 * About creator
 */

bool About::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin About creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxDialog::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end About creation
    return true;
}


/*
 * About destructor
 */

About::~About()
{
////@begin About destruction
////@end About destruction
}


/*
 * Member initialisation
 */

void About::Init()
{
////@begin About member initialisation
    m_htmlAbout = NULL;
////@end About member initialisation
}


/*
 * Control creation for About
 */

void About::CreateControls()
{    
////@begin About content construction
    About* itemDialog1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemDialog1->SetSizer(itemBoxSizer2);

    m_htmlAbout = new wxHtmlWindow( itemDialog1, ID_HTMLWINDOW, wxDefaultPosition, wxSize(450, 300), wxHW_SCROLLBAR_AUTO|wxSUNKEN_BORDER|wxHSCROLL|wxVSCROLL );
    itemBoxSizer2->Add(m_htmlAbout, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    itemBoxSizer2->Add(itemBoxSizer4, 0, wxALIGN_RIGHT|wxALL, 5);

    wxButton* itemButton5 = new wxButton( itemDialog1, ID_BUTTON_CLOSE_ABOUT, _("Close"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer4->Add(itemButton5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

////@end About content construction

	FillHtmlContent();
}


/*
 * Should we show tooltips?
 */

bool About::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap About::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin About bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end About bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon About::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin About icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end About icon retrieval
}


/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_ABOUT
 */

void About::OnButtonCloseAboutClick( wxCommandEvent&  )
{
////@begin wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_ABOUT in About.
    // Before editing this code, remove the block markers.
    EndModal(ID_BUTTON_CLOSE_ABOUT);
////@end wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_ABOUT in About. 
}

/*
 * wxEVT_COMMAND_HTML_LINK_CLICKED event handler for ID_HTMLWINDOW
 */

void About::OnHtmlwindowLinkClicked( wxHtmlLinkEvent& event )
{
	if (event.GetLinkInfo().GetHref().StartsWith(_T("http://")))
		wxLaunchDefaultBrowser(event.GetLinkInfo().GetHref());
	else
		m_htmlAbout->OnLinkClicked(event.GetLinkInfo());

	event.Skip(false);
}

void About::FillHtmlContent()
{
	wxString page (_T("<style type=\"text/css\"><!-- div {font-family: Arial, Helvetica, sans-serif} --></style>"));
	page.Append (_T("<div><img src=\"#DATA_PATH#/logos/sitplus_logo_text_small_400x100.png\"><h2>"));
	page.Append (_("A free framework to develop interactive activities."));
	page.Append (_T("</h2><h3>v#APPVERSION#</h3>"));
	
	page.Append (_T("<h4>"));
	page.Append (_T("<a href='http://sitplus.appctarragona.org' target='_blank'>http://sitplus.appctarragona.org</a>"));
	page.Append (_T("</h4>"));

	page.Append (_T("<p>&copy; 2010-11 Cesar Mauri Loba<br>"));
	page.Append (_T("&copy; 2010-11 Associaci&oacute; Provincial de Par&agrave;lisi Cerebral (APPC) de Tarragona.</p>"));

	page.Append (_T("<p><strong>"));
	page.Append (_("Released under the terms of the GNU General Public License version 3"));
	page.Append (_T("</strong></p>"));

	page.Append (_T("<p>"));
	page.Append (_("With the support of:"));
	page.Append (_T("</p>"));
	page.Append (_T("<p><img src=\"#DATA_PATH#/logos/obrasocial_puntcat.png\"></p>"));
	page.Append (_T("<p><img src=\"#DATA_PATH#/logos/acciosocial.png\"></p>"));
	page.Append (_T("<p><img src=\"#DATA_PATH#/logos/tuajudes.png\"></p>"));
	page.Append (_T("<p>"));	
	page.Append (_("Thanks to:"));
	page.Append (_T("</p><br><p>"));
	
	const char* dataDir= spcore::getSpCoreRuntime()->GetPaths().GetDataDir();
	wxString fullPath(dataDir, wxConvUTF8);
	fullPath.Append(_T("/THANKS"));
	wxTextFile thanksFile(fullPath);
	if (thanksFile.Open()) {
		wxString str;
		for (str = thanksFile.GetFirstLine(); !thanksFile.Eof(); str = thanksFile.GetNextLine()) {
			page.Append (str);
			page.Append (_T("<br>"));
		}
	}

	page.Append (_T("</p></div>"));

	page.Replace (_T("#DATA_PATH#"), wxString(dataDir, wxConvUTF8), true);
	page.Replace (_T("#APPVERSION#"), _T(SITPLUS_VERSION), true);
	m_htmlAbout->SetPage(page);
}




