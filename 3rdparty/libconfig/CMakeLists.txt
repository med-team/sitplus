cmake_minimum_required(VERSION 2.6)
project(libconfig)

set(libconfig_SRCS
	grammar.c
	grammar.h
	libconfig.c
	libconfig.h
	parsectx.h
	scanctx.c
	scanctx.h
	scanner.c
	scanner.h
	strbuf.c
	strbuf.h
	wincompat.h
)

SET (LibConfig_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
MESSAGE (STATUS "LIBCONFIG: ${LibConfig_INCLUDE_DIR}")
add_definitions (-DLIBCONFIG_EXPORTS -DYY_NO_UNISTD_H -DYY_USE_CONST -D_CRT_SECURE_NO_DEPRECATE -D_STDLIB_H)
add_library (libconfig SHARED ${libconfig_SRCS})

if(WIN32)
	install (TARGETS libconfig RUNTIME DESTINATION ${BINDIR})
endif(WIN32)