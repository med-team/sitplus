/////////////////////////////////////////////////////////////////////////////
// Name:        wcamerapanel.h
// Purpose:		wxPanel derived class to show live camera image
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Modified by: 
// Created:     
// Copyright:   (C) 2008-11 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#ifndef CAMERAPANEL_H_
#define CAMERAPANEL_H_

#include "creavision/crvimage.h"
#include <wx/panel.h>
#include <wx/bitmap.h>
#include <wx/event.h>
#include <boost/function.hpp>

namespace mod_camera {

#define CAMERA_PANEL_NAME _("Camera viewer")

class WXRoiControls;

class CameraPanel : public wxPanel
{     
public:
	// Functor typedef to install a callback when an instance is destroyed
	typedef boost::function0<void> CleanupFunctor;

	//CameraPanel();
	CameraPanel(CleanupFunctor functor= NULL, WXRoiControls* roi_controls= NULL);
	CameraPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSIMPLE_BORDER, const wxString& name= CAMERA_PANEL_NAME ); 
	
	bool Create(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSIMPLE_BORDER, const wxString& name= CAMERA_PANEL_NAME ); 
	
	virtual ~CameraPanel(void);

	// Draw method
	void DrawCam (IplImage const * pImg);

	void SetAllowAutoRezise ( bool value ) { m_autoResize= value; }

	void RemoveCleanupFunctor();
private:
	virtual wxSize DoGetBestSize() const;
	void OnPaint(wxPaintEvent& event);
	
	void OnRecvRefresh( wxCommandEvent &event );
	void OnEvtMotion ( wxMouseEvent& event );
	void OnEvtLeftDClick ( wxMouseEvent& event );
	void OnMouse ( wxMouseEvent& event );	

	void Init();

protected:
	DECLARE_EVENT_TABLE()

private:
	wxCriticalSection	m_ImageCopyMutex;
	wxBitmap	m_Bitmap;
	volatile bool	m_ImageShowed;
	volatile bool	m_AccessingImage;
	bool m_autoResize;
	
	// Previous captured image size
	int		m_nImgWidth;
	int		m_nImgHeight;
	CIplImage m_SharedImage;
	CIplImage m_DisplayImage;	

	CleanupFunctor m_cleanupFunctor;
	WXRoiControls* m_roiControls;
};

}

#endif
