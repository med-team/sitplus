
type test_ipl2sdl
name "IPL two SDL"

# Camera source. 
create camera_grabber grabber

# Send to main
create send_main_async to_main
connect grabber image to_main in

# Camera panel 
create camera_viewer viewer
connect to_main out viewer image

# SDL drawer
create sdl_drawer sdl_drawer

# IPL2SDL
create ipl2sdl ipl2sdl
connect to_main out ipl2sdl in

connect ipl2sdl out sdl_drawer queue
connect ipl2sdl out sdl_drawer draw

begin_gui_layout
	layout_begin hbox
		component viewer
	layout_end
end_gui_layout
