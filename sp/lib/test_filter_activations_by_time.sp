
type test_filter_values_by_time
name "Test Filter values by time"
import filter_values_by_time.sps

create filter_values_by_time f -t 2000
create widget_button b
connect b pressed f in

create widget_slider sldtime --min 250 --max 5000 -l "Time (ms)" -v 1000 -i
connect sldtime value f time

create print p
connect f out p in

begin_gui_layout
	layout_begin vbox
		component b
		component sldtime
	layout_end
end_gui_layout
