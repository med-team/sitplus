/////////////////////////////////////////////////////////////////////////////
// Name:        collapsible.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     01/06/2011 18:18:15
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

#ifndef _COLLAPSIBLE_H_
#define _COLLAPSIBLE_H_


/*!
 * Includes
 */


////@begin includes
#include "wx/collpane.h"
////@end includes
#include "spcore/component.h"
#include "spcore/pinimpl.h"
#include "spcore/basictypes.h"


/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_COLLAPSIBLEPANE_PANEL -1
#define SYMBOL_COLLAPSIBLEPANEL_STYLE wxCP_DEFAULT_STYLE|wxCP_NO_TLW_RESIZE
#define SYMBOL_COLLAPSIBLEPANEL_IDNAME ID_COLLAPSIBLEPANE_PANEL
#define SYMBOL_COLLAPSIBLEPANEL_SIZE wxDefaultSize
#define SYMBOL_COLLAPSIBLEPANEL_POSITION wxDefaultPosition
////@end control identifiers

namespace mod_widgets {

class CollapsibleComponent;
/*!
 * CollapsiblePanel class declaration
 */
// TODO: derive from ContainerCollapsible. On Windows there are problems importing
// static data members. See: 
// http://social.msdn.microsoft.com/Forums/en-US/vcgeneral/thread/6b73c234-46f7-4905-a7ae-9c679544e878
class CollapsiblePanel: public wxGenericCollapsiblePane
{    
    DECLARE_DYNAMIC_CLASS( CollapsiblePanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CollapsiblePanel();
    CollapsiblePanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCP_DEFAULT_STYLE, const wxValidator& validator = wxDefaultValidator);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCP_DEFAULT_STYLE, const wxValidator& validator = wxDefaultValidator);

    /// Destructor
    ~CollapsiblePanel();

	// Set destruction callback. NULL removes.
	void SetComponent(CollapsibleComponent* component) { m_component= component; }

	// Notify changes
	void ValueChanged();

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CollapsiblePanel event handler declarations

    /// wxEVT_COMMAND_COLLPANE_CHANGED event handler for ID_COLLAPSIBLEPANE_PANEL
    void OnCollapsiblepanePanelPaneChanged( wxCollapsiblePaneEvent& event );

////@end CollapsiblePanel event handler declarations

////@begin CollapsiblePanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end CollapsiblePanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin CollapsiblePanel member variables
////@end CollapsiblePanel member variables
	CollapsibleComponent* m_component;
};

/**
	widget_collapsible component

	Input pins:		

	Output pins:
		expanded (bool)	True when expanded, false when collapsed

	Command line:
		[-l <label> ]	Label. If empty, no label is shown.
*/
class CollapsibleComponent : public spcore::CComponentAdapter {
public:
	static const char* getTypeName() { return "widget_collapsible"; };
	virtual const char* GetTypeName() const { return CollapsibleComponent::getTypeName(); };

	//
	// Component methods
	//
	CollapsibleComponent(const char * name, int argc, const char * argv[]);
	
	virtual int DoInitialize();

	virtual wxWindow* GetGUI(wxWindow * parent);

	// 
	// Called from GUI
	//	

	//bool IsExpanded() const { return m_expanded->getValue(); }
	void SetIsExpanded(bool v);

	const std::string& GetLabel() const { return m_label; }
	
	void OnPanelDestroyed ();

private:
	CollapsiblePanel* m_panel;
	SmartPtr<spcore::IOutputPin> m_oPinExpanded;
	SmartPtr<spcore::CTypeBool> m_expanded;
	std::string m_label;
	
	virtual ~CollapsibleComponent();
};

typedef spcore::ComponentFactory<CollapsibleComponent> CollapsibleComponentFactory;


}; // namespace end
#endif
    // _COLLAPSIBLE_H_
