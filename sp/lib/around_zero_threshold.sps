#
# Threshold a value around zero
#
# Input pins
#	in (float)
#	thres (float)
#
# Output pins
# 	out (float)
#	above (bool)	- True if value is above threshold.
#
# Arguments
#	-lsld <label>	Slider label
#	-lchk <label>	Checkbox label
#	-max <num>		Max range
#	-v <num>		Initial value

type around_zero_threshold
name "Threshold"
args -lsld -lchk -max -v

# Input pins
create forward forward_in
export_ipin forward_in in in float
create forward forward_thres
export_ipin forward_thres in thres float

#
# Threshold
#
create fthreshold thresneg -a 0 -b orig_minus_thres
create fthreshold threspos -a orig_minus_thres -b 0
create fadd add_thres

connect forward_in out threspos value
connect forward_in out thresneg value
connect threspos result add_thres b
connect thresneg result add_thres a

#
# Slider
#
create widget_slider sldthres --min 0 --max $-max$ -v $-v$ -l \"$-lsld$\" --log 1
connect sldthres value threspos thres
create fmul neg -v -1.0
connect sldthres value neg a
connect neg result thresneg thres

# Input threshold
connect forward_thres out sldthres value
connect forward_thres out threspos thres
connect forward_thres out neg a

# Checkbox to show if motion detected
create widget_checkbox chkmotion -l \"$-lchk$\"
create fneq hasmotion -v 0 
connect add_thres result hasmotion a
connect hasmotion result chkmotion value

# Export motion value
export_opin add_thres result out
export_opin hasmotion result above

begin_gui_layout
	layout_begin vbox
		component sldthres
		component chkmotion
	layout_end
end_gui_layout
