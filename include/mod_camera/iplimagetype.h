/////////////////////////////////////////////////////////////////////////////
// File:        iplimagetype.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef IPLIMAGETYPE_H
#define IPLIMAGETYPE_H

#include "spcore/pin.h"
#include "spcore/pinimpl.h"
#include "spcore/coreruntime.h"
#include "spcore/basictypes.h"
#include <cv.h>

namespace mod_camera {
/**
	Class that carries an IplImage struct
*/

class CTypeIplImageContents : public spcore::CTypeAny {
public:
	static inline const char* getTypeName() { return "iplimage"; }
	virtual const IplImage* getImage() const { return m_iplImage; }
	virtual void setImage( IplImage* i) {
		assert (i);
		assert (i!= m_iplImage);
		if (m_iplImage) cvReleaseImage( &m_iplImage );
		m_iplImage= i;
	}
protected:
	CTypeIplImageContents(int id) : CTypeAny(id) {
		m_iplImage= NULL;
	}
	virtual ~CTypeIplImageContents() {
		if (m_iplImage) cvReleaseImage( &m_iplImage );
		m_iplImage= NULL;
	}
private:
    IplImage* m_iplImage;
};

typedef spcore::SimpleType< CTypeIplImageContents > CTypeIplImage;

}

#endif