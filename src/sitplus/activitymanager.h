/////////////////////////////////////////////////////////////////////////////
// Name:        activitymanager.h
// Purpose:
// Author:      C�sar Mauri Loba
// Modified by:
// Created:     04/04/2011 21:20:18
// RCS-ID:
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
/////////////////////////////////////////////////////////////////////////////

#ifndef _ACTIVITYMANAGER_H_
#define _ACTIVITYMANAGER_H_


/*!
 * Includes
 */

#include "spcore/component.h"
#include "widgets_base/containerbook.h"

////@begin includes
#include "wx/frame.h"
#include "wx/toolbar.h"
#include "wx/notebook.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
class ContainerBook;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_ACTIVITYMANAGER 10008
#define ID_TOOLBAR 10009
#define ID_TOOL_PLAY 10010
#define ID_TOOL_STOP 10011
#define ID_NOTEBOOK_CONT 10007
#define SYMBOL_ACTIVITYMANAGER_STYLE wxCAPTION|wxSYSTEM_MENU|wxMINIMIZE_BOX|wxCLOSE_BOX
#define SYMBOL_ACTIVITYMANAGER_TITLE _("Activity manager")
#define SYMBOL_ACTIVITYMANAGER_IDNAME ID_ACTIVITYMANAGER
#define SYMBOL_ACTIVITYMANAGER_SIZE wxDefaultSize
#define SYMBOL_ACTIVITYMANAGER_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * ActivityManager class declaration
 */

class ActivityManager: public wxFrame
{
    DECLARE_CLASS( ActivityManager )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ActivityManager();
    ActivityManager( wxWindow* parent, wxWindowID id = SYMBOL_ACTIVITYMANAGER_IDNAME, const wxString& caption = SYMBOL_ACTIVITYMANAGER_TITLE, const wxPoint& pos = SYMBOL_ACTIVITYMANAGER_POSITION, const wxSize& size = SYMBOL_ACTIVITYMANAGER_SIZE, long style = SYMBOL_ACTIVITYMANAGER_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_ACTIVITYMANAGER_IDNAME, const wxString& caption = SYMBOL_ACTIVITYMANAGER_TITLE, const wxPoint& pos = SYMBOL_ACTIVITYMANAGER_POSITION, const wxSize& size = SYMBOL_ACTIVITYMANAGER_SIZE, long style = SYMBOL_ACTIVITYMANAGER_STYLE );

    /// Destructor
    ~ActivityManager();

	bool SetActivity (const char * component_type);

	bool SetActivity (SmartPtr<spcore::IComponent>);
private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	bool InitializeActivity();

	void OpenComponentPanels (spcore::IComponent& component);

	void AddSitplusPanel (wxWindow* pan);

////@begin ActivityManager event handler declarations

    /// wxEVT_SIZE event handler for ID_ACTIVITYMANAGER
    void OnSize( wxSizeEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TOOL_PLAY
    void OnToolPlayClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_TOOL_STOP
    void OnToolStopClick( wxCommandEvent& event );

////@end ActivityManager event handler declarations

////@begin ActivityManager member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ActivityManager member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ActivityManager member variables
    wxToolBar* m_toolBar;
    ContainerBook* m_book;
////@end ActivityManager member variables

	SmartPtr<spcore::IComponent> m_rootComponent;
};

#endif
    // _ACTIVITYMANAGER_H_
