type wiiinstr
name _"Wii Virtual Instrument"
args -wii

# Wii input
import lib/wii_mp_motion.sps
import lib/wii_acc_motion.sps

create $-wii$ wii_input

# MIDI player
import lib/motion_midi_player.sps
create motion_midi_player player
connect wii_input motion player motion

# Collage
import lib/motion_collage.sps
create motion_collage motion_collage
create send_main_async to_main

connect wii_input motion to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox _"Wii Remote"
			component wii_input
		layout_end
		layout_begin vbox _"MIDI Sound"
			component player
		layout_end
		layout_begin vbox _"Graphics"
			component motion_collage			
		layout_end
	layout_end
end_gui_layout
