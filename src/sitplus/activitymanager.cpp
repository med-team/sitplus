/////////////////////////////////////////////////////////////////////////////
// Name:        activitymanager.cpp
// Purpose:
// Author:      C�sar Mauri Loba
// Modified by:
// Created:     04/04/2011 21:20:17
// RCS-ID:
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
#include "wx/imaglist.h"
////@end includes

#include "activitymanager.h"
#include "widgets_base/containerbook.h"

////@begin XPM images
#include "bitmaps/Play-Hot-icon.xpm"
#include "bitmaps/Play-Disabled-icon.xpm"
#include "bitmaps/Stop-Normal-Red-icon.xpm"
#include "bitmaps/Stop-Disabled-icon.xpm"
////@end XPM images


/*
 * ActivityManager type definition
 */

IMPLEMENT_CLASS( ActivityManager, wxFrame )


/*
 * ActivityManager event table definition
 */

BEGIN_EVENT_TABLE( ActivityManager, wxFrame )

////@begin ActivityManager event table entries
    EVT_SIZE( ActivityManager::OnSize )

    EVT_MENU( ID_TOOL_PLAY, ActivityManager::OnToolPlayClick )

    EVT_MENU( ID_TOOL_STOP, ActivityManager::OnToolStopClick )

////@end ActivityManager event table entries

END_EVENT_TABLE()

using namespace spcore;

/*
 * ActivityManager constructors
 */

ActivityManager::ActivityManager()
{
    Init();
}

ActivityManager::ActivityManager( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*
 * ActivityManager creator
 */

bool ActivityManager::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin ActivityManager creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxFrame::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end ActivityManager creation
    return true;
}


/*
 * ActivityManager destructor
 */

ActivityManager::~ActivityManager()
{
////@begin ActivityManager destruction
////@end ActivityManager destruction
}


/*
 * Member initialisation
 */

void ActivityManager::Init()
{
////@begin ActivityManager member initialisation
    m_toolBar = NULL;
    m_book = NULL;
////@end ActivityManager member initialisation
}


/*
 * Control creation for ActivityManager
 */

void ActivityManager::CreateControls()
{
////@begin ActivityManager content construction
    ActivityManager* itemFrame1 = this;

    m_toolBar = CreateToolBar( wxTB_FLAT|wxTB_HORIZONTAL|wxTB_TEXT, ID_TOOLBAR );
    wxBitmap itemtool3Bitmap(itemFrame1->GetBitmapResource(wxT("bitmaps/Play-Hot-icon.xpm")));
    wxBitmap itemtool3BitmapDisabled(itemFrame1->GetBitmapResource(wxT("bitmaps/Play-Disabled-icon.xpm")));
    m_toolBar->AddTool(ID_TOOL_PLAY, _("Play"), itemtool3Bitmap, itemtool3BitmapDisabled, wxITEM_NORMAL, wxEmptyString, wxEmptyString);
    wxBitmap itemtool4Bitmap(itemFrame1->GetBitmapResource(wxT("bitmaps/Stop-Normal-Red-icon.xpm")));
    wxBitmap itemtool4BitmapDisabled(itemFrame1->GetBitmapResource(wxT("bitmaps/Stop-Disabled-icon.xpm")));
    m_toolBar->AddTool(ID_TOOL_STOP, _("Stop"), itemtool4Bitmap, itemtool4BitmapDisabled, wxITEM_NORMAL, wxEmptyString, wxEmptyString);
    m_toolBar->EnableTool(ID_TOOL_STOP, false);
    m_toolBar->Realize();
    itemFrame1->SetToolBar(m_toolBar);

    wxBoxSizer* itemBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    itemFrame1->SetSizer(itemBoxSizer5);

    m_book = new ContainerBook( itemFrame1, ID_NOTEBOOK_CONT, wxDefaultPosition, wxDefaultSize, wxBK_DEFAULT|wxNB_NOPAGETHEME );

    itemBoxSizer5->Add(m_book, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);

    // Connect events and objects
    m_toolBar->Connect(ID_TOOLBAR, wxEVT_SIZE, wxSizeEventHandler(ActivityManager::OnSize), NULL, this);
////@end ActivityManager content construction
}

bool ActivityManager::InitializeActivity()
{
	OpenComponentPanels (*m_rootComponent);

	if (m_rootComponent->Initialize()!= 0) {
		wxString message= _("Initialization of activity component ")
			+ wxString(m_rootComponent->GetName(), wxConvUTF8)
			+ _(" failed.\nSee console for details.");
		wxMessageDialog dlg(this, message, _("SITPLUS error"), wxOK);
		dlg.ShowModal();
		m_rootComponent.reset();
		return false;
	}

	if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();

	return true;
}

bool ActivityManager::SetActivity (SmartPtr<spcore::IComponent> compo)
{
	// Tries to create root component for the activity
	assert ( m_rootComponent.get()== NULL);

	m_rootComponent= compo;

	return InitializeActivity();
}

bool ActivityManager::SetActivity (const char * component_type)
{
	// Tries to create root component for the activity
	assert ( m_rootComponent.get()== NULL);

	m_rootComponent= getSpCoreRuntime()->CreateComponent(component_type, component_type, 0, NULL);
	if (m_rootComponent.get()== NULL) {
		wxString message= _("Creation of activity component ")
			+ wxString(component_type, wxConvUTF8)
			+ _(" failed.\nSee console for details.");
		wxMessageDialog dlg(this, message, _("SITPLUS error"), wxOK);
		dlg.ShowModal();
		return false;
	}

	return InitializeActivity();
}

void ActivityManager::AddSitplusPanel (wxWindow* pan)
{
	m_book->AddPage (pan, pan->GetName());
}

void ActivityManager::OpenComponentPanels (IComponent& component)
{
	wxWindow* panel= component.GetGUI(m_book);
	if (panel== NULL) {
		SmartPtr< spcore::IIterator< IComponent* > > it= component.QueryComponents();

		// If component has children, open its pannels
		if (it.get())			
			for (; !it->IsDone(); it->Next()) OpenComponentPanels (*it->CurrentItem());
	}
	else {
		AddSitplusPanel (panel);
	}
}

/*
 * Should we show tooltips?
 */

bool ActivityManager::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ActivityManager::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ActivityManager bitmap retrieval
    wxUnusedVar(name);
    if (name == _T("bitmaps/Play-Hot-icon.xpm"))
    {
        wxBitmap bitmap(Play_Hot_icon);
        return bitmap;
    }
    else if (name == _T("bitmaps/Play-Disabled-icon.xpm"))
    {
        wxBitmap bitmap(Play_Disabled_icon);
        return bitmap;
    }
    else if (name == _T("bitmaps/Stop-Normal-Red-icon.xpm"))
    {
        wxBitmap bitmap(Stop_Normal_Red_icon);
        return bitmap;
    }
    else if (name == _T("bitmaps/Stop-Disabled-icon.xpm"))
    {
        wxBitmap bitmap(Stop_Disabled_icon);
        return bitmap;
    }
    return wxNullBitmap;
////@end ActivityManager bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ActivityManager::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ActivityManager icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end ActivityManager icon retrieval
}


/*
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TOOL_PLAY
 */

void ActivityManager::OnToolPlayClick( wxCommandEvent& event )
{
	assert ( m_rootComponent.get()!= NULL);
	if (m_rootComponent->Start()!= 0) {
		wxMessageDialog dlg(this, _("Can not start activity.\nSee console for details."), _("SITPLUS error"), wxOK);
		dlg.ShowModal();
	}
	else {
		m_toolBar->EnableTool(ID_TOOL_PLAY, false);
		m_toolBar->EnableTool(ID_TOOL_STOP, true);
	}
    event.Skip(false);
}


/*
 * wxEVT_COMMAND_MENU_SELECTED event handler for ID_TOOL_STOP
 */

void ActivityManager::OnToolStopClick( wxCommandEvent& event )
{
	assert ( m_rootComponent.get()!= NULL);
	m_rootComponent->Stop();
	//m_rootComponent->Finish();
	m_toolBar->EnableTool(ID_TOOL_PLAY, true);
	m_toolBar->EnableTool(ID_TOOL_STOP, false);
    event.Skip();
}


/*
 * wxEVT_SIZE event handler for ID_TOOLBAR
 */

void ActivityManager::OnSize( wxSizeEvent& event )
{
	if (event.GetSize().GetX() || event.GetSize().GetY())
		wxFrame::OnSize(event);
	else {
		Layout();
		Fit();
		Refresh();
		
		event.Skip (false);
	}
}
