/////////////////////////////////////////////////////////////////////////////
// Name:        config_gui.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     Wed 09 Mar 2011 11:27:16 CET
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.  
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#include "iostream"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "config_gui.h"
#include "spcore/coreruntime.h"
#include "spcore/component.h"
#include "spcore/module.h"
#include "spcore/basictypes.h"
using namespace spcore;

////@begin XPM images
////@end XPM images


/*
 * CollageConfigurationGUI type definition
 */

IMPLEMENT_DYNAMIC_CLASS( CollageConfigurationGUI, wxPanel )


/*
 * CollageConfigurationGUI event table definition
 */

BEGIN_EVENT_TABLE( CollageConfigurationGUI, wxPanel )

////@begin CollageConfigurationGUI event table entries
    EVT_INIT_DIALOG( CollageConfigurationGUI::OnInitDialog )

    EVT_FILEPICKER_CHANGED( ID_FILEPICKERCTRL, CollageConfigurationGUI::OnFilepickerctrlFilePickerChanged )

    EVT_SLIDER( ID_SLIDER_SENS, CollageConfigurationGUI::OnSliderSensUpdated )

    EVT_SLIDER( ID_SLIDER_DEADZONE, CollageConfigurationGUI::OnSliderDeadzoneUpdated )

    EVT_SLIDER( ID_SLIDER_MAXIMUM, CollageConfigurationGUI::OnSliderMaximumUpdated )

    EVT_SLIDER( ID_SLIDER_SPEED, CollageConfigurationGUI::OnSliderSpeedUpdated )

    EVT_CHECKBOX( ID_CHECKBOX_VANISH, CollageConfigurationGUI::OnCheckboxVanishClick )

    EVT_BUTTON( ID_BUTTON_PREV, CollageConfigurationGUI::OnButtonPrevClick )

    EVT_BUTTON( ID_BUTTON_NEXT, CollageConfigurationGUI::OnButtonNextClick )

////@end CollageConfigurationGUI event table entries

END_EVENT_TABLE()


/*
 * CollageConfigurationGUI constructors
 */

CollageConfigurationGUI::CollageConfigurationGUI()
{
    Init();
}

CollageConfigurationGUI::CollageConfigurationGUI( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, pos, size, style);
}


/*
 * ColageConfigurationGUI creator
 */

bool CollageConfigurationGUI::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
////@begin CollageConfigurationGUI creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end CollageConfigurationGUI creation
    return true;
}


/*
 * CollageConfigurationGUI destructor
 */

CollageConfigurationGUI::~CollageConfigurationGUI()
{
}


/*
 * Member initialisation
 */

void CollageConfigurationGUI::Init()
{
////@begin CollageConfigurationGUI member initialisation
    m_file = NULL;
    m_sensibility = NULL;
    m_SensibilityValue = NULL;
    m_deadZone = NULL;
    m_DeadZoneValue = NULL;
    m_maximum = NULL;
    m_MaximumValue = NULL;
    m_speedAnimation = NULL;
    m_SpeedAnimationValue = NULL;
    m_chkVanish = NULL;
////@end CollageConfigurationGUI member initialisation
}


/*
 * Control creation for ColageConfigurationGUI
 */

void CollageConfigurationGUI::CreateControls()
{    
////@begin CollageConfigurationGUI content construction
    CollageConfigurationGUI* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemPanel1->SetSizer(itemBoxSizer2);

    wxFlexGridSizer* itemFlexGridSizer3 = new wxFlexGridSizer(0, 2, 0, 0);
    itemBoxSizer2->Add(itemFlexGridSizer3, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    wxStaticText* itemStaticText4 = new wxStaticText( itemPanel1, wxID_STATIC, _("File:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText4, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_file = new wxFilePickerCtrl( itemPanel1, ID_FILEPICKERCTRL, wxEmptyString, _("Please select the XML configuration file for Colage."), _T("*.xml"), wxDefaultPosition, wxDefaultSize, wxFLP_DEFAULT_STYLE|wxFLP_OPEN|wxFLP_FILE_MUST_EXIST|wxFLP_CHANGE_DIR );
    itemFlexGridSizer3->Add(m_file, 1, wxGROW|wxGROW|wxALL, 5);

    wxStaticText* itemStaticText6 = new wxStaticText( itemPanel1, wxID_STATIC, _("Sensibility:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText6, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    itemFlexGridSizer3->Add(itemBoxSizer7, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_sensibility = new wxSlider( itemPanel1, ID_SLIDER_SENS, 500, 0, 1000, wxDefaultPosition, wxSize(250, -1), wxSL_HORIZONTAL );
    itemBoxSizer7->Add(m_sensibility, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_SensibilityValue = new wxStaticText( itemPanel1, wxID_STATIC, _("Static text"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer7->Add(m_SensibilityValue, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText10 = new wxStaticText( itemPanel1, wxID_STATIC, _("Dead Zone:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText10, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer11 = new wxBoxSizer(wxHORIZONTAL);
    itemFlexGridSizer3->Add(itemBoxSizer11, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_deadZone = new wxSlider( itemPanel1, ID_SLIDER_DEADZONE, 0, 0, 100, wxDefaultPosition, wxSize(250, -1), wxSL_HORIZONTAL );
    itemBoxSizer11->Add(m_deadZone, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_DeadZoneValue = new wxStaticText( itemPanel1, wxID_STATIC, _("Static text"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer11->Add(m_DeadZoneValue, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText14 = new wxStaticText( itemPanel1, wxID_STATIC, _("Maximum:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText14, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer15 = new wxBoxSizer(wxHORIZONTAL);
    itemFlexGridSizer3->Add(itemBoxSizer15, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_maximum = new wxSlider( itemPanel1, ID_SLIDER_MAXIMUM, 0, 1, 100, wxDefaultPosition, wxSize(250, -1), wxSL_HORIZONTAL|wxSL_TOP );
    itemBoxSizer15->Add(m_maximum, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_MaximumValue = new wxStaticText( itemPanel1, wxID_STATIC, _("Static text"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer15->Add(m_MaximumValue, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticText* itemStaticText18 = new wxStaticText( itemPanel1, wxID_STATIC, _("Animation speed:"), wxDefaultPosition, wxDefaultSize, 0 );
    itemFlexGridSizer3->Add(itemStaticText18, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer19 = new wxBoxSizer(wxHORIZONTAL);
    itemFlexGridSizer3->Add(itemBoxSizer19, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_speedAnimation = new wxSlider( itemPanel1, ID_SLIDER_SPEED, 87, 0, 99, wxDefaultPosition, wxSize(250, -1), wxSL_HORIZONTAL|wxSL_TOP );
    itemBoxSizer19->Add(m_speedAnimation, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_SpeedAnimationValue = new wxStaticText( itemPanel1, wxID_STATIC, _("Static text"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer19->Add(m_SpeedAnimationValue, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    itemFlexGridSizer3->Add(5, 5, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkVanish = new wxCheckBox( itemPanel1, ID_CHECKBOX_VANISH, _("Automatically vanish objects"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkVanish->SetValue(false);
    itemFlexGridSizer3->Add(m_chkVanish, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxBoxSizer* itemBoxSizer24 = new wxBoxSizer(wxHORIZONTAL);
    itemBoxSizer2->Add(itemBoxSizer24, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

    wxButton* itemButton25 = new wxButton( itemPanel1, ID_BUTTON_PREV, _("<  Prev"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer24->Add(itemButton25, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxButton* itemButton26 = new wxButton( itemPanel1, ID_BUTTON_NEXT, _("Next  >"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer24->Add(itemButton26, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

////@end CollageConfigurationGUI content construction
}


/*
 * wxEVT_INIT_DIALOG event handler for ID_COLAGECONFIGURATION
 */

void CollageConfigurationGUI::OnInitDialog( wxInitDialogEvent& event )
{
	InitPanel();
	
	event.Skip(false);
}

void CollageConfigurationGUI::InitPanel(  )
{
	SmartPtr<const CTypeFloat> floatValue=sptype_dynamic_cast<const CTypeFloat>(m_pinSensibility->Read());
	m_SensibilityValue->SetLabel(wxString::Format(_T("%.2f"),floatValue->getValue()));
	m_sensibility->SetValue(floatValue->getValue()*500);
	
	floatValue=sptype_dynamic_cast<const CTypeFloat>(m_pinDeadZone->Read());
	m_DeadZoneValue->SetLabel(wxString::Format(_T("%.2f"),floatValue->getValue()));
	m_deadZone->SetValue(floatValue->getValue()*100);
	
	SmartPtr<const CTypeInt> intValue=sptype_dynamic_cast<const CTypeInt>(m_pinMaximum->Read());	
	m_MaximumValue->SetLabel(wxString::Format(_T("%d"),intValue->getValue()));
	m_maximum->SetValue(intValue->getValue());
	
	floatValue=sptype_dynamic_cast<const CTypeFloat>(m_pinSpeedAnimation->Read());
	m_SpeedAnimationValue->SetLabel(wxString::Format(_T("%.2f"),floatValue->getValue()));
	m_speedAnimation->SetValue((floatValue->getValue()*33));

	SmartPtr<const CTypeBool> boolValue=sptype_dynamic_cast<const CTypeBool>(m_pinVanish->Read());
	m_chkVanish->SetValue(boolValue->getValue());
}

/*
 * wxEVT_FILEPICKER_CHANGED event handler for ID_FILEPICKERCTRL
 */

void CollageConfigurationGUI::OnFilepickerctrlFilePickerChanged( wxFileDirPickerEvent& event )
{
	SmartPtr<CTypeString> file= spcore::CTypeString::CreateInstance();
	
	file->set((char*)m_file->GetPath().char_str());
	m_pinFile->Send(file);
	
    event.Skip(false);
}


/*
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_SENS
 */

void CollageConfigurationGUI::OnSliderSensUpdated( wxCommandEvent& event )
{
	m_SensibilityValue->SetLabel(wxString::Format(_T("%.2f"),((float)m_sensibility->GetValue())/500));
	SmartPtr<CTypeFloat> floatValue=spcore::CTypeFloat::CreateInstance();
	floatValue->setValue(((float) m_sensibility->GetValue())/500);
	m_pinSensibility->Send(floatValue);

    event.Skip(false);
}

/*
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_DEADZONE
 */

void CollageConfigurationGUI::OnSliderDeadzoneUpdated( wxCommandEvent& event )
{
	m_DeadZoneValue->SetLabel(wxString::Format(_T("%.2f"),((float)m_deadZone->GetValue())/100));
	SmartPtr<CTypeFloat> floatValue=spcore::CTypeFloat::CreateInstance();
	floatValue->setValue(((float) m_deadZone->GetValue())/101.0f+0.01f);
	m_pinDeadZone->Send(floatValue);
	
    event.Skip(false); 
}


/*
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_MAXIMUM
 */

void CollageConfigurationGUI::OnSliderMaximumUpdated( wxCommandEvent& event )
{
	m_MaximumValue->SetLabel(wxString::Format(_T("%d"),m_maximum->GetValue()));
	SmartPtr<CTypeInt> intValue= spcore::CTypeInt::CreateInstance();
	intValue->setValue(m_maximum->GetValue());
	m_pinMaximum->Send(intValue);
	    
    event.Skip(false); 
}

/*
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_SPEED
 */

void CollageConfigurationGUI::OnSliderSpeedUpdated( wxCommandEvent& event )
{
	m_SpeedAnimationValue->SetLabel(wxString::Format(_T("%.2f"),((float) m_speedAnimation->GetValue())/33));
	SmartPtr<CTypeFloat> floatValue=spcore::CTypeFloat::CreateInstance();
	floatValue->setValue(((float) m_speedAnimation->GetValue())/33);
	m_pinSpeedAnimation->Send(floatValue);
    event.Skip(false);
}

/*
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_VANISH
 */

void CollageConfigurationGUI::OnCheckboxVanishClick( wxCommandEvent& event )
{
	SmartPtr<CTypeBool> boolValue=spcore::CTypeBool::CreateInstance();
	boolValue->setValue(event.IsChecked());
	m_pinVanish->Send(boolValue);
	event.Skip(false);
}

/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_PREV
 */

void CollageConfigurationGUI::OnButtonPrevClick( wxCommandEvent& event )
{
	SmartPtr<CTypeBool> boolValue=spcore::CTypeBool::CreateInstance();
	boolValue->setValue(false);
	m_pinChangeScene->Send(boolValue);

    event.Skip(false);
}

/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_NEXT
 */

void CollageConfigurationGUI::OnButtonNextClick( wxCommandEvent& event )
{
	SmartPtr<CTypeBool> boolValue=spcore::CTypeBool::CreateInstance();
	boolValue->setValue(true);
	m_pinChangeScene->Send(boolValue);

    event.Skip(false);
}



/*
 * Should we show tooltips?
 */

bool CollageConfigurationGUI::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap CollageConfigurationGUI::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin CollageConfigurationGUI bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end CollageConfigurationGUI bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon CollageConfigurationGUI::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin CollageConfigurationGUI icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end CollageConfigurationGUI icon retrieval
}




