type virtualinstrument
name _"Playing with voice + Virtual Instrument"

#
# Inputs
#

# Camera source
create camera_grabber grabber

# Background drawer
import lib/ipl2sdl.sps
create ipl2sdl_chk ipl2sdl_chk
connect grabber image ipl2sdl_chk image

# Camera motion tracker
import lib/camera_oflow_motion.sps
create camera_oflow_motion cam_tracker
connect grabber image cam_tracker image
# Enable checkbox
create fmul cam_enable
create widget_checkbox chk_enable_cam -v true -l _"Enable camera control"
create fcast cam_enable_cast
connect chk_enable_cam value cam_enable_cast in
connect cam_enable_cast out cam_enable b
connect cam_tracker motion cam_enable a

# Playing with the voice
import lib/pvoice.sps
create playingwithvoice pvoice
# Enable checkbox
create fmul pvoice_enable
create widget_checkbox chk_enable_voice -v true -l _"Enable voice control"
create fcast pvoice_enable_cast
connect chk_enable_voice value pvoice_enable_cast in
connect pvoice_enable_cast out pvoice_enable b
connect pvoice out pvoice_enable a

#
# Master sensitivity
#
create widget_slider sld_sens_master -l _"Master sensitivity" --min 0 --max 2 -v 1

#
# MIDI player
#
import lib/motion_midi_player.sps
create motion_midi_player player
create fmul sens_midi
connect sld_sens_master value sens_midi b
connect cam_enable result sens_midi a
connect sens_midi result player motion

#
# Collage
#
import lib/motion_collage.sps
create motion_collage motion_collage
connect ipl2sdl_chk surface motion_collage background

create send_main_async to_main

# Accumulate motion
# Motion from cam is sent to the main thread and is the one
# input that provides synchonization.
create fadd cam_voice_add
connect pvoice_enable result cam_voice_add b
connect cam_enable result cam_voice_add a
create fmul cam_voice_sens
connect sld_sens_master value cam_voice_sens b
connect cam_voice_add result cam_voice_sens a
connect cam_voice_sens result to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox
			layout_begin book
				layout_begin book_page _"Camera Tracker"
					component chk_enable_cam
					component cam_tracker
					component ipl2sdl_chk
				layout_end
				layout_begin book_page _"Voice Input"
					component chk_enable_voice
					component pvoice
				layout_end
			layout_end
#			component sld_sens_master
		layout_end
		layout_begin vbox _"MIDI Sound"
			component player
		layout_end
		layout_begin vbox _"Graphics"
			component motion_collage			
		layout_end
	layout_end
end_gui_layout