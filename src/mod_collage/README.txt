mod_collage:
	mod_collage.cpp=> Implementació del mod_collage, Utilitza els kernels com a nucli per mostrar les dades, cada kernel comporta un comportament diferent 
	config_gui.cpp=> Configuració del mod_collage
	kernel: Conté la el nucli o comportament del mod_collage, son els que s'encarreguen de fer la feina de dir quines imatges s'han de mostrar i com s'efectuen les "transicions"
		kernel.h=> Conte la declaració dels 2 kernels i l'abstracció.
		kernel.cpp=> conte la implementació dels 2 kernels, mes la clase abstracta dels kernels que necessita, i també té les 3 clases de tipus delay i la seva abstracció que son utilitzades per el kernel ciclic. (ho separare en breus)
	Pictures: Conté la clase PictureNode i transition, que son les clases que utilitzem per modificar les imatges i saber quines son les que s'estan mostrant
		Pictures.h => Conteé la declaració de la clase PictureNode, i les transicions + la clase abstracte de les transicions
		Pictures.cpp => Conté la clase PictureNode
		Factories.h => Conté la declaració de les factories per les diferents transicions, s'utilitzen, per carregar el XML, i generar en ram un mapa del contingut del XML, facilment transformable al que necessitem.
		Factories.cpp=> Conte la implementacio de les Factories.h
		Transition.cpp=> Conté l'implementació de totes les transicions.
	XMLImplementation: Conté les classes necess�ries per transformar el codi XML en objectes utilitzables pel codi.
		InterfaceXML.h => Cont� la declaraci� de les classes Error, LogError, XMLHandler i LoadError. Serveixen per traduir l'XML.
		LogError.cpp => Implementa les classes Error y LogError. La classe Error guarda un error de qualsevol tipus amb la corresponent informaci�: Codi d'error, element, attribut i l�nia on s'ha produ�t l'error i un missatge. La classe LogError es un registre de tots els errors produ�ts.
		XMLHandler.cpp => Implementa la classe del mateix nom. �s l'encarregada de tradu�r pr�piament els tags de l'XML a objectes. Al mateix temps s'encarrega de comprovar els errors que pot haver-hi a l'XML.
		Implementation.h => Cont� la declaraci� de les classes Activity, Module, Picture i DBImages. Les tres primeres representen els diferents elements de l'XML, excepte pel tag 'transition' que vam estimar que amb una factoria de transicions era suficient i no necessitavem una classe Transici�.
		La classe DBImages, s'encarrega d'emmagatzemar les imatges carrgades. Quan es demana una imatge, si no hi �s a la base de dades es carrega, es guarda i es retorna, i si hi �s es retorna la imatge ja guardada. La implementaci� d'aquestes classes es troba als arxius amb el mateix nom que la classe: Activity.cpp, Module.cpp, Picture.cpp i DBImages.cpp.