#
# wii_bb_motion_position
#
# Gets motion and position from the balance board
#
# Output pins (values are sent in the same order)
#  x_pos (float) (range -1 to 1)
#  y_pos (float) (range -1 to 1)
#  x_motion (float)
#  y_motion (float)
#

type wii_bb_motion_position
name "Wii Balance Board"

# Wii input
create wiimotes_input wii
create wii_bb_to_composite convert
connect wii balance_board convert in

# Split output
create split split -o 2
connect convert out split input

# Apply offset to obtain (x, y) around (0,0)
create fadd xpos -v 0.0
connect split 1 xpos a
create fadd ypos -v 0.0
connect split 2 ypos a

#
# Motion is computed as differences between consecutive samples
#
create fsub xmotion
# First send to a input which provoques the full evaluation
connect xpos result xmotion a
# Secondly store the "old" value to the fsub component
connect xpos result xmotion b

create fsub ymotion
# First send to a input which provoques the full evaluation
connect ypos result ymotion a
# Secondly store the "old" value to the fsub component
connect ypos result ymotion b

#
# Export output pins
#
export_opin xpos result x_pos
export_opin ypos result y_pos
export_opin xmotion result x_motion
export_opin ymotion result y_motion
