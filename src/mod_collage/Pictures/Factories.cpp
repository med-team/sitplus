/*
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Factories.h"

using boost::shared_ptr;

namespace Pictures{
	ITransitionFactory::~ITransitionFactory(){}

	IdentityTransitionFactory::~IdentityTransitionFactory(){}
	
	shared_ptr<PicturesTransition> IdentityTransitionFactory::getTransition(shared_ptr<PictureNode> pn){
		return shared_ptr<PicturesTransition>(new PicturesTransition(pn));
	}
	
	AlphaTransitionFactory::~AlphaTransitionFactory(){}
	
	shared_ptr<PicturesTransition> AlphaTransitionFactory::getTransition(shared_ptr<PictureNode> pn){return shared_ptr<PicturesTransition>(new AlphaTransition(pn));}
	
	ScaleTransitionFactory::~ScaleTransitionFactory(){}
	
	shared_ptr<PicturesTransition> ScaleTransitionFactory::getTransition(shared_ptr<PictureNode> pn){return shared_ptr<PicturesTransition>(new ScaleTransition(pn));}
	
	RotateTransitionFactory::~RotateTransitionFactory(){}

	RotateTransitionFactory::RotateTransitionFactory(int degrees){m_degrees = degrees;}
	
	shared_ptr<PicturesTransition> RotateTransitionFactory::getTransition(shared_ptr<PictureNode> pn){return shared_ptr<PicturesTransition>(new RotateTransition(pn, m_degrees));}
	
	ChangePictureTransitionFactory::~ChangePictureTransitionFactory(){}
	
	ChangePictureTransitionFactory::ChangePictureTransitionFactory(SmartPtr<mod_sdl::CTypeSDLSurface> surf){m_destinationSurf=surf;}
		
	shared_ptr<PicturesTransition> ChangePictureTransitionFactory::getTransition(shared_ptr<PictureNode> pn){
		return shared_ptr<PicturesTransition>(new ChangePictureTransition(pn,m_destinationSurf));
	}
	
	TranslateTransitionFactory::~TranslateTransitionFactory(){}

	TranslateTransitionFactory::TranslateTransitionFactory(float x, float y){
		m_x=x;
		m_y=y;
	}
	
	shared_ptr<PicturesTransition> TranslateTransitionFactory::getTransition(shared_ptr<PictureNode> pn){
		return shared_ptr<PicturesTransition>(new TranslatePictureTransition(pn, m_x, m_y));
	}
	
	VibratePictureTransitionFactory::~VibratePictureTransitionFactory(){}
	
	VibratePictureTransitionFactory::VibratePictureTransitionFactory(float percent){
		m_percent=percent;
	}
	
	shared_ptr<PicturesTransition> VibratePictureTransitionFactory::getTransition(shared_ptr<PictureNode> pn){
		return shared_ptr<PicturesTransition>(new VibratePackagePictureTransition(shared_ptr<PicturesTransition>(new ScaleTransition(pn)), m_percent));
	}
	
	RandomTransitionFactory::~RandomTransitionFactory(){}
	
	shared_ptr<PicturesTransition> RandomTransitionFactory::getTransition(shared_ptr<PictureNode> pn){
		shared_ptr<PicturesTransition> pt;
		VibratePictureTransitionFactory vptf(0.3f);
		TranslateTransitionFactory ttf(0,0);
		unsigned int randFact = rand()%5;
		switch(randFact){
			case 0:
				pt =  shared_ptr<PicturesTransition>(new AlphaTransition(pn));
				break;
			case 1:
				pt = shared_ptr<PicturesTransition>(new ScaleTransition(pn));
				break;
			case 2:
				pt = shared_ptr<PicturesTransition>(new RotateTransition(pn,90));
				break;
			case 3:
				pt = vptf.getTransition(pn);
				break;
			case 4:
				pt = ttf.getTransition(pn);
				break;
		}
		return pt;
	}
	
	
	
}