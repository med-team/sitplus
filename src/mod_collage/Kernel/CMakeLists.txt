cmake_minimum_required(VERSION 2.6)
project(mod_Kernel)

set(mod_Kernel_SRCS
	kernel.h
	kernel.cpp
	delay.h
)

# Base library
add_library (mod_Kernel STATIC ${mod_Kernel_SRCS})
target_link_libraries(mod_Kernel spcore)
