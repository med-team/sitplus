/////////////////////////////////////////////////////////////////////////////
// Name:        mdichildcontainer.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     04/04/2011 21:30:07
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "widgets_base/mdichildcontainer.h"

////@begin XPM images
////@end XPM images

namespace widgets_base {

/*
 * MDIChildContainer type definition
 */

IMPLEMENT_CLASS( MDIChildContainer, wxMDIChildFrame )


/*
 * MDIChildContainer event table definition
 */

BEGIN_EVENT_TABLE( MDIChildContainer, wxMDIChildFrame )

////@begin MDIChildContainer event table entries
    EVT_SIZE( MDIChildContainer::OnSize )

////@end MDIChildContainer event table entries

END_EVENT_TABLE()


/*
 * MDIChildContainer constructors
 */

MDIChildContainer::MDIChildContainer()
{
    Init();
}

MDIChildContainer::MDIChildContainer( wxMDIParentFrame* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*
 * MDIChildContainer creator
 */

bool MDIChildContainer::Create( wxMDIParentFrame* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin MDIChildContainer creation
    wxMDIChildFrame::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end MDIChildContainer creation
    return true;
}


/*
 * MDIChildContainer destructor
 */

MDIChildContainer::~MDIChildContainer()
{
////@begin MDIChildContainer destruction
////@end MDIChildContainer destruction
}


/*
 * Member initialisation
 */

void MDIChildContainer::Init()
{
////@begin MDIChildContainer member initialisation
    m_sizer = NULL;
////@end MDIChildContainer member initialisation
}


/*
 * Control creation for MDIChildContainer
 */

void MDIChildContainer::CreateControls()
{    
////@begin MDIChildContainer content construction
    MDIChildContainer* itemMDIChildFrame1 = this;

    m_sizer = new wxBoxSizer(wxVERTICAL);
    itemMDIChildFrame1->SetSizer(m_sizer);

////@end MDIChildContainer content construction
}


/*
 * Should we show tooltips?
 */

bool MDIChildContainer::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap MDIChildContainer::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin MDIChildContainer bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end MDIChildContainer bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon MDIChildContainer::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin MDIChildContainer icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end MDIChildContainer icon retrieval
}

void MDIChildContainer::AddSitplusPanel (wxWindow* panel)
{
	m_sizer->Add (panel, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 0);	
	m_sizer->SetSizeHints(this);
	SetTitle (panel->GetName());
	//Layout();
	//Fit();
}


/*
 * wxEVT_SIZE event handler for ID_MDICHILDCONTAINER
 */

void MDIChildContainer::OnSize( wxSizeEvent& event )
{
	Layout();
	Fit();

	event.Skip (false);
}

};