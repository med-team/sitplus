cmake_minimum_required(VERSION 2.6)
project(mod_XMLImplementation)

set(mod_XMLImplementation_SRCS
	Implementation.h
	Picture.cpp
	Module.cpp
	Activity.cpp
	DBImages.cpp
	InterfaceXML.h
	LogError.cpp
	LoadXML.cpp
	XmlHandler.cpp
)

# Base library
add_library (mod_XMLImplementation STATIC ${mod_XMLImplementation_SRCS})
target_link_libraries(mod_XMLImplementation spcore)
