/////////////////////////////////////////////////////////////////////////////
// Name:        midiconfiggui.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     01/04/2011 18:13:37
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

#ifndef _MIDICONFIGGUI_H_
#define _MIDICONFIGGUI_H_


/*!
 * Includes
 */
#include "spcore/component.h"

#include <wx/panel.h>

////@begin includes
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations
class wxChoice;

namespace mod_midi {
/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_MIDICONFIGGUI 10000
#define ID_CHOICE_MIDI_OUT 10001
#define ID_BUTTON_MIDI_TEST 10002
#define SYMBOL_MIDICONFIGGUI_STYLE wxCAPTION|wxTAB_TRAVERSAL
#define SYMBOL_MIDICONFIGGUI_TITLE _("MIDI Config")
#define SYMBOL_MIDICONFIGGUI_IDNAME ID_MIDICONFIGGUI
#define SYMBOL_MIDICONFIGGUI_SIZE wxSize(400, 300)
#define SYMBOL_MIDICONFIGGUI_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * MIDIConfigGui class declaration
 */

class MIDIConfigGui: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( MIDIConfigGui )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    MIDIConfigGui();
    MIDIConfigGui( wxWindow* parent, wxWindowID id = SYMBOL_MIDICONFIGGUI_IDNAME, const wxPoint& pos = SYMBOL_MIDICONFIGGUI_POSITION, const wxSize& size = SYMBOL_MIDICONFIGGUI_SIZE, long style = SYMBOL_MIDICONFIGGUI_STYLE, const wxString& name = SYMBOL_MIDICONFIGGUI_TITLE);

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_MIDICONFIGGUI_IDNAME, const wxPoint& pos = SYMBOL_MIDICONFIGGUI_POSITION, const wxSize& size = SYMBOL_MIDICONFIGGUI_SIZE, long style = SYMBOL_MIDICONFIGGUI_STYLE, const wxString& name = SYMBOL_MIDICONFIGGUI_TITLE);

    /// Destructor
    ~MIDIConfigGui();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin MIDIConfigGui event handler declarations

    /// wxEVT_CLOSE_WINDOW event handler for ID_MIDICONFIGGUI
    void OnCloseWindow( wxCloseEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_MIDI_TEST
    void OnButtonMidiTestClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
    void OnOkClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
    void OnCancelClick( wxCommandEvent& event );

////@end MIDIConfigGui event handler declarations

////@begin MIDIConfigGui member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end MIDIConfigGui member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin MIDIConfigGui member variables
    wxChoice* m_choMidiOut;
////@end MIDIConfigGui member variables

	SmartPtr<spcore::IComponent> m_midiConfigComponent;
};

};

#endif
    // _MIDICONFIGGUI_H_
