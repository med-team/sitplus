/////////////////////////////////////////////////////////////////////////////
// Name:        activityloader.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     08/06/2011 11:39:47
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "activityloader.h"

////@begin XPM images
#include "bitmaps/sitplus_logo_16x16.xpm"
////@end XPM images


/*
 * ActivityLoader type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ActivityLoader, wxDialog )


/*
 * ActivityLoader event table definition
 */

BEGIN_EVENT_TABLE( ActivityLoader, wxDialog )

////@begin ActivityLoader event table entries
    EVT_CHECKBOX( ID_CHECKBOX_CAM, ActivityLoader::OnCheckboxCamClick )

    EVT_CHECKBOX( ID_CHECKBOX_VOICE, ActivityLoader::OnCheckboxVoiceClick )

    EVT_CHECKBOX( ID_CHECKBOX_WII, ActivityLoader::OnCheckboxWiiClick )

    EVT_CHECKBOX( ID_CHECKBOX_USE_MOTION_PLUS, ActivityLoader::OnCheckboxUseMotionPlusClick )

    EVT_BUTTON( wxID_OK, ActivityLoader::OnOkClick )

    EVT_BUTTON( wxID_CANCEL, ActivityLoader::OnCancelClick )

////@end ActivityLoader event table entries

END_EVENT_TABLE()


/*
 * ActivityLoader constructors
 */

ActivityLoader::ActivityLoader()
{
    Init();
}

ActivityLoader::ActivityLoader( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, caption, pos, size, style);
}


/*
 * ActivityLoader creator
 */

bool ActivityLoader::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin ActivityLoader creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxDialog::Create( parent, id, caption, pos, size, style );

    CreateControls();
    SetIcon(GetIconResource(wxT("bitmaps/sitplus_logo_16x16.xpm")));
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end ActivityLoader creation
    return true;
}


/*
 * ActivityLoader destructor
 */

ActivityLoader::~ActivityLoader()
{
////@begin ActivityLoader destruction
////@end ActivityLoader destruction
}


/*
 * Member initialisation
 */

void ActivityLoader::Init()
{
	m_selection= 1;
////@begin ActivityLoader member initialisation
    m_chkCam = NULL;
    m_chkVoice = NULL;
    m_chkWii = NULL;
    m_chkUseMotionPlus = NULL;
////@end ActivityLoader member initialisation
}


/*
 * Control creation for ActivityLoader
 */

void ActivityLoader::CreateControls()
{    
////@begin ActivityLoader content construction
    ActivityLoader* itemDialog1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemDialog1->SetSizer(itemBoxSizer2);

    wxStaticBox* itemStaticBoxSizer3Static = new wxStaticBox(itemDialog1, wxID_ANY, _("Select input channels"));
    wxStaticBoxSizer* itemStaticBoxSizer3 = new wxStaticBoxSizer(itemStaticBoxSizer3Static, wxVERTICAL);
    itemBoxSizer2->Add(itemStaticBoxSizer3, 0, wxGROW|wxALL, 5);

    m_chkCam = new wxCheckBox( itemDialog1, ID_CHECKBOX_CAM, _("Camera tracker"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkCam->SetValue(true);
    itemStaticBoxSizer3->Add(m_chkCam, 0, wxALIGN_LEFT|wxALL, 5);

    m_chkVoice = new wxCheckBox( itemDialog1, ID_CHECKBOX_VOICE, _("Voice"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkVoice->SetValue(false);
    itemStaticBoxSizer3->Add(m_chkVoice, 0, wxALIGN_LEFT|wxALL, 5);

    m_chkWii = new wxCheckBox( itemDialog1, ID_CHECKBOX_WII, _("Wii Remote"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkWii->SetValue(false);
    itemStaticBoxSizer3->Add(m_chkWii, 0, wxALIGN_LEFT|wxALL, 5);

    wxBoxSizer* itemBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    itemStaticBoxSizer3->Add(itemBoxSizer7, 0, wxALIGN_LEFT|wxALL, 5);

    itemBoxSizer7->Add(5, 5, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    m_chkUseMotionPlus = new wxCheckBox( itemDialog1, ID_CHECKBOX_USE_MOTION_PLUS, _("Use Motion Plus"), wxDefaultPosition, wxDefaultSize, 0 );
    m_chkUseMotionPlus->SetValue(false);
    m_chkUseMotionPlus->Enable(false);
    itemBoxSizer7->Add(m_chkUseMotionPlus, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStdDialogButtonSizer* itemStdDialogButtonSizer10 = new wxStdDialogButtonSizer;

    itemBoxSizer2->Add(itemStdDialogButtonSizer10, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);
    wxButton* itemButton11 = new wxButton( itemDialog1, wxID_OK, _("&OK"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer10->AddButton(itemButton11);

    wxButton* itemButton12 = new wxButton( itemDialog1, wxID_CANCEL, _("&Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer10->AddButton(itemButton12);

    itemStdDialogButtonSizer10->Realize();

////@end ActivityLoader content construction
}


/*
 * Should we show tooltips?
 */

bool ActivityLoader::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ActivityLoader::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ActivityLoader bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end ActivityLoader bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ActivityLoader::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ActivityLoader icon retrieval
    wxUnusedVar(name);
    if (name == _T("bitmaps/sitplus_logo_16x16.xpm"))
    {
        wxIcon icon(sitplus_logo_16x16);
        return icon;
    }
    return wxNullIcon;
////@end ActivityLoader icon retrieval
}


/*
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_CAM
 */

void ActivityLoader::OnCheckboxCamClick( wxCommandEvent& event )
{
	unsigned int newSelection= m_selection;

	if (event.IsChecked()) m_selection|= INPUT_CAM;
	else {
		newSelection&= ~INPUT_CAM;
		if (newSelection) m_selection= newSelection;
		else { 
			m_chkVoice->SetValue(true);
			m_selection= INPUT_VOICE;
		}
	}

    event.Skip(false);
}


/*
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_VOICE
 */

void ActivityLoader::OnCheckboxVoiceClick( wxCommandEvent& event )
{
	unsigned int newSelection= m_selection;

	if (event.IsChecked()) m_selection|= INPUT_VOICE;
	else {
		newSelection&= ~INPUT_VOICE;
		if (newSelection) m_selection= newSelection;
		else {
			m_chkWii->SetValue(true);
			m_chkUseMotionPlus->Enable(true);
			m_selection= INPUT_WII;
		}
	}

    event.Skip(false);
}


/*
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_WII
 */

void ActivityLoader::OnCheckboxWiiClick( wxCommandEvent& event )
{
	unsigned int newSelection= m_selection;

	if (event.IsChecked()) {
		m_selection|= INPUT_WII;
		m_chkUseMotionPlus->Enable(true);
	}
	else {
		newSelection&= ~INPUT_WII;
		m_chkUseMotionPlus->Enable(false);
		if (newSelection) m_selection= newSelection;
		else {			
			m_chkCam->SetValue(true);
			m_selection= INPUT_CAM;
		}
	}

    event.Skip(false);
}

bool ActivityLoader::GetUseMotionPlus() const { 
	return m_chkUseMotionPlus->IsChecked();
}

/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
 */

void ActivityLoader::OnOkClick( wxCommandEvent& event )
{
////@begin wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK in ActivityLoader.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK in ActivityLoader. 
}


/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
 */

void ActivityLoader::OnCancelClick( wxCommandEvent& event )
{
////@begin wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL in ActivityLoader.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL in ActivityLoader. 
}


/*
 * wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_USE_MOTION_PLUS
 */

void ActivityLoader::OnCheckboxUseMotionPlusClick( wxCommandEvent& event )
{
////@begin wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_USE_MOTION_PLUS in ActivityLoader.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_USE_MOTION_PLUS in ActivityLoader. 
}

