/**
* @file		langutils.h
* @brief	i18n utility functions.
* @author	Cesar Mauri Loba
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPHOST_LANGUTILS_H
#define SPHOST_LANGUTILS_H

#include "config.h"

#ifdef ENABLE_NLS

#include <string>

#include "sphost/libimpexp_sphost.h"

namespace sphost {


/**
	@brief Open a language selection dialog.
	@param lang Language identifier
	@return false if the user cancelled, true otherwise
	@remarks wx must has been previously initialized.
*/
SPIMPEXP_CLASS_HOST
bool LanguageSelectionDialog(std::string& lang);

/**
	@brief Try to load the stored language setting if possible.
	@param lang Language identifier
	@return true if language has been found, false otherwise	
*/
SPIMPEXP_CLASS_HOST
bool LoadLanguageFromConfiguration(std::string& lang);

/*!
	@brief Save the language setting
	@param lang Language identifier
	@return true if language has been found, false otherwise	
*/
SPIMPEXP_CLASS_HOST
bool SaveLanguageToConfiguration(const std::string& lang);

};

#endif	// ENABLE_NLS

#endif	// SPHOST_LANGUTILS_H