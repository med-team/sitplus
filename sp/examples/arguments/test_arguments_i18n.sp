#
# Test and sample on how to use arguments
#

type test_arguments_new
name 'test_arguments_new'

import test_arguments.sps

create test_arguments test1 -a \'_"Hello world!"\' -b bb -c cc

# TODO: currently does not work. Not handled properly
#create test_arguments test2 -a \'_"Hello \"\"world\"!"\' -b bb -c cc

create print print _"Simple message!"