type virtualinstrument
name _"Virtual Instrument"

# Camera source
create camera_grabber grabber

# Background drawer
import lib/ipl2sdl.sps
create ipl2sdl_chk ipl2sdl_chk
connect grabber image ipl2sdl_chk image

# Motion tracker
import lib/camera_oflow_motion.sps
create camera_oflow_motion tracker
connect grabber image tracker image

# MIDI player
import lib/motion_midi_player.sps
create motion_midi_player player
connect tracker motion player motion

# Collage
import lib/motion_collage.sps
create motion_collage motion_collage

connect ipl2sdl_chk surface motion_collage background

create send_main_async to_main
connect tracker motion to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox _"Camera Tracker"
			component tracker
			component ipl2sdl_chk
		layout_end
		layout_begin vbox _"MIDI Sound"
			component player
		layout_end
		layout_begin vbox _"Graphics"
			component motion_collage			
		layout_end
	layout_end
end_gui_layout
