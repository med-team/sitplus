#
# Limit the number of activations by time
#
#	True values received at input are only sent to output
#	only when the specified time span has passed.
#
# Input pins
#	in (bool)
#	time (int)	- in ms
#
# Output pins
# 	out (bool)
#
# Arguments
#	-t <time>		Time in ms

type filter_values_by_time
name "Filter values by time"
args -t

# Input pins
create forward forward_in
export_ipin forward_in in in bool
create forward forward_time
export_ipin forward_time in time int

# Chrono
create chrono chrono
connect forward_in out chrono read

# Comparator
create igt igt -v $-t$
connect forward_time out igt b
connect chrono elapsed igt a

# Gate controlled by the comparator
create forward cmp_gate
connect igt result cmp_gate gate

# Connect input to gate
connect forward_in out cmp_gate in

# Gate to let flow only true values
create forward true_gate
connect cmp_gate out true_gate gate
connect cmp_gate out true_gate in

# Connect when a true value flows out reset chrono
connect true_gate out chrono reset

# Finally export pin
export_opin true_gate out
