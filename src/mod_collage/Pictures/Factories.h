/*
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef FACTORIES_H
#define FACTORIES_H
namespace Pictures{
	enum typeTransition {NIL,IDENTITY, ALPHA, SCALE, ROTATE, CHANGE, TRANSLATE, VIBRATE, RANDOM};
	class ITransitionFactory;
}

#include <boost/shared_ptr.hpp>
#include "Pictures.h"

namespace Pictures{
	
	/*
	 * Interface to create transition factories
	 */
	class ITransitionFactory{
	public:
		virtual boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn)=0;
		virtual ~ITransitionFactory();
	};
	/*
	 * Creates Identity transitions
	 */
	class IdentityTransitionFactory: public ITransitionFactory{
	public:
		virtual ~IdentityTransitionFactory();
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates Alpha transitions
	 */	
	class AlphaTransitionFactory : public ITransitionFactory{
	public:
		virtual ~AlphaTransitionFactory();
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates Scale transitions
	 */
	class ScaleTransitionFactory : public ITransitionFactory{
	public:
		virtual ~ScaleTransitionFactory();
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates Rotate transitions
	 */
	class RotateTransitionFactory : public ITransitionFactory{
		int m_degrees;
	public:
		virtual ~RotateTransitionFactory();
		RotateTransitionFactory(int degrees=90);
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates ChangePicture transitions
	 */	
	class ChangePictureTransitionFactory : public ITransitionFactory{
	private:
		SmartPtr<mod_sdl::CTypeSDLSurface> m_destinationSurf;
	public:
		virtual ~ChangePictureTransitionFactory();
		ChangePictureTransitionFactory(SmartPtr<mod_sdl::CTypeSDLSurface> surf);
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates translate transitions
	 */	
	class TranslateTransitionFactory: public ITransitionFactory{
	private:
		float m_x, m_y;
	public:
		virtual ~TranslateTransitionFactory();
		TranslateTransitionFactory(float x, float y);
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates VibratePicture transitions
	 */	
	class VibratePictureTransitionFactory: public ITransitionFactory{
		private:
			float m_percent;
		public:
			virtual ~VibratePictureTransitionFactory();
			VibratePictureTransitionFactory(float percent=0.3);
			boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
	/*
	 * Creates Random transitions from the other ones
	 */
	class RandomTransitionFactory : public ITransitionFactory{
	public:
		virtual ~RandomTransitionFactory();
		boost::shared_ptr<PicturesTransition> getTransition(boost::shared_ptr<PictureNode> pn);
	};
}

#endif