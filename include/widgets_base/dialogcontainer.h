/////////////////////////////////////////////////////////////////////////////
// Name:        dialogcontainer.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     04/04/2011 18:59:05
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

#ifndef _DIALOGCONTAINER_H_
#define _DIALOGCONTAINER_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "widgets_base/libimpexp_widgetsbase.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxBoxSizer;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_DIALOGCONTAINER 10007
#define SYMBOL_DIALOGCONTAINER_STYLE wxCAPTION|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL
#define SYMBOL_DIALOGCONTAINER_TITLE _("DialogContainer")
#define SYMBOL_DIALOGCONTAINER_IDNAME ID_DIALOGCONTAINER
#define SYMBOL_DIALOGCONTAINER_SIZE wxDefaultSize
#define SYMBOL_DIALOGCONTAINER_POSITION wxDefaultPosition
////@end control identifiers

namespace widgets_base {
/*!
 * DialogContainer class declaration
 */

class SPIMPEXP_CLASS_WIDGETSBASE DialogContainer: public wxDialog
{    
    DECLARE_DYNAMIC_CLASS( DialogContainer )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    DialogContainer();
    DialogContainer( wxWindow* parent, wxWindowID id = SYMBOL_DIALOGCONTAINER_IDNAME, const wxString& caption = SYMBOL_DIALOGCONTAINER_TITLE, const wxPoint& pos = SYMBOL_DIALOGCONTAINER_POSITION, const wxSize& size = SYMBOL_DIALOGCONTAINER_SIZE, long style = SYMBOL_DIALOGCONTAINER_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_DIALOGCONTAINER_IDNAME, const wxString& caption = SYMBOL_DIALOGCONTAINER_TITLE, const wxPoint& pos = SYMBOL_DIALOGCONTAINER_POSITION, const wxSize& size = SYMBOL_DIALOGCONTAINER_SIZE, long style = SYMBOL_DIALOGCONTAINER_STYLE );

    /// Destructor
    ~DialogContainer();

	// Adds a panels from a component
	void AddSitplusPanel (wxWindow* panel);
private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin DialogContainer event handler declarations

    /// wxEVT_SIZE event handler for ID_DIALOGCONTAINER
    void OnSize( wxSizeEvent& event );

////@end DialogContainer event handler declarations

////@begin DialogContainer member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end DialogContainer member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin DialogContainer member variables
    wxBoxSizer* m_sizer;
////@end DialogContainer member variables
};

};

#endif
    // _DIALOGCONTAINER_H_
