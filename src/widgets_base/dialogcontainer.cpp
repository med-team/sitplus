/////////////////////////////////////////////////////////////////////////////
// Name:        dialogcontainer.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     04/04/2011 18:59:05
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "widgets_base/dialogcontainer.h"

////@begin XPM images
#include "bitmaps/sitplus_logo_16x16.xpm"
////@end XPM images

namespace widgets_base {

/*
 * DialogContainer type definition
 */

IMPLEMENT_DYNAMIC_CLASS( DialogContainer, wxDialog )


/*
 * DialogContainer event table definition
 */

BEGIN_EVENT_TABLE( DialogContainer, wxDialog )

////@begin DialogContainer event table entries
    EVT_SIZE( DialogContainer::OnSize )

////@end DialogContainer event table entries

END_EVENT_TABLE()


/*
 * DialogContainer constructors
 */

DialogContainer::DialogContainer()
{
    Init();
}

DialogContainer::DialogContainer( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, caption, pos, size, style);
}


/*
 * DialogContainer creator
 */

bool DialogContainer::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin DialogContainer creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxDialog::Create( parent, id, caption, pos, size, style );

    CreateControls();
    SetIcon(GetIconResource(wxT("bitmaps/sitplus_logo_16x16.xpm")));
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end DialogContainer creation
    return true;
}


/*
 * DialogContainer destructor
 */

DialogContainer::~DialogContainer()
{
////@begin DialogContainer destruction
////@end DialogContainer destruction
}


/*
 * Member initialisation
 */

void DialogContainer::Init()
{
////@begin DialogContainer member initialisation
    m_sizer = NULL;
////@end DialogContainer member initialisation
}


/*
 * Control creation for DialogContainer
 */

void DialogContainer::CreateControls()
{    
////@begin DialogContainer content construction
    DialogContainer* itemDialog1 = this;

    m_sizer = new wxBoxSizer(wxVERTICAL);
    itemDialog1->SetSizer(m_sizer);

////@end DialogContainer content construction
}


/*
 * Should we show tooltips?
 */

bool DialogContainer::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap DialogContainer::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin DialogContainer bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end DialogContainer bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon DialogContainer::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin DialogContainer icon retrieval
    wxUnusedVar(name);
    if (name == _T("bitmaps/sitplus_logo_16x16.xpm"))
    {
        wxIcon icon(sitplus_logo_16x16);
        return icon;
    }
    return wxNullIcon;
////@end DialogContainer icon retrieval
}

void DialogContainer::AddSitplusPanel (wxWindow* panel)
{
	m_sizer->Add (panel, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 0);	
	m_sizer->SetSizeHints(this);
	SetTitle (panel->GetName());
	//Layout();
	//Fit();
}


/*
 * wxEVT_SIZE event handler for ID_DIALOGCONTAINER
 */

void DialogContainer::OnSize( wxSizeEvent& event )
{
	Layout();
	Fit();

	event.Skip (false);
}

};