/**
* @file		language.h
* @brief	i18n related functions.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010-11 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "config.h"
#include "spcore/libimpexp.h"

#ifdef ENABLE_NLS
	#include <libintl.h>

	/*	
		Some versions of libintl.h for Win32 provide macros for printf 
		funtion family. Those macros can cause crashes on Windows so
		we do not want them.
	*/
	#ifdef printf
		#error "libintl.h defines printf"
	#endif

	#ifdef ENABLE_WXWIDGETS
		#include <wx/intl.h>
	#endif

	/**
		@brief _() and __() macros

		Macro _() is usually defined by some frameworks such as wx 
		which the unicode versions is commonly used, so we define
		our own translation macro __() for ansi strings
	*/
	#ifndef _
		#define _(STRING)	gettext(STRING)
	#endif

	#ifndef N_
		#define N_(STRING)	(STRING)
	#endif
	
	#define __(STRING)	dgettext(GETTEXT_DOMAIN,STRING)
	#define N__(STRING)	(STRING)

	/**
		@brief Set locale configuration given a language identifier return by spGetLanguageId.
		@param[in]	id	Locale identifier.
		@return 0 if ok, -1 if failed.

		Caveats: this funtion should be called once, otherwise the behaviour is undefined.
	*/
	SPEXPORT_FUNCTION
	int spSetLanguage (const char* id);

	/**
		@brief Bind gettext domain with a directory.
		@param[in]	domain	Domain name.
		@param[in]	dirname	Directory name.
		@return 0 if OK, -1 if error.

		Before calling this function the language must be set.
	*/
	SPEXPORT_FUNCTION
	int spBindTextDomain(const char* domain, const char* dirname);

	/**
		@brief Obtains the number of available languages.
		@return the number of available languages.
	*/
	SPEXPORT_FUNCTION
	unsigned int spGetAvailableLanguages();

	/**
		@brief Obtains the language ID given its index.
		@param[in]	idx	Index of the parameter. 
		@return a pointer to an string whith the language id or NULL if error.

		The returned identifier can be stored and used later.
		Do not rely on indexs which could change between executions.
	*/
	SPEXPORT_FUNCTION
	const char* spGetLanguageId (unsigned int idx);

	/**
		@brief Obtains the language name given its index.
		@param[in]	idx	Index of the parameter.
		@param[in]	domain	Domain for gettext
		@return a pointer to an string whith the language name or NULL if error.

		The names provided are given in its native language encoded using utf-8.
	*/
	SPEXPORT_FUNCTION
	const char* spGetLanguageNativeName (unsigned int idx, const char* domain);

#else
	// If NLS disabled, macros do nothing.
	#define __(STRING)	(STRING)
	#define N__(STRING)	(STRING)
#endif