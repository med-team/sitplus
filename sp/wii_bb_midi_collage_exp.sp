type wiiinstr
name "Wii Virtual Instrument"

# Create wii input
import wii_bb_motion_position.sps
create wii_bb_motion_position wii_input
# wii provides data at a 90Hz rate, so we reduce it by a factor of 3
#create freductor freductor -r 3
#connect wii_input motion freductor in

# Axis selectors
create widget_checkbox chk_x -v 1 -l "Use X axis"
create fcast cast_x
create fmul gate_x_pos
create fmul gate_x_motion
connect chk_x value cast_x in
connect cast_x out gate_x_pos b
connect cast_x out gate_x_motion b
connect wii_input x_pos gate_x_pos a
connect wii_input x_motion gate_x_motion a

create widget_checkbox chk_y -v 1 -l "Use Y axis"
create fcast cast_y
create fmul gate_y_pos
create fmul gate_y_motion
connect chk_y value cast_y in
connect cast_y out gate_y_pos b
connect cast_y out gate_y_motion b
connect wii_input y_pos gate_y_pos a
connect wii_input y_motion gate_y_motion a

# Compute position modulus
import 2d_modulus.sps
create 2d_modulus pos_mod
connect gate_x_pos result pos_mod x
connect gate_y_pos result pos_mod y

# Adjust range
create widget_slider sld_pos_range -v 1 --min 0.1 --max 2 -l "MIDI Sensitivity" 
create fmul pos_range
connect sld_pos_range value pos_range b
connect pos_mod result pos_range a

# Limit range
create flimit pos_limit --max 1
connect pos_range result pos_limit in

#
# MIDI player
#
create midi_out midi_out
create score_player player
connect player note midi_out message

create freductor freductor_pos -r 2 -a
connect pos_limit out freductor_pos in
# Input position
connect freductor_pos out player pointer

#
# Graphics
#

# Compute position modulus
create 2d_modulus motion_mod
connect gate_x_motion result motion_mod x
connect gate_y_motion result motion_mod y
create freductor freductor_motion -r 3
connect motion_mod result freductor_motion in
create fthreshold motion_thres -t 0.001 -b 0 -a orig_minus_thres
connect freductor_motion out motion_thres value

# Collage
import motion_collage.sps
create motion_collage motion_collage
create send_main_async to_main

connect motion_thres result to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox
			component chk_x
			component chk_y
			component sld_pos_range
			component player			
		layout_end
		layout_begin vbox			
			component motion_collage			
		layout_end
	layout_end
end_gui_layout