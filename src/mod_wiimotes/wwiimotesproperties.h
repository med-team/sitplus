/////////////////////////////////////////////////////////////////////////////
// Name:        wiimotesproperties.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     18/03/2011 13:00:20
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.     
/////////////////////////////////////////////////////////////////////////////

#ifndef _WIIMOTESPROPERTIES_H_
#define _WIIMOTESPROPERTIES_H_


/*!
 * Includes
 */

#include "mod_wiimotes/wiimotes_types.h"
////@begin includes
////@end includes
#include <wx/statbmp.h>
#include <wx/panel.h>


/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_WIIMOTESPROPERTIES 10005
#define wxID_ICON 10004
#define ID_CHK_CONNECTED 10003
#define ID_CHK_ACC 10000
#define ID_CHK_NUNCHUCK 10001
#define ID_CHK_MOTION_PLUS 10002
#define SYMBOL_WIIMOTESPROPERTIES_STYLE 0
//#define SYMBOL_WIIMOTESPROPERTIES_TITLE _("Wiimotes properties")
#define SYMBOL_WIIMOTESPROPERTIES_IDNAME ID_WIIMOTESPROPERTIES
#define SYMBOL_WIIMOTESPROPERTIES_SIZE wxDefaultSize
#define SYMBOL_WIIMOTESPROPERTIES_POSITION wxDefaultPosition
////@end control identifiers

namespace mod_wiimotes {

/*!
 * Wiimotesproperties class declaration
 */

class Wiimotesproperties: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( Wiimotesproperties )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    Wiimotesproperties();
    Wiimotesproperties( wxWindow* parent, wxWindowID id = SYMBOL_WIIMOTESPROPERTIES_IDNAME, const wxPoint& pos = SYMBOL_WIIMOTESPROPERTIES_POSITION, const wxSize& size = SYMBOL_WIIMOTESPROPERTIES_SIZE, long style = SYMBOL_WIIMOTESPROPERTIES_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_WIIMOTESPROPERTIES_IDNAME, const wxPoint& pos = SYMBOL_WIIMOTESPROPERTIES_POSITION, const wxSize& size = SYMBOL_WIIMOTESPROPERTIES_SIZE, long style = SYMBOL_WIIMOTESPROPERTIES_STYLE );

    /// Destructor
    ~Wiimotesproperties();
    
    void Update (const CTypeWiimotesStatus &, unsigned int n);
private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin Wiimotesproperties event handler declarations

////@end Wiimotesproperties event handler declarations

////@begin Wiimotesproperties member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end Wiimotesproperties member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin Wiimotesproperties member variables
    wxStaticBitmap* m_icon;
    wxCheckBox* m_chkConnected;
    wxCheckBox* m_chkAcc;
    wxCheckBox* m_chkNunchuck;
    wxCheckBox* m_chkMotionPlus;
////@end Wiimotesproperties member variables
};

};

#endif
    // _WIIMOTESPROPERTIES_H_
