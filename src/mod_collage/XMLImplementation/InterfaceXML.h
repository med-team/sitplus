/*
    InterfaceXML.h -- Interface to load and translate an XML file (only for mod_collage)
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#ifndef INTERFACEXML_H
#define INTERFACEXML_H

#include <vector>
#include <string>

#include "Implementation.h"
#include "../Pictures/Factories.h"

#include "Poco/SAX/ContentHandler.h"

#include "boost/shared_ptr.hpp"

#include "mod_sdl/sdlsurfacetype.h"

namespace XMLImplementation{ 
	/*
	 * This class serves to store any kind of error given when handling XML file.
	 */
	class Error{
	private:
		int m_code;
		std::string m_element;
		std::string m_attribute;
		unsigned int m_line;
		std::string m_message;
	public:
		Error(int code, std::string element, std::string attribute, unsigned int line, std::string message);
		~Error();
		std::string toString();
		int getCode();
	};
	/*
	 * This class is a log for errors on loading XML file.
	 */
	class LogError{
	private:
		std::vector<boost::shared_ptr<Error> > m_errorLog;
	public:
		~LogError();
		void addError(boost::shared_ptr<Error> err);
		std::vector<boost::shared_ptr<Error> > getErrors();
		bool hasFatalErrors();
	};
	/*
	 * This class is an interface beetwen the XML file and the code program. It tranlate XML tags into objects.
	 */
	class XMLHandler : public Poco::XML::ContentHandler{
	public:
		XMLHandler(std::string dir, boost::shared_ptr<Activity> activity, boost::shared_ptr<LogError> errors, boost::shared_ptr<DBImages>);
		~XMLHandler();
		void setDocumentLocator(const Poco::XML::Locator* loc);
		void startDocument();
		void endDocument();
		void startElement(const Poco::XML::XMLString& uri, const Poco::XML::XMLString& localName, const Poco::XML::XMLString& qname, const Poco::XML::Attributes& attributes);
		void endElement(const Poco::XML::XMLString& uri, const Poco::XML::XMLString& localName, const Poco::XML::XMLString& qname);
		void characters(const Poco::XML::XMLChar ch[], int start, int length);
		void ignorableWhitespace(const Poco::XML::XMLChar ch[], int start, int length);
		void processingInstruction(const Poco::XML::XMLString& target, const Poco::XML::XMLString& data);
		void startPrefixMapping(const Poco::XML::XMLString& prefix, const Poco::XML::XMLString& uri);
		void endPrefixMapping(const Poco::XML::XMLString& prefix);
		void skippedEntity(const Poco::XML::XMLString& name);
	private:
		const Poco::XML::Locator* m_pLocator;
		boost::shared_ptr<Activity> m_activitat;
		boost::shared_ptr<LogError> m_errors;
		boost::shared_ptr<DBImages> m_images;
		Pictures::typeTransition m_defTrIn,m_defTrOut,m_trIn,m_trOut;
		boost::shared_ptr<Module> m_mod;
		boost::shared_ptr<Picture> m_image;
		bool m_errorKrnl, m_errorSrc, m_trans, m_def;
		bool m_tagError;
		int m_level;
		std::string m_dir;
		std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > m_surfs;
		int m_degrees;
		float m_x,m_y;
	};
	/*
	 * This class initializes the loading of XML file to handle its contets with XMLHandler
	 */
	class LoadXML{
	private:
		boost::shared_ptr<Activity> m_configuracio;
		boost::shared_ptr<LogError> m_errors;
	public:
		LoadXML(std::string dir, std::string file, boost::shared_ptr<DBImages> images);
		~LoadXML();
		boost::shared_ptr<Activity> getConfiguration();
		boost::shared_ptr<LogError> getErrors();
	};
}

#endif // INTERFACEXML_H
