/////////////////////////////////////////////////////////////////////////////
// Name:        sitplus_main_window.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     04/04/2011 18:26:27
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#ifndef _SITPLUS_MAIN_WINDOW_H_
#define _SITPLUS_MAIN_WINDOW_H_


/*!
 * Includes
 */

#include "spcore/coreruntime.h"

////@begin includes
#include "wx/frame.h"
////@end includes
#include <wx/textctrl.h>

#include <string>
#include <vector>

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_MAIN_WINDOW 10000
#define ID_MENUITEM_EXIT 10002
#define ID_MENUITEM_ACTIVITY_START 10017
#define ID_MENUITEM_ACTIVITY_LOAD_FILE 10014
#define ID_MENUITEM_CONFIG_CAMERA 10003
#define ID_MENUITEM_CONFIG_WIIMOTES 10004
#define ID_MENUITEM_CONFIG_MIDI 10006
#define ID_MENUITEM_CONFIG_PD 10001
#define ID_MENUITEM_LANGUAGE 10018
#define ID_MENUITEM_HELPHELP 10023
#define ID_MENUITEM_ABOUT 10022
#define ID_TEXTCTRL_LOG 10005
#define SYMBOL_SITPLUSMAINWINDOW_STYLE wxDEFAULT_FRAME_STYLE|wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxMINIMIZE_BOX|wxMAXIMIZE_BOX|wxCLOSE_BOX
#define SYMBOL_SITPLUSMAINWINDOW_TITLE _("SITPLUS")
#define SYMBOL_SITPLUSMAINWINDOW_IDNAME ID_MAIN_WINDOW
#define SYMBOL_SITPLUSMAINWINDOW_SIZE wxSize(400, 300)
#define SYMBOL_SITPLUSMAINWINDOW_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * SitplusMainWindow class declaration
 */

class SitplusMainWindow: public wxFrame, public spcore::ILogTarget
{    
    DECLARE_CLASS( SitplusMainWindow )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    SitplusMainWindow();
    SitplusMainWindow( wxWindow* parent, wxWindowID id = SYMBOL_SITPLUSMAINWINDOW_IDNAME, const wxString& caption = SYMBOL_SITPLUSMAINWINDOW_TITLE, const wxPoint& pos = SYMBOL_SITPLUSMAINWINDOW_POSITION, const wxSize& size = SYMBOL_SITPLUSMAINWINDOW_SIZE, long style = SYMBOL_SITPLUSMAINWINDOW_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_SITPLUSMAINWINDOW_IDNAME, const wxString& caption = SYMBOL_SITPLUSMAINWINDOW_TITLE, const wxPoint& pos = SYMBOL_SITPLUSMAINWINDOW_POSITION, const wxSize& size = SYMBOL_SITPLUSMAINWINDOW_SIZE, long style = SYMBOL_SITPLUSMAINWINDOW_STYLE );

    /// Destructor
    ~SitplusMainWindow();

private:
	void EnableActivityOptions(bool enable);

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	void OpenConfigDialog(const char* component, const std::vector<std::string>* args= NULL);

	void LoadActivity (const char* fileName);

	void AppendLogMessage(spcore::ICoreRuntime::LogSeverityLevel severity, const wxString& msg);

	void OnLogMessage( wxCommandEvent& event );

	virtual void LogMessage (spcore::ICoreRuntime::LogSeverityLevel severity, const char* message);

////@begin SitplusMainWindow event handler declarations

    /// wxEVT_CLOSE_WINDOW event handler for ID_MAIN_WINDOW
    void OnCloseWindow( wxCloseEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_EXIT
    void OnMenuitemExitClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_ACTIVITY_START
    void OnMenuitemActivityStartClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_ACTIVITY_LOAD_FILE
    void OnMenuitemActivityLoadFileClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_CONFIG_CAMERA
    void OnMenuitemConfigCameraClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_CONFIG_WIIMOTES
    void OnMenuitemConfigWiimotesClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_CONFIG_MIDI
    void OnMenuitemConfigMidiClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_CONFIG_PD
    void OnMenuitemConfigPdClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_LANGUAGE
    void OnMenuitemLanguageClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_HELPHELP
    void OnMenuitemHelphelpClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEM_ABOUT
    void OnMenuitemAboutClick( wxCommandEvent& event );

////@end SitplusMainWindow event handler declarations

	void OnCloseChildWindow( wxCloseEvent& event );

////@begin SitplusMainWindow member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end SitplusMainWindow member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin SitplusMainWindow member variables
    wxTextCtrl* m_txtLog;
////@end SitplusMainWindow member variables

	bool m_hasVetoed;
};

#endif
    // _MAIN_WINDOW_H_
