/*
    Module.cpp -- Implements an object whose structure is according to the tag module from XML file
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Implementation.h"

using mod_sdl::CTypeSDLSurface;
using std::vector;
using boost::shared_ptr;
using namespace Kernel;

namespace XMLImplementation{
	/*
	 * Constructor of class Module. Initializes class' variables.
	 */
	Module::Module(shared_ptr<AbstractKernelFactory> akf, delayType delay, vector<SmartPtr<CTypeSDLSurface> > lBg, float lapse){
		this->m_abstractKernelFactory = akf;
		this->m_delay = delay;
		this->m_listSrcBg = lBg;
		this->m_lapseAnim = lapse;
	}
	/*
	 * Destructor from class Module.
	 */
	Module::~Module(){}
	/*
	 * Adds a Picture to the Pictures' list
	 */
	void Module::addPicture(boost::shared_ptr<Picture> image){
		this->m_listPictures.push_back(image);
	}
	/*
	 * Returns a pointer to Picture, given its index, or NULL if the index parameter is out of bounds.
	 */
	shared_ptr<Picture> Module::getPicture (unsigned int index){
		if(index>=0&&index<m_listPictures.size())
			return m_listPictures[index];
		//else return shared_ptr<Picture>(NULL);

		// CHECK THIS: all paths should return something, if this never should
		// happen, assert in debug mode and handle it accordingly in
		// release mode
		assert (false);
		return shared_ptr<Picture>();
	}

	/*
	 * Returns de Pictures's list
	 */
	vector<shared_ptr<Picture> > Module::getListPictures(){
		return this->m_listPictures;
	}	
	/*
	 * Returns the kernel used, given by the factory
	 */
	shared_ptr<AbstractKernel> Module::getKernel(shared_ptr<Module> m){
		return this->m_abstractKernelFactory->getKernel(m);
 	}	
 	/*
	 * Returns the kernel's behaviour (an enumerate from 'delayType').
	 */
	delayType Module::getDelayType()
	{
		return this->m_delay;
	}
	/*
	 * Return the Picture's list used as background animation
	 */
	vector<SmartPtr<mod_sdl::CTypeSDLSurface> > Module::getListSrcBg(){
		return this->m_listSrcBg;
	}
	/*
	 * Returns a float which represents the lapse time between Pictures to do the animation from background
	 */
	float Module::getLapseAnimation(){
		return this->m_lapseAnim;
	}
	/*
	 * Returns false either case, but if Pictures' list is empty returns true.
	 */
	bool Module::isEmpty(){return m_listPictures.size()==0;}
}