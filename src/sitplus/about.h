/////////////////////////////////////////////////////////////////////////////
// Name:        about.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     20/06/2011 11:18:13
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.  
/////////////////////////////////////////////////////////////////////////////

#ifndef _ABOUT_H_
#define _ABOUT_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/html/htmlwin.h"
////@end includes
#include <wx/dialog.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxHtmlWindow;
////@end forward declarations


/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_ABOUT 10019
#define ID_HTMLWINDOW 10020
#define ID_BUTTON_CLOSE_ABOUT 10021
#define SYMBOL_ABOUT_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL
#define SYMBOL_ABOUT_TITLE _("About")
#define SYMBOL_ABOUT_IDNAME ID_ABOUT
#define SYMBOL_ABOUT_SIZE wxDefaultSize
#define SYMBOL_ABOUT_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * About class declaration
 */

class About: public wxDialog
{    
    DECLARE_DYNAMIC_CLASS( About )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    About();
    About( wxWindow* parent, wxWindowID id = SYMBOL_ABOUT_IDNAME, const wxString& caption = SYMBOL_ABOUT_TITLE, const wxPoint& pos = SYMBOL_ABOUT_POSITION, const wxSize& size = SYMBOL_ABOUT_SIZE, long style = SYMBOL_ABOUT_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_ABOUT_IDNAME, const wxString& caption = SYMBOL_ABOUT_TITLE, const wxPoint& pos = SYMBOL_ABOUT_POSITION, const wxSize& size = SYMBOL_ABOUT_SIZE, long style = SYMBOL_ABOUT_STYLE );

    /// Destructor
    ~About();

private:
	void FillHtmlContent();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin About event handler declarations

    /// wxEVT_COMMAND_HTML_LINK_CLICKED event handler for ID_HTMLWINDOW
    void OnHtmlwindowLinkClicked( wxHtmlLinkEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_ABOUT
    void OnButtonCloseAboutClick( wxCommandEvent& event );

////@end About event handler declarations

////@begin About member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end About member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin About member variables
    wxHtmlWindow* m_htmlAbout;
////@end About member variables
};

#endif
    // _ABOUT_H_
