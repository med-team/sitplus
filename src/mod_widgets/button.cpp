/////////////////////////////////////////////////////////////////////////////
// Name:        buttonpanel.cpp
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     03/06/2011 18:34:35
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "button.h"

////@begin XPM images
////@end XPM images

using namespace spcore;
using namespace std;

namespace mod_widgets {

ButtonComponent::ButtonComponent(const char * name, int argc, const char * argv[])
: BaseWidgetComponent<ButtonPanel,ButtonComponent>(name, argc, argv)
{
	//
	// Create pins
	//
	m_oPinPressed=  CTypeBool::CreateOutputPin("pressed");
	m_valuePressed= CTypeBool::CreateInstance();

	RegisterOutputPin (*m_oPinPressed);

	//
	// Process arguments
	//
	if (argc) {	
		for (int i= 0; i< argc; ++i) {
			if (argv[i] && strlen(argv[i])) {
				string error_msg("widget_button. Unknown option:");
				error_msg+= argv[i];
				throw std::runtime_error(error_msg);
			}
		}
	}
}

// 
// Private methods
//	

// 
// Called from GUI
//	

void ButtonComponent::Pressed()
{
	m_valuePressed->setValue(true);
	m_oPinPressed->Send(m_valuePressed);
}


//////////////////////////////////////////////////////////////////////////////
/*!
 * ButtonPanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ButtonPanel, wxButton )


/*!
 * ButtonPanel event table definition
 */

BEGIN_EVENT_TABLE( ButtonPanel, wxButton )

////@begin ButtonPanel event table entries
    EVT_BUTTON( ID_BUTTON, ButtonPanel::OnButtonClick )

////@end ButtonPanel event table entries

END_EVENT_TABLE()


/*!
 * ButtonPanel constructors
 */

ButtonPanel::ButtonPanel()
{
    Init();
}

ButtonPanel::ButtonPanel(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator)
{
    Init();
    Create(parent, id, label, pos, size, style, validator);
}


/*!
 * ButtonPanel creator
 */

bool ButtonPanel::Create(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator)
{
////@begin ButtonPanel creation
    wxButton::Create(parent, id, label, pos, size, style, validator);
    CreateControls();
////@end ButtonPanel creation
    return true;
}


/*!
 * ButtonPanel destructor
 */

ButtonPanel::~ButtonPanel()
{
////@begin ButtonPanel destruction
////@end ButtonPanel destruction
}


/*!
 * Member initialisation
 */

void ButtonPanel::Init()
{
////@begin ButtonPanel member initialisation
////@end ButtonPanel member initialisation
}


/*!
 * Control creation for ButtonPanel
 */

void ButtonPanel::CreateControls()
{    
////@begin ButtonPanel content construction
////@end ButtonPanel content construction
	if (this->m_component && this->m_component->GetLabel().size()) {
		wxString label(m_component->GetLabel().c_str(), wxConvUTF8);
		this->SetLabel(label);
	}
}

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON
 */

void ButtonPanel::OnButtonClick( wxCommandEvent& event )
{
	if (this->m_component) this->m_component->Pressed();

    event.Skip(false);
}

}; // namespace
