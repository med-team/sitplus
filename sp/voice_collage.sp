type playingwithvoice
name _"Playing with voice"

# Create pvoice component
import lib/pvoice.sps
create playingwithvoice pvoice

# Send to main thread
create send_main_async to_main
connect pvoice out to_main in

# Collage
import lib/motion_collage.sps
create motion_collage motion_collage
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox _"Voice Input"
			component pvoice
		layout_end
		layout_begin vbox _"Graphics"
			component motion_collage			
		layout_end
	layout_end	
end_gui_layout