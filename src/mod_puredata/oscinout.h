/////////////////////////////////////////////////////////////////////////////
// Name:       
// Purpose:  
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Modified by: 
// Created:     
// Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef OSCINOUT_H
#define OSCINOUT_H

#include <ip/UdpSocket.h>
#include <osc/OscPacketListener.h>
#include <osc/OscOutboundPacketStream.h>
#include <wx/thread.h>

class COscOut : public osc::OutboundPacketStream
{
public:
	COscOut (int port);
	~COscOut ();

	void Open();

	void Close();

	void Send();

	void SendSimpleMessage (const char *name, float v);

private:
	int m_port;
	UdpTransmitSocket* m_pTransmitSocket;
	enum { OSC_OUTPUT_BUFFER_SIZE= 1024 };
	char m_oscBuffer[OSC_OUTPUT_BUFFER_SIZE];
	
};

class COscIn
{
public:
	COscIn (int port, PacketListener *listener);
	~COscIn();

	void Open();

	void Close();

private:

	class COscReceiverThread : public wxThread
	{
	public:
		COscReceiverThread(UdpListeningReceiveSocket* socket);
		void Break() { assert(m_socket); m_socket->AsynchronousBreak(); }
		virtual wxThread::ExitCode Entry();
		~COscReceiverThread();
	private:
		UdpListeningReceiveSocket* m_socket;
	};

	int m_port;
	PacketListener *m_listener;
	COscReceiverThread* m_thread;
};


#endif