#
# Wii accelerometers motion tracker
#
#
# Output pins
# 	motion (float)
#

type wii_acc_motion
name "Wii Motion Tracker"

create wiimotes_config_gui wii_config

# Wii input
create wiimotes_input wii
create wii_acc_estimate acc_estimate

connect wii accelerometers acc_estimate in

# Split axis information
create split split -o 3
connect acc_estimate out split input

# Axis selectors
create widget_checkbox chk_x -v 1 -l _"Use X axis (left - right)"
create fcast cast_x
connect chk_x value cast_x in
create fmul gate_x
connect cast_x out gate_x b
# Invert axis
create fmul invertx -v -1.0
connect split 1 invertx a
connect invertx result gate_x a

create widget_checkbox chk_y -v 0 -l _"Use Y axis (back - forth)"
create fmul gate_y
create fcast cast_y
connect chk_y value cast_y in
connect cast_y out gate_y b
connect split 2 gate_y a

create widget_checkbox chk_z -v 1 -l _"Use Z axis (up - down)"
create fmul gate_z
create fcast cast_z
connect chk_z value cast_z in
connect cast_z out gate_z b
connect split 3 gate_z a

# Accumulate axis outcomes
create fadd add_yx
connect gate_y result add_yx a
connect gate_x result add_yx b

create fadd add_zyx
connect gate_z result add_zyx a
connect add_yx result add_zyx b

# Scale value 
create fmul fmul_scale -v 0.04
connect add_zyx result fmul_scale a

# Absolute value
create fabs fabs
connect fmul_scale result fabs in

# Threshold
import around_zero_threshold.sps
create around_zero_threshold thres -lsld _"Motion threshold" -lchk _"Motion detected" -max 0.002 -v 0.0016
connect fabs out thres in

# Sensitivity
create fmul fmul_sens
connect thres out fmul_sens a
create widget_slider sldsens --min 0 --max 4 -v 1 -l _"Sensitivity" --log
connect sldsens value fmul_sens b

# wii provides data at a 90Hz rate, so we reduce it by a factor of 3
create freductor freductor -r 3
connect fmul_sens result freductor in

# Motion value is on fmul_sens output, export as "motion"
export_opin freductor out motion

begin_gui_layout
	layout_begin vbox
		component sldsens
		layout_begin collapsible _"Wii Devices"
			component wii_config
		layout_end
		layout_begin collapsible _"Advanced"
			component chk_x
			component chk_y
			component chk_z			
			component thres
		layout_end
	layout_end
end_gui_layout