/////////////////////////////////////////////////////////////////////////////
// Name:        sdlconfiguration.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     09/01/2011 03:25:06
// RCS-ID:      
// Copyright:   (c) 2011 C�sar Mauri Loba
// Licence: 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef _SDLCONFIGURATION_H_
#define _SDLCONFIGURATION_H_


/*!
 * Includes
 */

////@begin includes
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_SDLCONFIGURATION 10002
#define ID_TEXTCTRL 10000
#define ID_TEXTCTRL1 10001
#define ID_CHECKBOX 10003
#define SYMBOL_SDLCONFIGURATIONGUI_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL
#define SYMBOL_SDLCONFIGURATIONGUI_TITLE _("SDL Configuration")
#define SYMBOL_SDLCONFIGURATIONGUI_IDNAME ID_SDLCONFIGURATION
#define SYMBOL_SDLCONFIGURATIONGUI_SIZE wxSize(400, 300)
#define SYMBOL_SDLCONFIGURATIONGUI_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * SDLConfigurationGUI class declaration
 */

class SDLConfigurationGUI: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( SDLConfigurationGUI )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    SDLConfigurationGUI();
    SDLConfigurationGUI( wxWindow* parent, wxWindowID id = SYMBOL_SDLCONFIGURATIONGUI_IDNAME, const wxString& caption = SYMBOL_SDLCONFIGURATIONGUI_TITLE, const wxPoint& pos = SYMBOL_SDLCONFIGURATIONGUI_POSITION, const wxSize& size = SYMBOL_SDLCONFIGURATIONGUI_SIZE, long style = SYMBOL_SDLCONFIGURATIONGUI_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_SDLCONFIGURATIONGUI_IDNAME, const wxString& caption = SYMBOL_SDLCONFIGURATIONGUI_TITLE, const wxPoint& pos = SYMBOL_SDLCONFIGURATIONGUI_POSITION, const wxSize& size = SYMBOL_SDLCONFIGURATIONGUI_SIZE, long style = SYMBOL_SDLCONFIGURATIONGUI_STYLE );

    /// Destructor
    ~SDLConfigurationGUI();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin SDLConfigurationGUI event handler declarations

    /// wxEVT_INIT_DIALOG event handler for ID_SDLCONFIGURATION
    void OnInitDialog( wxInitDialogEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
    void OnCancelClick( wxCommandEvent& event );

////@end SDLConfigurationGUI event handler declarations

////@begin SDLConfigurationGUI member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end SDLConfigurationGUI member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin SDLConfigurationGUI member variables
    wxTextCtrl* m_txtWidth;
    wxTextCtrl* m_txtHeight;
    wxCheckBox* m_chkFullscreen;
////@end SDLConfigurationGUI member variables
};

#endif
    // _SDLCONFIGURATION_H_
