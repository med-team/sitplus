/////////////////////////////////////////////////////////////////////////////
// File:        mod_wiimotes.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef MOD_WIIMOTES_H
#define MOD_WIIMOTES_H

#include "mod_wiimotes/wiimotes_types.h"
// Visual C++ complains about the structs defined in wiiuse.h
// so we disable the meaningless warnings
#if defined(_MSC_VER)
#pragma warning (disable:4510)
#pragma warning (disable:4512)
#pragma warning (disable:4610)
#define snprintf _snprintf
#endif
#include "wiiuse/include/wiiuse.h"
#include <boost/thread/thread.hpp>

using namespace spcore;

namespace mod_wiimotes {

// wiimote listener
class WiimoteListener
{
public:
	virtual void StatusNotification (const CTypeWiimotesStatus &)= 0;
	virtual void WiimoteNotification (wiimote *)= 0;
};

/*
	Thread controller

	This is a singleton class which starts/stops the wiimotes thread and forwards requests
*/
class WiiuseThread;

class WiiuseThreadController {
public:
	static WiiuseThreadController * getInstance();
	static void destroyInstance();


	void RegisterListener (WiimoteListener& wl, unsigned int df, unsigned int filter);
	void UnregisterListener (WiimoteListener& wl);

	void Reconnect();
	void ReqStatus();

private:
	WiiuseThreadController();
	virtual ~WiiuseThreadController();

	WiiuseThread* m_worker;
	boost::thread* m_thread;
	static WiiuseThreadController *g_instance;
};

};

#endif