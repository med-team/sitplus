#
# Test and sample on how to use arguments
#

type test_arguments_main
name "test_arguments_main"

import test_arguments.sps

#
# Simple argument passing
#
create test_arguments test_arguments1 -a aa -b bb -c cc

#
# Void argument value. Will expand into nothing
#
create test_arguments test_arguments2 -a -b bb -c cc

#
# Substitution of the same variable twice
#
create test_arguments test_arguments3 -a $SP_DATA_DIR$#$SP_DATA_DIR$ -b bb -c cc

#
# -a argument will expand into two new tokens "-sub" and "aa", note the
# leading whitespace after "
#
create test_arguments test_arguments4 -a " -sub aa" -b bb -c cc

#
# The same as above but passing a list of arguments
#
create test_arguments test_arguments6 -a " -sub a a" -b bb -c cc

#
# The same as above but passing a list of arguments packed
#
create test_arguments test_arguments7 -a " -sub \"a a\"" -b bb -c cc


#create test_arguments test_arguments4 -b bb -c cc

