/////////////////////////////////////////////////////////////////////////////
// Name:        oftracker.h
// Purpose:     
// Author:      Cesar Mauri Loba
// Modified by: 
// Created:    04/09/2009
// Copyright:   (C) 2010 Cesar Mauri (cesar at crea-si dot com)
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifndef OFTRACKER_H
#define OFTRACKER_H

#include "creavision/crvimage.h"
#include "mod_camera/roitype.h"

namespace mod_vision {

class COfTracker 
{
public:
	COfTracker(); 
	~COfTracker(void);

	// TODO: optimization: accept only gray images through an smart pointer
   // int ProcessImage(const CIplImage& image, float &x, float &y);
	int ProcessImage(const IplImage& image, float &x, float &y);
	
	void SetROI (const mod_camera::CTypeROI & roi) {
		roi.Clone(m_roi.get(), true);
	}

private:	
	bool AllocateImages(const IplImage& image);

    CIplImage m_velX;
    CIplImage m_velY;
    CIplImage m_previousImg;
	CIplImage m_currentImg;
	CIplImage m_diffImg; 
	SmartPtr<mod_camera::CTypeROI> m_roi;
};

};

#endif
