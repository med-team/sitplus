/**
* @file		paths.h
* @brief	Common path utilities.
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPCORE_PATHS_H
#define SPCORE_PATHS_H

#include "spcore/baseobj.h"

namespace spcore {

/**
	@brief Interface class for path reporting class
*/
class IPaths : public IBaseObject {
protected:
	virtual ~IPaths() {}

public:
	/**
		@brief Return the location of the applications global, i.e. not 
		user-specific, data files.
		The returned value is, in order of precedence:
		- The value set using SetDataDir
		- The value found in SP_DATA_DIR environment variable
		- A guessed path which is usually install-prefix/share/appname
		on *nix and the path where the binary resides on Windows
		
		@return pointer to an internal storage which should not be deallocated or NULL
	*/
	virtual const char* GetDataDir() const= 0;

	/**
		@brief Sets the location of the applications global, i.e. not 
		user-specific, data files.
		@param dir Data files directory
	*/
	virtual void SetDataDir(const char* dir)= 0;
	
	/**
		@brief Return the directory for the user-dependent application data 
		and configuration files
		The returned value is, in order of precedence:
		- The value set using SetUserDataDir
		- The value found in SP_USER_DATA_DIR environment variable
		- A guessed path which is usually ~/.appname on *nix and
		 C:/Users/username/AppData/appname on Windows
		@return pointer to an internal storage which should not be deallocated or NULL
	*/
	virtual const char* GetUserDataDir() const= 0;

	/**
		@brief Sets the location for the user-dependent application data
		@param dir User-dependent data files directory
	*/
	virtual void SetUserDataDir(const char* dir)= 0;
	
	/**
		@brief Return language catalogs directory.
		The returned value is, in order of precedence:
		- The value set using SetLocalesDir
		- The value found in SP_LOCALES_DIR environment variable
		- A guessed path which is usually install_prefix/share/locale/ on *nix and 
		DataDir plus locale/ on Windows
		@return pointer to an internal storage which should not be deallocated or NULL
	*/
	virtual const char* GetLocalesDir() const= 0;

	/**
		@brief Sets the location for language catalogs
		@param dir Language catalogs files directory
	*/
	virtual void SetLocalesDir(const char* dir)= 0;

	/**
		@brief Return plug-ins directory.
		The returned value is, in order of precedence:
		- The value set using SetPluginsDir
		- The value found in SP_PLUGINS_DIR environment variable
		- A guessed path which is usually install_prefix/lib/sitplus/ on *nix and 
		DataDir on Windows
		@return pointer to an internal storage which should not be deallocated or NULL
	*/
	virtual const char* GetPluginsDir() const= 0;

	/**
		@brief Sets the location for plug-ins catalogs
		@param dir Directory name.
	*/
	virtual void SetPluginsDir(const char* dir)= 0;
};

} // namespace spcore
#endif
