/*
    Picture.cpp -- Implements an object whose structure is according to the tag picture from XML file
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Implementation.h"

using namespace Pictures;
using std::string;
using std::vector;
using boost::shared_ptr;
using mod_sdl::CTypeSDLSurface;

namespace XMLImplementation{
	/*
	 * Constructor of Picture class. Initializes attributes
	 */
	Picture::Picture(vector<SmartPtr<CTypeSDLSurface> > surfaces, int q,float scale, float x, float y){
		m_quantity = q;
		this->m_scale = scale;
		m_actualImage = 0;
		m_surfaces = surfaces;
		this->m_x = x;
		this->m_y = y;
	}
	/*
	 * Destructor from class Picture
	 */
	Picture::~Picture(){}
	/*
	 * Returns the quantity of the pictures
	 */
	int Picture::getQuantity(){
		return m_quantity;
	}
	/*
	 * Returns the factor scalation of the pictures
	 */
	float Picture::getScale(){
		return m_scale;
	}
	/*
	 * Returns a PictureNode from this Picture, and points to the next image.
	 */
	shared_ptr<PictureNode> Picture::getPictureNode(float rx, float ry,float rz){
		shared_ptr<PictureNode> pn = shared_ptr<PictureNode>(new PictureNode(m_surfaces[m_actualImage],rx,ry,rz));
		boost::shared_ptr<PicturesTransition> ident = boost::shared_ptr<PicturesTransition>(new PicturesTransition(pn));
		//If the list of transition factories is empty then the transition setted is the identity transition
		if(m_transInFactory.size()>0)
			pn->setTransitionIn(m_transInFactory[m_actualImage%m_transInFactory.size()]->getTransition(pn));
		else
			pn->setTransitionIn(ident);
		if(m_transOutFactory.size()>0)
			pn->setTransitionOut(m_transOutFactory[m_actualImage%m_transOutFactory.size()]->getTransition(pn));
		else
			pn->setTransitionOut(ident);
		int size = m_surfaces.size();
		//next image for next time that is called
		if(size>1){
			m_actualImage=(m_actualImage+1)%size;
		}
		return pn;
	}
	/*
	 * Sets the in transition factory
	 */
	void Picture::setTransitionInFactory(vector<shared_ptr<ITransitionFactory> > in){
		m_transInFactory = in;
	}
	/*
	 * Sets the out transition factory
	 */
	void Picture::setTransitionOutFactory(vector<shared_ptr<ITransitionFactory> > out){
		m_transOutFactory = out;
	}
	/*
	 * Returns the x coordinate of picture position
	 */
	float Picture::getPointX(){return m_x;}
	/*
	 * Returns the y coordinate of picture position
	 */
	float Picture::getPointY(){return m_y;}
}