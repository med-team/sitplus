/*
    Activity.cpp -- Implements an object whose structure is according to the tag activity from XML file
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Implementation.h"

namespace XMLImplementation{
	/*
	 * Destructor from class Activity
	 */
	Activity::~Activity(){}
	/*
	 * Adds a module to the modules' list
	 */
	void Activity::addModule(boost::shared_ptr<Module> mod){
		this->m_listModules.push_back(mod);
	}
	/*
	 * returns the modules' list
	 */
	std::vector<boost::shared_ptr<Module> > Activity::getListModules(){
		return this->m_listModules;
	}
	/*
	 * return false in either case, but if the modules' list is empty returns true
	 */
	bool Activity::isEmpty(){return m_listModules.size()==0;}
}