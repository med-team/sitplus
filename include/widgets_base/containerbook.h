/////////////////////////////////////////////////////////////////////////////
// Name:        containerbook.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     23/05/2011 20:24:54
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

#ifndef _CONTAINERBOOK_H_
#define _CONTAINERBOOK_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/notebook.h"
////@end includes
#include "widgets_base/libimpexp_widgetsbase.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_NOTEBOOK 10007
#define SYMBOL_CONTAINERBOOK_STYLE wxNB_DEFAULT|wxNB_NOPAGETHEME
#define SYMBOL_CONTAINERBOOK_IDNAME ID_NOTEBOOK
#define SYMBOL_CONTAINERBOOK_SIZE wxDefaultSize
#define SYMBOL_CONTAINERBOOK_POSITION wxDefaultPosition
////@end control identifiers

//namespace widgets_base {

/*!
 * ContainerBook class declaration
 */

class SPIMPEXP_CLASS_WIDGETSBASE ContainerBook: public wxNotebook
{    
    DECLARE_DYNAMIC_CLASS( ContainerBook )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ContainerBook();
    ContainerBook(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxBK_DEFAULT);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxBK_DEFAULT);

    /// Destructor
    ~ContainerBook();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin ContainerBook event handler declarations

    /// wxEVT_SIZE event handler for ID_NOTEBOOK
    void OnSize( wxSizeEvent& event );

////@end ContainerBook event handler declarations

////@begin ContainerBook member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ContainerBook member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ContainerBook member variables
////@end ContainerBook member variables
};

//};

#endif
    // _CONTAINERBOOK_H_
