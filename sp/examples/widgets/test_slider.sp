#
# SITPLUS script syntax
#
# Line comments start with '#'. Blank lines are also ignored.
#
# Commands
# 	type <id>
#		Sets the type of the component, actually only meaningful when
#		when defining subcomponents (see below)
#
#	name <name>
#		Name given to the component. Must appear at the very beggining
#		of the scripts, otherwise parsing will fail. 
#		It is usually used as a title for GUI elements.	Not needed for 
#		subcomponents.
#
#	create <component type> <name> [args]
#		Creates a new component of a given type with the given name.
#		The type can refer a core registered type or a subcomponent.
#		The name must be unique among the same scope. 
#		args are optional and component dependent (currently subcomponents
#		do not support args)
#
#	connect <src component> <src_pin> <dst component> <dst_pin>
#       Tries to connect src_pin of src component to dst_pin of dst component.
#
#	export_ipin <component> <pin name> [<new name> [<new type>]]
#	export_opin <component> <pin name> [<new name> [<new type>]]
#		Registers the input/output pin of a certain component as if where part of
#		this component and optionally rename it. NOTE: renaming the pin 
#		actually means that	the pin name also changes for the original component.
#		If parameter new type is passed tries to change the type of the pin. 
#		The new type must be only of the types registered in the core.
#		Note that changing the type is only possible for type "any" pins.
#	
#	As a rule of thumb always put the exportation statatement at the end
#	of the script or the subcomponent	
#
# Defining subcomponents
#	
#	Subcomponents allow to define new types of components which are 
#	composed of other components. Use
#		
#		subcomponent
#
#	keyword to begin a subcomponent definition and end it with
#
#		subcomponent_end
#

type testslider
name "Test slider"


create widget_slider s1 --min -100 --max 100 -v 3 -l "This is the label"
create widget_slider s2 --min -100 --max 100

connect s1 value s2 value
connect s2 value s1 value

