type testslider2
name "Test slider2"


create widget_slider s1 --min -0 --max 1000 -v 10 -l "This is the label" --log 2
create widget_slider s2 --min -0 --max 1000

connect s1 value s2 value
connect s2 value s1 value

