#
# 2d_modulus_sign 
# 
# Compute 2d modulus with sign
#
# Input pins
#	x (float)
#	y (float)
#
#	For proper operation, first we expect x and then y
#
# Output pins
#	result (float)
#

type 2d_modulus
name "2d modulus"

# Input pins
create forward x
create forward y
export_ipin x in x
export_ipin y in y

# Square each value
create fmul xsq 
connect x out xsq b
connect x out xsq a

create fmul ysq 
connect y out ysq b
connect y out ysq a

# Add
create fadd sum
connect xsq result sum b
connect ysq result sum a

# Square root
create fsqrt sqrt
connect sum result sqrt a

export_opin sqrt result