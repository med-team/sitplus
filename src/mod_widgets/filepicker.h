/////////////////////////////////////////////////////////////////////////////
// Name:        filepicker.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     18/05/2011 09:45:13
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

#ifndef _FILEPICKER_H_
#define _FILEPICKER_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "spcore/component.h"
#include "spcore/pinimpl.h"
#include "spcore/basictypes.h"

#include <wx/panel.h>

#include <wx/icon.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations
class wxStaticBox;
class wxTextCtrl;

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_FILEPICKERPANEL 10006
#define ID_TEXTCTRL_FILE_PATH 10007
#define ID_BUTTON_CHOOSE 10008
#define SYMBOL_FILEPICKERPANEL_STYLE wxTAB_TRAVERSAL
#define SYMBOL_FILEPICKERPANEL_TITLE _("FilePicker")
#define SYMBOL_FILEPICKERPANEL_IDNAME ID_FILEPICKERPANEL
#define SYMBOL_FILEPICKERPANEL_SIZE wxDefaultSize
#define SYMBOL_FILEPICKERPANEL_POSITION wxDefaultPosition
////@end control identifiers

namespace mod_widgets {

class FilePickerComponent;

/*!
 * FilePickerPanel class declaration
 */

class FilePickerPanel: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( FilePickerPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    FilePickerPanel();
    FilePickerPanel( wxWindow* parent, wxWindowID id = SYMBOL_FILEPICKERPANEL_IDNAME, const wxPoint& pos = SYMBOL_FILEPICKERPANEL_POSITION, const wxSize& size = SYMBOL_FILEPICKERPANEL_SIZE, long style = SYMBOL_FILEPICKERPANEL_STYLE, const wxString& name = SYMBOL_FILEPICKERPANEL_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_FILEPICKERPANEL_IDNAME, const wxPoint& pos = SYMBOL_FILEPICKERPANEL_POSITION, const wxSize& size = SYMBOL_FILEPICKERPANEL_SIZE, long style = SYMBOL_FILEPICKERPANEL_STYLE, const wxString& name = SYMBOL_FILEPICKERPANEL_TITLE );

    /// Destructor
    ~FilePickerPanel();

	// Set destruction callback. NULL removes.
	void SetComponent(FilePickerComponent* component) { m_component= component; }

	// Notify value change
	void ValueChanged();

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin FilePickerPanel event handler declarations

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CHOOSE
    void OnButtonChooseClick( wxCommandEvent& event );

////@end FilePickerPanel event handler declarations
	
	// Value change event
	void OnValueChanged( wxCommandEvent& );

////@begin FilePickerPanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end FilePickerPanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin FilePickerPanel member variables
    wxStaticBox* m_staSizer;
    wxTextCtrl* m_txtFilePath;
////@end FilePickerPanel member variables

	FilePickerComponent* m_component;
};

/**
	widget_filepicker component

	Input pins:
		value (string)

		Sets the initial path of the widget. If the path doesn't exists then
		the widget is not updated nor the path is sent during initialization.

	Output pins:
		value (string)

		Set path.

	Command line:
		[-l <label> ]	Label. If empty, no label is shown.
		[-v <path>]		Set initial value
		[-w <string>]	Wildcard. Ex: *.jpg;*.png
		[-t [d|a]]		Type. d -> directory, a -> regular file (def. a)

	Notes:
		Currently supports only open file pickers
*/
class FilePickerComponent : public spcore::CComponentAdapter {
public:
	static const char* getTypeName() { return "widget_filepicker"; };
	virtual const char* GetTypeName() const { return FilePickerComponent::getTypeName(); };

	//
	// Component methods
	//
	FilePickerComponent(const char * name, int argc, const char * argv[]);
	
	virtual int DoInitialize();

	virtual wxWindow* GetGUI(wxWindow * parent);

	// 
	// Called from GUI
	//	

	// Return true if value successfuly set
	bool SetFilePickerValue (const char* v);
	const char* GetFilePickerValue() const;

	bool GetPickDirectory() const { return m_pickDirectory; }
	
	const std::string& GetLabel() const { return m_label; }
	const std::string& GetWildcard() const { return m_wildcard; }
	
	void OnPanelDestroyed ();

private:
	bool m_pickDirectory;
	FilePickerPanel* m_panel;
	SmartPtr<spcore::IInputPin> m_iPin;
	SmartPtr<spcore::IOutputPin> m_oPin;
	SmartPtr<spcore::CTypeString> m_value;
	std::string m_label;
	std::string m_wildcard;

	virtual ~FilePickerComponent();

	bool IsValid(const char* path) const;

	void OnPinValue (const spcore::CTypeString & msg);

	
	class InputPinValue 
		: public spcore::CInputPinWriteOnly<spcore::CTypeString, FilePickerComponent> {
	public:
		InputPinValue (FilePickerComponent & component)
		: spcore::CInputPinWriteOnly<spcore::CTypeString, FilePickerComponent>("value", component) { }

		virtual int DoSend(const spcore::CTypeString & msg) {
			m_component->OnPinValue(msg);
			return 0;
		}
	};
};

typedef spcore::ComponentFactory<FilePickerComponent> FilePickerComponentFactory;

};	// namespace
#endif
    // _FILEPICKER_H_
