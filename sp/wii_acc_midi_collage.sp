type wiiinstr
name _"Wii Virtual Instrument"

# Create tracker
import lib/wii_acc_motion.sps
create wii_acc_motion grabber

# MIDI player
import lib/motion_midi_player.sps
create motion_midi_player player
connect grabber motion player motion

# Collage
import lib/motion_collage.sps
create motion_collage motion_collage
create send_main_async to_main

connect grabber motion to_main in
connect to_main out motion_collage motion

begin_gui_layout
	layout_begin hbox
		layout_begin vbox
			component grabber
		layout_end
		component player
		layout_begin vbox
			component motion_collage			
		layout_end
	layout_end	
end_gui_layout