#
# Camera based fine motion tracker
#
# Input pins
#	image (iplimage)
#
# Output pins
# 	motion (float)
#

type camera_oflow_motion
name "Motion Tracker"

# Camera source. Create a forwarder to export the pin ...
create forward forward

# and export it as "image"
export_ipin forward in image iplimage
			
# Camera panel ans its roi
create camera_viewer viewer
create roi_storage roi -s 0.3 0.3 -d 1 --color 13934615

connect roi roi viewer roi
connect viewer roi roi roi
connect forward out viewer image	

# Optical flow tracker and its roi
create optical_flow_tracker oftracker
connect roi roi oftracker roi
connect forward out oftracker image

#
# Optical flow output processing
#

# Split output for each axis
create split split -o 1
connect oftracker motion split input

#
# Threshold
#
import around_zero_threshold.sps
create around_zero_threshold thres -lsld _"Motion detection threshold" -lchk _"Motion detected" -max 0.5 -v 0.01
connect split 1 thres in

#
# Sensitivity slider
#
create widget_slider sldsens --min 0 --max 5 -v 1 -l _"Camera sensitivity"
create fmul sens 
connect sldsens value sens b
connect thres out sens a

# Export motion value
export_opin sens result motion

begin_gui_layout
	layout_begin vbox
		component viewer
		component sldsens
		layout_begin collapsible _"Advanced"
			component thres
		layout_end
	layout_end
end_gui_layout
