/////////////////////////////////////////////////////////////////////////////
// Name:        wcameraconfiguration.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#ifndef _WCAMERACONFIGURATION_H_
#define _WCAMERACONFIGURATION_H_


/*!
 * Includes
 */

#include "mod_camera.h"
////@begin includes
////@end includes
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/choice.h>
#include <wx/checkbox.h>

namespace mod_camera {

/*!
 * Forward declarations
 */

////@begin forward declarations
class CameraPanel;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CCAMERACONFIGURATION 10060
#define ID_PANEL_CAMERA 10061
#define ID_CHOICE_SELECTED_CAMERA 10000
#define ID_CHOICE_FORMAT 10001
#define ID_CHOICE_FPS 10002
#define ID_BUTTON_DRIVER_SETTINGS 10064
#define ID_CHECKBOX_MIRROR_IMAGE 10003
#define ID_BUTTON_CLOSE_CCONFIG 10065
#define SYMBOL_CCAMERACONFIGURATION_STYLE wxCAPTION|wxTAB_TRAVERSAL
#define SYMBOL_CCAMERACONFIGURATION_TITLE _("Camera Configuration")
#define SYMBOL_CCAMERACONFIGURATION_IDNAME ID_CCAMERACONFIGURATION
#define SYMBOL_CCAMERACONFIGURATION_SIZE wxDefaultSize
#define SYMBOL_CCAMERACONFIGURATION_POSITION wxDefaultPosition
////@end control identifiers
//#define SYMBOL_CCAMERACONFIGURATION_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL

/*!
 * CCameraConfiguration class declaration
 */

class CCameraConfiguration: public wxPanel, protected mod_camera::CameraCaptureListener
{    
    DECLARE_DYNAMIC_CLASS( CCameraConfiguration )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CCameraConfiguration();
    CCameraConfiguration( wxWindow* parent, wxWindowID id = SYMBOL_CCAMERACONFIGURATION_IDNAME, const wxPoint& pos = SYMBOL_CCAMERACONFIGURATION_POSITION, const wxSize& size = SYMBOL_CCAMERACONFIGURATION_SIZE, long style = SYMBOL_CCAMERACONFIGURATION_STYLE, const wxString& name= SYMBOL_CCAMERACONFIGURATION_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CCAMERACONFIGURATION_IDNAME, const wxPoint& pos = SYMBOL_CCAMERACONFIGURATION_POSITION, const wxSize& size = SYMBOL_CCAMERACONFIGURATION_SIZE, long style = SYMBOL_CCAMERACONFIGURATION_STYLE, const wxString& name= SYMBOL_CCAMERACONFIGURATION_TITLE );

    /// Destructor
    ~CCameraConfiguration();

protected:
	virtual void CameraCaptureCallback (SmartPtr<const mod_camera::CTypeIplImage> img);

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CCameraConfiguration event handler declarations

    /// wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_SELECTED_CAMERA
    void OnChoiceSelectedCameraSelected( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_FORMAT
    void OnChoiceFormatSelected( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHOICE_SELECTED event handler for ID_CHOICE_FPS
    void OnChoiceFpsSelected( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_DRIVER_SETTINGS
    void OnButtonDriverSettingsClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_MIRROR_IMAGE
    void OnCheckboxMirrorImageClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_CLOSE_CCONFIG
    void OnButtonCloseCconfigClick( wxCommandEvent& event );

////@end CCameraConfiguration event handler declarations

////@begin CCameraConfiguration member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end CCameraConfiguration member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

	void PopulateControls();

	spcore::IInputPin* GetCamerasPin();
	spcore::IInputPin* GetSelectedCameraPin();
	spcore::IInputPin* GetCaptureParametersPin();
	spcore::IInputPin* GetMirrorEffectPin();
	spcore::IInputPin* GetSettingsDialogPin();

////@begin CCameraConfiguration member variables
    CameraPanel* m_camPanel;
    wxChoice* m_choSelectedCamera;
    wxChoice* m_choFormat;
    wxChoice* m_choFPS;
    wxCheckBox* m_chkMirrorImage;
////@end CCameraConfiguration member variables
	SmartPtr<mod_camera::CameraConfig> m_cameraConfig;
};

}

#endif
    // _WCAMERACONFIGURATION_H_
