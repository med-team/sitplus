#
# Shows a collage according to input motion
#
# Input pins
#	motion (float)
#	background (sdlsurface)	- Image to use as background
#
# Output pins
#

type motion_collage
name "Motion collage"

# Motion source. Create a forwarder to export the pin and export it as "motion"
create forward motion_ipin
export_ipin motion_ipin in motion float

# Background source.
create forward background_ipin
export_ipin background_ipin in background sdl_surface

# SDL drawer
create sdl_drawer sdl_drawer
connect background_ipin out sdl_drawer queue

#
# Sensitivity
#
create widget_slider sld_sens_collage -v 1 --max 15 --min 0 -l _"Graphical sensitivity" --log
create fmul mul_sens_collage
connect sld_sens_collage value mul_sens_collage b
connect motion_ipin out mul_sens_collage a

#
# XML selector for collage_background
#
# Category
create widget_choice cho_back -l _"Background"
create file_browser fbrow_back -t a -p \"$SP_DATA_DIR$/graphics/mod_collage_xml/backgrounds\"
# Hidden choice used to store full paths
create widget_choice cho_back_hidden
connect fbrow_back files cho_back options
connect fbrow_back paths cho_back_hidden options
connect cho_back selection cho_back_hidden selection

#
# Collage background
#
create collage_graphics collage_graphics_background
connect collage_graphics_background result sdl_drawer queue

# Background animation speed
create widget_slider sld_animation -v 1 --max 5 --min 0 -l _"Animation speed"
connect sld_animation value collage_graphics_background SpeedAnimation


# Gate
create forward gate_background
connect mul_sens_collage result gate_background in
create bcast bcast_back
connect cho_back selection bcast_back in
# Don't send motion when no background is selected
connect bcast_back out gate_background gate
connect bcast_back out sld_animation enable
connect gate_background out collage_graphics_background motion
connect cho_back_hidden selection_string collage_graphics_background file

#
# XML selector for collage_graphics
#

# Category
create widget_choice cho_dirs -l _"Category"
create file_browser fbrow_cat -t d -p \"$SP_DATA_DIR$/graphics/mod_collage_xml/activities\"
# Hidden choice used to store full paths
create widget_choice cho_dirs_hidden
connect fbrow_cat files cho_dirs options
connect fbrow_cat paths cho_dirs_hidden options
connect cho_dirs selection cho_dirs_hidden selection

# Subcategory
create widget_choice cho_files -l _"Subcategory"
create file_browser fbrow_files -t a
connect cho_dirs_hidden selection_string fbrow_files path
# Hidden choice used to store full paths
create widget_choice cho_files_hidden
connect fbrow_files files cho_files options
connect fbrow_files paths cho_files_hidden options
connect cho_files selection cho_files_hidden selection

# File picker (hidden in a collapsible panel)
create widget_collapsible coll_fick -l _"Open file"
create widget_filepicker collage_file -w "*.xml;*.XML" 
# -l "Collage file" -v $SP_DATA_DIR$/mod_collage/XML/collage/collage.xml

# Hide choices when user picks file manually
create not not
connect coll_fick expanded not a
connect not result cho_dirs enable
connect not result cho_files enable

#
# Collage graphics
# 
create collage_graphics collage_graphics
connect cho_files_hidden selection_string collage_graphics file
connect collage_file value collage_graphics file
connect mul_sens_collage result collage_graphics motion
connect collage_graphics result sdl_drawer queue

# Trigger draw
connect mul_sens_collage result sdl_drawer draw

# Change effects
create widget_button next_effect -l _"Next effect (when available)"
connect next_effect pressed collage_graphics NextScene

#
# Advanced controls
#

# Maximum slider
create widget_slider sld_max -v 20 --max 100 --min 1 -i -l _"Maximum number of objects"
connect sld_max value collage_graphics maximum

# Vanish objects
create widget_checkbox chk_vanish -v false -l _"Automatically vanish objects"
connect chk_vanish value collage_graphics vanish

begin_gui_layout
	layout_begin vbox
		layout_begin vbox _"Foreground"
			layout_begin hbox
				component cho_dirs
				component cho_files
			layout_end
			layout_begin hbox
				component next_effect
			layout_end
			layout_begin component coll_fick
				component collage_file
			layout_end
			layout_begin collapsible _"Advanced"
				component sld_max
				component chk_vanish
				component sld_sens_collage		
			layout_end
		layout_end
		layout_begin vbox _"Background"
			component cho_back	
			component sld_animation
		#	layout_begin collapsible _"Advanced"
		#		layout_begin vbox					
		#			component collage_graphics
		#		layout_end
		#	layout_end
		layout_end
	layout_end
end_gui_layout