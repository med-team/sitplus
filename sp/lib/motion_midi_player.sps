#
# Motion based MIDI player
#
# Input pins
#	motion (float)
#
# Output pins
#	notes_played (int)
#

type motion_midi_player
name "MIDI player"

#
# Compute score position
#

# Sensitivity adjustment
create fmul fmul_sens
# "motion" -> sensitivity multiplier
export_ipin fmul_sens a motion

# Slider
create widget_slider sldsens --min 0 --max 10 -v 4 -l _"MIDI sensitivity" --log -2
connect sldsens value fmul_sens b

# Accumulate motion
#create faccum faccum
#connect fmul_sens result faccum val

#
# MIDI player
#
create score_player player -c 0
create midi_out midi_out
connect player note midi_out message
#connect faccum result player pointer
connect fmul_sens result player progress

#
# Instrument selector
#
create instrument_selector is -s "1 13 17 25 33 41 49 57 65 73 81 89 97 105 113"
create widget_choice cho_instr -l _"Instrument" 
connect is instruments cho_instr options
connect cho_instr selection is instrument
connect is midi_number player instrument

#
# Score selector
#
create textfile_dump td
create widget_choice cho_scores
create file_browser fbrow_scores -p \"$SP_DATA_DIR$/scores\"
# Hidden choice used to store full paths
create widget_choice cho_scores_hidden
connect fbrow_scores files cho_scores options
connect fbrow_scores paths cho_scores_hidden options
connect cho_scores selection cho_scores_hidden selection
connect cho_scores_hidden selection_string td path
# Advanced picker
create widget_collapsible coll_fick -l _"Open from file"
create widget_filepicker pick -w "*.sc;*.SC|*.*"
connect pick value td path
connect td contents player score
# Hide choices when user picks file manually
create not not
connect coll_fick expanded not a
connect not result cho_scores enable

#
# Volume
#
create widget_slider sld_vol --min 0 --max 127 -v 127 -i
connect sld_vol value player volume

#
# Note duration
#
create widget_slider sld_dur -l _"Note duration" --min 50 --max 5000 -v 2000 -i
connect sld_dur value player duration

#
# Score wrapping
#
create widget_checkbox chk_wrap -l _"Cyclic score" -v true
connect chk_wrap value player wrap

#
# Output pins
#
export_opin player notes_played

begin_gui_layout
	layout_begin vbox
		layout_begin vbox _"Volume"
			component sld_vol
		layout_end
		component cho_instr
		layout_begin vbox _"Musical score"
			component cho_scores
			layout_begin component coll_fick
				component pick
			layout_end
		layout_end
		layout_begin collapsible _"Advanced"			
			component sld_dur
			component sldsens
			component chk_wrap
		layout_end
	layout_end
end_gui_layout
