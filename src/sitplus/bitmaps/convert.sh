for i in *.png; do
        outname=`echo $i | cut -d '.' -f 1`
        convert $i -resize 32x32 $outname.xpm
	sed -i 's/static\ char\ \*/static\ const\ char\ \*/g' $outname.xpm
done
