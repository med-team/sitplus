cmake_minimum_required(VERSION 2.6)
project(spmod_sdl)

set (SDL_BUILDING_LIBRARY ON)
find_package ( SDL REQUIRED )

set(spmod_sdl_SRCS
	${CUSTOM_INCLUDE_PATH}/mod_sdl/sdlsurfacetype.h
	mod_sdl.cpp
)

set(mod_sdl_gui_SRCS
	config_gui.h
	config_gui.cpp
)

include_directories (${SDL_INCLUDE_DIR})

# Base library
add_library (spmod_sdl MODULE ${spmod_sdl_SRCS})
target_link_libraries(spmod_sdl spcore)
target_link_libraries(spmod_sdl ${SDL_LIBRARY})

# Base gui library
add_library (mod_sdl_gui MODULE ${mod_sdl_gui_SRCS})
target_link_libraries(mod_sdl_gui spcore)
target_link_libraries(mod_sdl_gui ${wxWidgets_LIBRARIES})

IF(BUILD_TESTS)
	ADD_SUBDIRECTORY(tests)
ENDIF(BUILD_TESTS)

INSTALL (TARGETS spmod_sdl RUNTIME DESTINATION ${LIBRUNTIMEDIR} LIBRARY DESTINATION ${PLUGINDIR})

IF (WIN32)
	FIND_PATH (SDL_RUNTIME_LIBS_DIR SDL.dll
		HINTS
		$ENV{SDLDIR}
		PATH_SUFFIXES lib64 lib
		PATHS)
	FILE (GLOB SDL_RUNTIME_LIBS ${SDL_RUNTIME_LIBS_DIR}/*.dll)
	
	INSTALL (PROGRAMS ${SDL_RUNTIME_LIBS} DESTINATION ${LIBRUNTIMEDIR})
ENDIF (WIN32)


