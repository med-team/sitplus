/////////////////////////////////////////////////////////////////////////////
// File:        mod_puredata.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "spcore/component.h"
#include "spcore/module.h"
#include "spcore/basictypes.h"
#include "spcore/libimpexp.h"
#include "spcore/pinimpl.h"

#include "puredatawrapper.h"
#include "puredataconfig.h"
#include "playwithvoice.h"

#include <string>

using namespace spcore;

namespace mod_puredata {
/* ******************************************************************************
****************************************************************************** */
class PureDataModule : public CModuleAdapter {
public:
	PureDataModule() {		

		//
		// components
		//
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new PureDataConfigFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new PlayWithVoiceComponentFactory(), false));
	}
	virtual const char * GetName() const { return "mod_puredata"; }
	virtual ~PureDataModule() {
		PureDataController::destroyInstance();
	}
};

static PureDataModule* g_module= NULL;
SPEXPORT_FUNCTION spcore::IModule* module_create_instance()
{
	if (g_module== NULL) g_module= new PureDataModule();
	return g_module;
}

};
