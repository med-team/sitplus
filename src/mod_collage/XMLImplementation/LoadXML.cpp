/*
    LoadXML.cpp -- Initializes the loading of the XML file
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "InterfaceXML.h"

#include "Poco/SAX/InputSource.h"
#include "Poco/SAX/SAXParser.h"
#include "Poco/SAX/WhitespaceFilter.h"
#include "Poco/SAX/XMLReader.h"
#include "Poco/SAX/SAXException.h"

using boost::shared_ptr;
using Poco::XML::XMLReader;
using namespace std;

namespace XMLImplementation{
	/*
	 * Constructor of class LoadXML. Given the path and the file name of XML, loads it and lets to recover the configuration
	 * and the errors. The images are stored on the data base given.
	 *  
	 */
	LoadXML::LoadXML (string dir, string file, shared_ptr<DBImages> images){
		#ifdef _WIN32
			dir += "\\";
		#else
			dir += "/";  
		#endif
		try
		{
			Poco::XML::InputSource src(dir+file);
			Poco::XML::SAXParser reader;
			Poco::XML::WhitespaceFilter filter(&reader);
			reader.setFeature(XMLReader::FEATURE_NAMESPACES, true);
			reader.setFeature(XMLReader::FEATURE_NAMESPACE_PREFIXES, true);
			//Initializing attributes
			m_configuracio = shared_ptr<Activity>(new Activity);
			m_errors = shared_ptr<LogError>(new LogError);
			//creating XMLHandler
			shared_ptr<XMLHandler> handler(new XMLHandler(dir,m_configuracio,m_errors,images));
			reader.setContentHandler(handler.get());
			//parsing the XML file
			reader.parse(&src);
		}
		catch(Poco::FileNotFoundException&){
			//If the file doesn't exists
			m_errors = shared_ptr<LogError>(new LogError);
			shared_ptr<Error> e = shared_ptr<Error>(new Error(-16,"","",0,"Fatal error: File configuration doesn't exist."));
			m_errors.get()->addError(e);
		}
		catch(Poco::XML::SAXParseException& exc){
			//If the file has a malformed tag or duplicated attributes or mismatched tags
			m_errors = shared_ptr<LogError>(new LogError);
			string s = "Fatal error: File configuration malformed.";
			s += " --> ";
			s += exc.message();
			shared_ptr<Error> e = shared_ptr<Error>(new Error(-17,"","",exc.getLineNumber(),s));
			m_errors.get()->addError(e);
		}
		catch (Poco::Exception&)
		{
			//If the file has an unknown error (probably read error)
			m_errors = shared_ptr<LogError>(new LogError);
			shared_ptr<Error> e = shared_ptr<Error>(new Error(-18,"","",0,"Fatal error: Unknown error."));
			m_errors.get()->addError(e);
		}
	}
	/*
	 * Destructor from class LoadXML
	 */
	LoadXML::~LoadXML(){}
	/*
	 * returns the configuration of XML, gives an Activity object
	 */
	shared_ptr<Activity> LoadXML::getConfiguration(){
		return m_configuracio;
	}
	/*
	 * return errors from XML, a LogError object
	 */
	shared_ptr<LogError> LoadXML::getErrors(){
		return m_errors;
	}
}

