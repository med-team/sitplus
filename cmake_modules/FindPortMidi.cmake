# - Try to find PortMidi
# Once done, this will define
#
#  PortMidi_FOUND - system has PortMidi
#  PortMidi_INCLUDE_DIRS - the PortMidi include directories
#  PortMidi_LIBRARIES - link these to use PortMidi
#

include(FindPkgMacros)

findpkg_begin(PortMidi)

# Uses environtment variable as a hint
getenv_path(PORTMIDIDIR)
set(PortMidi_PREFIX_PATH ${ENV_PORTMIDIDIR} )

# redo search if prefix path changed
clear_if_changed(PortMidi_PREFIX_PATH
	PortMidi_LIBRARY_REL
	PortMidi_LIBRARY_DBG
	PortMidi_INCLUDE_DIR
)

create_search_paths(PortMidi)

set(PortMidi_INC_SEARCH_PATH ${PortMidi_INC_SEARCH_PATH} ${PortMidi_PREFIX_PATH})

set(PortMidi_LIBRARY_NAMES portmidi)
get_debug_names(PortMidi_LIBRARY_NAMES)

find_path(PortMidi_INCLUDE_DIR NAMES portmidi.h HINTS ${PortMidi_INC_SEARCH_PATH})

find_library(PortMidi_LIBRARY_REL NAMES ${PortMidi_LIBRARY_NAMES} HINTS ${PortMidi_LIB_SEARCH_PATH} PATH_SUFFIXES Linux/i686)

find_library(PortMidi_LIBRARY_DBG NAMES ${PortMidi_LIBRARY_NAMES_DBG} HINTS ${PortMidi_LIB_SEARCH_PATH} PATH_SUFFIXES Linux/i686)

make_library_set(PortMidi_LIBRARY)

findpkg_finish(PortMidi)
