type testcollapsible
name "Test collapsible component"

create widget_collapsible coll -l "The label"
create widget_checkbox chk -l "Panel expanded"

connect coll expanded chk value

# Create more stuff just to fill the dialogue
create widget_slider sld1 -l "Slider 1"
create widget_slider sld2 -l "Slider 2"

begin_gui_layout
	layout_begin vbox
		component chk
		layout_begin component coll
			component sld1
			component sld2
		layout_end
	layout_end
end_gui_layout

