/////////////////////////////////////////////////////////////////////////////
// Name:        cameradialog2.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     27/01/2011 10:52:27
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////

#ifndef _CAMERADIALOG2_H_
#define _CAMERADIALOG2_H_


/*!
 * Includes
 */

////@begin includes
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
class CCamWindow2;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_CAMERADIALOG 10032
#define ID_PANEL3 10033
#define SYMBOL_CAMERADIALOG_STYLE wxCAPTION|wxRESIZE_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_CAMERADIALOG_TITLE _("Camera Dialog")
#define SYMBOL_CAMERADIALOG_IDNAME ID_CAMERADIALOG
#define SYMBOL_CAMERADIALOG_SIZE wxDefaultSize
#define SYMBOL_CAMERADIALOG_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * CameraDialog class declaration
 */

class CameraDialog: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( CameraDialog )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    CameraDialog();
    CameraDialog( wxWindow* parent, wxWindowID id = SYMBOL_CAMERADIALOG_IDNAME, const wxPoint& pos = SYMBOL_CAMERADIALOG_POSITION, const wxSize& size = SYMBOL_CAMERADIALOG_SIZE, long style = SYMBOL_CAMERADIALOG_STYLE, const wxString& name= SYMBOL_CAMERADIALOG_TITLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_CAMERADIALOG_IDNAME, const wxPoint& pos = SYMBOL_CAMERADIALOG_POSITION, const wxSize& size = SYMBOL_CAMERADIALOG_SIZE, long style = SYMBOL_CAMERADIALOG_STYLE, const wxString& name= SYMBOL_CAMERADIALOG_TITLE );

    /// Destructor
    ~CameraDialog();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin CameraDialog event handler declarations

    /// wxEVT_CLOSE_WINDOW event handler for ID_CAMERADIALOG
    void OnCloseWindow( wxCloseEvent& event );

////@end CameraDialog event handler declarations

////@begin CameraDialog member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end CameraDialog member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin CameraDialog member variables
    CCamWindow2* m_camWindow;
////@end CameraDialog member variables
};

#endif
    // _CAMERADIALOG2_H_
