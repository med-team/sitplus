/////////////////////////////////////////////////////////////////////////////
// File:        mod_widgets.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "spcore/libimpexp.h"
#include "spcore/module.h"
#include "slider.h"
#include "checkbox.h"
#include "filepicker.h"
#include "choice.h"
#include "collapsible.h"
#include "button.h"

using namespace spcore;

namespace mod_widgets {

/* ******************************************************************************
	widgets module
*/
class WidgetsModule : public spcore::CModuleAdapter {
public:
	WidgetsModule() {
		
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new SliderComponentFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new CheckboxComponentFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new FilePickerComponentFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new ChoiceComponentFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new CollapsibleComponentFactory(), false));
		RegisterComponentFactory(SmartPtr<spcore::IComponentFactory>(new ButtonComponentFactory(), false));
	}
	virtual const char * GetName() const { return "mod_widgets"; }
};

static spcore::IModule* g_module= NULL;
SPEXPORT_FUNCTION spcore::IModule* module_create_instance()
{
	if (g_module== NULL) g_module= new WidgetsModule();
	return g_module;
}

};