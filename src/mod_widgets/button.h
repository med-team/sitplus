/////////////////////////////////////////////////////////////////////////////
// Name:        button.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     03/06/2011 18:34:49
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.    
/////////////////////////////////////////////////////////////////////////////

#ifndef _BUTTON_H_
#define _BUTTON_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "base_widget.h"

#include <wx/button.h>

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_BUTTON -1
#define SYMBOL_BUTTONPANEL_STYLE 0
#define SYMBOL_BUTTONPANEL_IDNAME ID_BUTTON
#define SYMBOL_BUTTONPANEL_SIZE wxDefaultSize
#define SYMBOL_BUTTONPANEL_POSITION wxDefaultPosition
////@end control identifiers

namespace mod_widgets {

class ButtonComponent;

/*!
 * ButtonPanel class declaration
 */

class ButtonPanel: public wxButton, public BaseWidgetPanel<ButtonComponent>
{    
    DECLARE_DYNAMIC_CLASS( ButtonPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ButtonPanel();
    ButtonPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& label = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator);

    /// Destructor
    ~ButtonPanel();

private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin ButtonPanel event handler declarations

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON
    void OnButtonClick( wxCommandEvent& event );

////@end ButtonPanel event handler declarations

////@begin ButtonPanel member function declarations

////@end ButtonPanel member function declarations

////@begin ButtonPanel member variables
////@end ButtonPanel member variables
};

/**
	widget_button component

	Input pins:
		enable (bool)	Enable/disable control

	Output pins:
		pressed (bool)	True when pressed

	Command line:
		[-e [0|1|false|true]]	Enabled. Control will be initially enabled (def. 1)
		[-l <label> ]	Label. If empty, no label is shown.
*/
class ButtonComponent : public BaseWidgetComponent<ButtonPanel,ButtonComponent> {
public:
	static const char* getTypeName() { return "widget_button"; };
	virtual const char* GetTypeName() const { return ButtonComponent::getTypeName(); };

	//
	// Component methods
	//
	ButtonComponent(const char * name, int argc, const char * argv[]);
	
	virtual int DoInitialize() { return 0; }

	// 
	// Called from GUI
	//	

	void Pressed();	

private:
	//
	// Data members
	//
	SmartPtr<spcore::IOutputPin> m_oPinPressed;
	SmartPtr<spcore::CTypeBool> m_valuePressed;

	//
	// Private methods
	//
	

	//
	// Input pin classes
	//
};

typedef spcore::ComponentFactory<ButtonComponent> ButtonComponentFactory;
};	// namespace

#endif
    // _BUTTON_H_
