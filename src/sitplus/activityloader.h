/////////////////////////////////////////////////////////////////////////////
// Name:        activityloader.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     08/06/2011 11:39:47
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
/////////////////////////////////////////////////////////////////////////////

#ifndef _ACTIVITYLOADER_H_
#define _ACTIVITYLOADER_H_


/*!
 * Includes
 */

////@begin includes
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_ACTIVITYLOADER 10012
#define ID_CHECKBOX_CAM 10013
#define ID_CHECKBOX_VOICE 10015
#define ID_CHECKBOX_WII 10016
#define ID_CHECKBOX_USE_MOTION_PLUS 10024
#define SYMBOL_ACTIVITYLOADER_STYLE wxCAPTION|wxRESIZE_BORDER|wxSYSTEM_MENU|wxCLOSE_BOX|wxTAB_TRAVERSAL
#define SYMBOL_ACTIVITYLOADER_TITLE _("ActivityLoader")
#define SYMBOL_ACTIVITYLOADER_IDNAME ID_ACTIVITYLOADER
#define SYMBOL_ACTIVITYLOADER_SIZE wxSize(400, 300)
#define SYMBOL_ACTIVITYLOADER_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * ActivityLoader class declaration
 */

class ActivityLoader: public wxDialog
{    
    DECLARE_DYNAMIC_CLASS( ActivityLoader )
    DECLARE_EVENT_TABLE()

public:
	enum { INPUT_CAM= 0x1, INPUT_VOICE= 0x2, INPUT_WII= 0x4 };

    /// Constructors
    ActivityLoader();
    ActivityLoader( wxWindow* parent, wxWindowID id = SYMBOL_ACTIVITYLOADER_IDNAME, const wxString& caption = SYMBOL_ACTIVITYLOADER_TITLE, const wxPoint& pos = SYMBOL_ACTIVITYLOADER_POSITION, const wxSize& size = SYMBOL_ACTIVITYLOADER_SIZE, long style = SYMBOL_ACTIVITYLOADER_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_ACTIVITYLOADER_IDNAME, const wxString& caption = SYMBOL_ACTIVITYLOADER_TITLE, const wxPoint& pos = SYMBOL_ACTIVITYLOADER_POSITION, const wxSize& size = SYMBOL_ACTIVITYLOADER_SIZE, long style = SYMBOL_ACTIVITYLOADER_STYLE );

    /// Destructor
    ~ActivityLoader();

	unsigned int GetSelection() const { return m_selection; }

	bool GetUseMotionPlus() const;
private:
    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin ActivityLoader event handler declarations

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_CAM
    void OnCheckboxCamClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_VOICE
    void OnCheckboxVoiceClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_WII
    void OnCheckboxWiiClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_USE_MOTION_PLUS
    void OnCheckboxUseMotionPlusClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
    void OnOkClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
    void OnCancelClick( wxCommandEvent& event );

////@end ActivityLoader event handler declarations

////@begin ActivityLoader member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ActivityLoader member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ActivityLoader member variables
    wxCheckBox* m_chkCam;
    wxCheckBox* m_chkVoice;
    wxCheckBox* m_chkWii;
    wxCheckBox* m_chkUseMotionPlus;
////@end ActivityLoader member variables
	unsigned int m_selection;
};

#endif
    // _ACTIVITYLOADER_H_
