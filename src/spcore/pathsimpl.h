/////////////////////////////////////////////////////////////////////////////
// File:        paths.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#ifdef SPCORE_PATHSIMPL_H
#error __FILE__ included twice
#endif
#define SPCORE_PATHSIMPL_H

#include "spcore/paths.h"
#include "config.h"

#include <string.h>

#ifdef ENABLE_WXWIDGETS
#include <wx/stdpaths.h>
#endif

namespace spcore {

/**
	Interface class for path reporting class
**/
class Paths : public IPaths {
protected:
	virtual ~Paths() {}

public:
	Paths() { }

	virtual const char* GetDataDir() const {
		if (m_dataDir.empty()) {
			// Not set, first try environtment variable
			const char* dataDir= getenv("SP_DATA_DIR");
			if (dataDir) m_dataDir= dataDir;
#ifdef ENABLE_WXWIDGETS
			// TODO: provide an implementation not based on wx
			if (!dataDir){
				//assert (m_wxInitialized);
				m_dataDir= wxStandardPaths::Get().GetDataDir().mb_str();
			}
#else
	#error "Not implemented yet"
#endif
			// Convert backslahes into slashes
			for (unsigned int i= 0; i< m_dataDir.size(); ++i)
				if (m_dataDir[i]== '\\') m_dataDir[i]= '/';
		}
		
		return m_dataDir.c_str();
	}

	virtual void SetDataDir (const char* dir) {	if (dir) m_dataDir= dir; }

	virtual const char* GetUserDataDir() const {
		if (m_userDataDir.empty()) {
			// Not set, first try environtment variable
			const char* userDataDir= getenv("SP_USER_DATA_DIR");
			if (userDataDir) m_userDataDir= userDataDir;
			else {
#ifdef WIN32
				const char* baseUserDataDir= getenv("APPDATA");
				if (!baseUserDataDir) return NULL;
				m_userDataDir+= baseUserDataDir;
				m_userDataDir+= "/";
				m_userDataDir+= APP_NAME;
#else
				const char* baseUserDataDir= getenv("HOME");
				if (!baseUserDataDir) return NULL;
				m_userDataDir+= baseUserDataDir;
				m_userDataDir+= "/.";
				m_userDataDir+= APP_NAME;
#endif
			}
		}
		return m_userDataDir.c_str();
	}

	virtual void SetUserDataDir (const char* dir) {	if (dir) m_userDataDir= dir; }

	virtual const char* GetLocalesDir() const {
		if (m_localeDir.empty()) {
			const char* localeDir= getenv("SP_LOCALE_DIR");
			if (localeDir) m_localeDir= localeDir;
			else {
#ifdef WIN32
				// On Windows rely on the location of the binary
				const char* dataDir= GetDataDir();
				if (!dataDir) return NULL;
				m_localeDir= dataDir;
				m_localeDir+= "/locale";
#else
				// On linux locales are supposed to be found in the standard location
				m_localeDir= INSTALL_PREFIX;
				m_localeDir+= "/share/locale";
#endif	// WIN32
			}
		}

		return m_localeDir.c_str();
	}

	virtual void SetLocalesDir (const char* dir) { if (dir) m_localeDir= dir; }



	virtual const char* GetPluginsDir() const {
		if (m_pluginsDir.empty()) {
			const char* pluginsDir= getenv("SP_PLUGINS_DIR");
			if (pluginsDir) m_pluginsDir= pluginsDir;
			else {
#ifdef WIN32
				// On Windows rely on the location of the binary
				const char* dataDir= GetDataDir();
				if (!dataDir) return NULL;
				m_pluginsDir= dataDir;
#else
				// On linux Plugins are supposed to be found in the standard location
				m_pluginsDir= INSTALL_PREFIX;
				m_pluginsDir+= "/lib/sitplus";
#endif	// WIN32
			}
		}

		return m_pluginsDir.c_str();
	}

	virtual void SetPluginsDir (const char* dir) { if (dir) m_pluginsDir= dir; }

private:
	mutable std::string m_dataDir;
	mutable std::string m_userDataDir;
	mutable std::string m_localeDir;
	mutable std::string m_pluginsDir;
};

} // namespace spcore

