
type test_ipl2sdl2
name "IPL two SDL"

# Camera source. 
create camera_grabber grabber

import ../../lib/ipl2sdl.sps
create ipl2sdl_chk ipl2sdl_chk

# Camera panel 
create camera_viewer viewer
connect grabber image viewer image

connect grabber image ipl2sdl_chk image

# SDL drawer
create sdl_drawer sdl_drawer
connect ipl2sdl_chk surface sdl_drawer queue
connect ipl2sdl_chk surface sdl_drawer draw

begin_gui_layout
	layout_begin vbox
		component viewer
		component ipl2sdl_chk
	layout_end
end_gui_layout
