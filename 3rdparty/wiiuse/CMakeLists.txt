cmake_minimum_required(VERSION 2.6)
project(wiiuselib)

set (WIIUSE_MAJOR_VERSION 0)
set (WIIUSE_RELEASE_VERSION 13)
set (WIIUSE_EPOCH_VERSION 0)

#
# Set default build mode
#

IF(NOT CMAKE_BUILD_TYPE)
  SET(CMAKE_BUILD_TYPE Debug CACHE STRING
      "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
      FORCE)
ENDIF(NOT CMAKE_BUILD_TYPE)

IF (WIN32)
	# On WIN32 force shared lib because wiiuse depends on many specific libraries
	SET(BUILD_SHARED_LIBS ON)
ELSE (WIN32)
	IF(NOT DEFINED BUILD_SHARED_LIBS)
		SET(BUILD_SHARED_LIBS OFF CACHE BOOL 
			"Build shared libraries as default?")
	ENDIF(NOT DEFINED BUILD_SHARED_LIBS)
ENDIF(WIN32)

#
# Compilation flags
#

#IF (CMAKE_BUILD_TYPE STREQUAL "Debug")
#	ADD_DEFINITIONS (-DWITH_WIIUSE_DEBUG)
#ENDIF()

IF (CMAKE_COMPILER_IS_GNUCC)
	ADD_DEFINITIONS (-Wall)
ENDIF()

SET (WIIUSE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/include")
INCLUDE_DIRECTORIES(${WIIUSE_INCLUDE_DIRECTORIES})

IF (NOT EXECUTABLE_OUTPUT_PATH)
	set (EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
ENDIF()

IF (NOT LIBRARY_OUTPUT_PATH)
	set (LIBRARY_OUTPUT_PATH ${EXECUTABLE_OUTPUT_PATH})
ENDIF()

SET(WIIUSE_BUILD_EXAMPLES OFF CACHE BOOL "Build examples")

add_subdirectory(src)

IF (WIIUSE_BUILD_EXAMPLES)
	add_subdirectory(example)
	add_subdirectory(example-sdl)
ENDIF()
