/**
* @file		baseobj.h
* @brief	Reference counted base class for all other classes
* @author	Cesar Mauri Loba (cesar at crea-si dot com)
*	
* -------------------------------------------------------------------------
*
* Copyright:   (C) 2010 Cesar Mauri Loba - CREA Software Systems
* 
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPCORE_BASEOBJ_H
#define SPCORE_BASEOBJ_H

#include <assert.h>
#include <boost/detail/atomic_count.hpp>
#include <boost/intrusive_ptr.hpp>
#include "spcore/libimpexp.h"

#ifdef _MSC_VER
#pragma warning(push)
// 'm_A' : class 'A' needs to have dll-interface to be used by clients of class 'B'
#pragma warning(disable: 4251)
// non dll-interface class 'A' used as base for dll-interface class 'B'
#pragma warning(disable: 4275)
#endif // _MSC_VER

namespace spcore {

/*
	Smart pointers.

	Here we use the instrusive_ptr from boost library which we expose
	under the alias name "SmartPtr"

	For convenience, although is ugly, we use define's to declare
	the alias instead of the following idiom:

	template <typename T>
	struct SmartPtr { typedef boost::intrusive_ptr<T> type; };

	Doing so to use the smart pointer we should write something like:

	SmartPtr<MyType>::type

	which complicates the syntax besides other problems. See:

	http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2003/n1449.pdf
*/
/**
	@todo, TODO: implement it using the template alias feature of the new
	C++0x standard when it become more widespread.		
*/	
#define SmartPtr boost::intrusive_ptr
#define smartptr_static_cast boost::static_pointer_cast
#define smartptr_dynamic_cast boost::dynamic_pointer_cast

/**
	@brief Reference counted base class for all other classes

	This class is used as base to derive other classes within
	sitplus-lib that need to be reference counted.

	Note that this implementation relies on inline methods due to
	its simplicity and to make sure that the "delete this" instruction
	it is defined within the specific module boundaries. In fact, on
	WIN32 this class does no generate linkage.
*/
class IBaseObject {
protected:
	virtual ~IBaseObject() {}
	/**
		@brief Protected constructor.
		
		Initialises reference counter to 1.
	*/
	IBaseObject() : m_refCount(1) {}

public:
	/**
		@brief Increment reference counter.

		This method is thread safe.
	*/
	void AddRef() const {
		assert (m_refCount> 0);
  		++m_refCount;
	}

	/**
		@brief Decrement reference counter.

		Decrement reference counter and destroy the instance if it reaches 0.
		This method is thread safe.
	*/
	void Release() const  {
  		if (--m_refCount== 0) Destroy();
	}

protected:
	/**
		@brief Delete current object (this). 
		
		Inline, so ensure deletion is carried out within module.
	*/
	void Destroy() const {
		delete this;
	}

	/**
		@brief Assignment operator.
	*/
	IBaseObject& operator=( const IBaseObject& ) {
		// Does nothing. The reference counter should never be copied
		return *this;
	}

private:
	// Copy constructor. Disabled
	IBaseObject ( const IBaseObject& );

	/**
		@brief Atomic reference counter.
	*/
	mutable boost::detail::atomic_count m_refCount;
};

} // namespace spcore

namespace boost
{
inline void intrusive_ptr_add_ref(const ::spcore::IBaseObject * p) {
	p->AddRef();
}
inline void intrusive_ptr_release(const ::spcore::IBaseObject * p) {
	p->Release();
}
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif
