/*
    Implementation.h -- Contains the headers so that XMLHandler is able to transform XML code into this objects
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http:/
*/

#ifndef XMLIMPLEMENTATION_H
#define XMLIMPLEMENTATION_H

#include <vector>
#include <map>
#include "mod_sdl/sdlsurfacetype.h"
#include "boost/shared_ptr.hpp"

namespace XMLImplementation{
	class Activity;
	class Module;
	class Picture;
	class DBImages;
	
	/*
	 * This enumeration is to inform kernels about his behaviour.
	 */
	enum delayType {NIL,NODELAY,RAND,MOTION};
}

#include "../Pictures/Pictures.h"
#include "../Pictures/Factories.h"
#include "../Kernel/kernel.h"

namespace XMLImplementation{
	/*
	 * This class represents the tag 'picture' from XML file
	 */
	class Picture{
	public:
		Picture(std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > surfaces,int quantity,float scale=0.25f, float x=0.0f, float y=0.0f);
		~Picture();
		int getQuantity();
		float getScale();
		void setTransitionInFactory(std::vector<boost::shared_ptr<Pictures::ITransitionFactory> > in);
		void setTransitionOutFactory(std::vector<boost::shared_ptr<Pictures::ITransitionFactory> > out);
		boost::shared_ptr<Pictures::PictureNode> getPictureNode(float rx, float ry,float rz);
		float getPointX();
		float getPointY();
	private:		
		int m_quantity;
		float m_scale;
		int m_actualImage;
		std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > m_surfaces;
		std::vector<boost::shared_ptr<Pictures::ITransitionFactory> > m_transInFactory;
		std::vector<boost::shared_ptr<Pictures::ITransitionFactory> > m_transOutFactory;
		float m_x,m_y;
	};
	/*
	 * This class represents the tag 'module' from XML file
	 */
	class Module{
	public:
		Module(boost::shared_ptr<Kernel::AbstractKernelFactory> akf, delayType delay, std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > lBg, float lapse=-1.0f);
		~Module();
		void addPicture(boost::shared_ptr<Picture> image);
		boost::shared_ptr<Picture> getPicture(unsigned int index);
		std::vector<boost::shared_ptr<Picture> > getListPictures();
		boost::shared_ptr<Kernel::AbstractKernel> getKernel(boost::shared_ptr<Module> m);
		delayType getDelayType();
		float getLapseAnimation();
		std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > getListSrcBg();
		bool isEmpty();
	private:
		std::vector<boost::shared_ptr<Picture> > m_listPictures;
		std::vector<SmartPtr<mod_sdl::CTypeSDLSurface> > m_listSrcBg;
		float m_lapseAnim;
		boost::shared_ptr<Kernel::AbstractKernelFactory> m_abstractKernelFactory;
		delayType m_delay;
	};
	/*
	 * This class represents the tag 'activity' from XML file
	 */
	class Activity{
	public:
		~Activity();
		void addModule(boost::shared_ptr<Module>  mod);
		std::vector<boost::shared_ptr<Module> > getListModules();
		bool isEmpty();
	private:
		std::vector<boost::shared_ptr<Module> > m_listModules;
	};
	/*
	 * This structure represents is used to mantein ordenated the images in the DBImages' map
	 */
	struct Classcomp {
		bool operator() (std::string s1, std::string s2) const;
	};
	/*
	 * This class loads and stores the images given by the XML file
	 */
	class DBImages {
		public:
			~DBImages();
			static boost::shared_ptr<DBImages> create();
			SmartPtr<mod_sdl::CTypeSDLSurface> getImage(std::string src,int degRot=0);
		private:
			DBImages();
			std::map<std::string,SmartPtr<mod_sdl::CTypeSDLSurface>,Classcomp> m_images;
	};
};

#endif

