/////////////////////////////////////////////////////////////////////////////
// Name:        config_gui.h
// Purpose:     
// Author:      C�sar Mauri Loba
// Modified by: 
// Created:     Wed 09 Mar 2011 11:27:16 CET
// RCS-ID:      
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.   
/////////////////////////////////////////////////////////////////////////////
#ifndef _CONFIG_GUI_H_
#define _CONFIG_GUI_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/filepicker.h"
////@end includes
#include "wx/wx.h"

#include "spcore/pin.h"

using namespace spcore;

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxFilePickerCtrl;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_COLLAGECONFIGURATION 10002
#define ID_FILEPICKERCTRL 10000
#define ID_SLIDER_SENS 10001
#define ID_SLIDER_DEADZONE 10003
#define ID_SLIDER_MAXIMUM 10005
#define ID_SLIDER_SPEED 10008
#define ID_CHECKBOX_VANISH 10004
#define ID_BUTTON_PREV 10006
#define ID_BUTTON_NEXT 10007
#define SYMBOL_COLLAGECONFIGURATIONGUI_STYLE 0
#define SYMBOL_COLLAGECONFIGURATIONGUI_TITLE _("Collage Configuration")
#define SYMBOL_COLLAGECONFIGURATIONGUI_IDNAME ID_COLLAGECONFIGURATION
#define SYMBOL_COLLAGECONFIGURATIONGUI_SIZE wxSize(400, 300)
#define SYMBOL_COLLAGECONFIGURATIONGUI_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * CollageConfigurationGUI class declaration
 */

class CollageConfigurationGUI: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( CollageConfigurationGUI )
    DECLARE_EVENT_TABLE()

public:
	/// Constructors
	CollageConfigurationGUI();
	CollageConfigurationGUI( wxWindow* parent, wxWindowID id = SYMBOL_COLLAGECONFIGURATIONGUI_IDNAME, const wxPoint& pos = SYMBOL_COLLAGECONFIGURATIONGUI_POSITION, const wxSize& size = SYMBOL_COLLAGECONFIGURATIONGUI_SIZE, long style = SYMBOL_COLLAGECONFIGURATIONGUI_STYLE );
	
	/// Creation
	bool Create( wxWindow* parent, wxWindowID id = SYMBOL_COLLAGECONFIGURATIONGUI_IDNAME, const wxPoint& pos = SYMBOL_COLLAGECONFIGURATIONGUI_POSITION, const wxSize& size = SYMBOL_COLLAGECONFIGURATIONGUI_SIZE, long style = SYMBOL_COLLAGECONFIGURATIONGUI_STYLE );
	
	/// Destructor
	~CollageConfigurationGUI();
	
	/// Initialises member variables
	void Init();

	void setFilePin(IInputPin* pin){
		m_pinFile=pin;
	}
	
	void setSensibilityPin(IInputPin* pin){
		m_pinSensibility=pin;
	}
	
	void setDeadZonePin(IInputPin* pin){
		m_pinDeadZone=pin;
	}
	
	void setMaxPin(IInputPin* pin){
		m_pinMaximum=pin; 
	}
	
	void setScenePin(IInputPin* pin){
		m_pinChangeScene=pin; 
	}
	
	void setSpeedAnimationPin(IInputPin* pin){
		m_pinSpeedAnimation=pin; 
	}

	void setVanishPin(IInputPin* pin){
		m_pinVanish=pin;
	}
	
	void InitPanel();

private:	
	/// Creates the controls and sizers
	void CreateControls();
	
	////@begin CollageConfigurationGUI event handler declarations

    /// wxEVT_INIT_DIALOG event handler for ID_COLLAGECONFIGURATION
    void OnInitDialog( wxInitDialogEvent& event );

    /// wxEVT_FILEPICKER_CHANGED event handler for ID_FILEPICKERCTRL
    void OnFilepickerctrlFilePickerChanged( wxFileDirPickerEvent& event );

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_SENS
    void OnSliderSensUpdated( wxCommandEvent& event );

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_DEADZONE
    void OnSliderDeadzoneUpdated( wxCommandEvent& event );

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_MAXIMUM
    void OnSliderMaximumUpdated( wxCommandEvent& event );

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER_SPEED
    void OnSliderSpeedUpdated( wxCommandEvent& event );

    /// wxEVT_COMMAND_CHECKBOX_CLICKED event handler for ID_CHECKBOX_VANISH
    void OnCheckboxVanishClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_PREV
    void OnButtonPrevClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_NEXT
    void OnButtonNextClick( wxCommandEvent& event );

	////@end CollageConfigurationGUI event handler declarations
	
	////@begin CollageConfigurationGUI member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
	////@end CollageConfigurationGUI member function declarations
	
	/// Should we show tooltips?
	static bool ShowToolTips();
	
	////@begin CollageConfigurationGUI member variables
    wxFilePickerCtrl* m_file;
    wxSlider* m_sensibility;
    wxStaticText* m_SensibilityValue;
    wxSlider* m_deadZone;
    wxStaticText* m_DeadZoneValue;
    wxSlider* m_maximum;
    wxStaticText* m_MaximumValue;
    wxSlider* m_speedAnimation;
    wxStaticText* m_SpeedAnimationValue;
    wxCheckBox* m_chkVanish;
	////@end CollageConfigurationGUI member variables

    IInputPin* m_pinFile;
    IInputPin* m_pinSensibility;
    IInputPin* m_pinDeadZone;
    IInputPin* m_pinMaximum;
    IInputPin* m_pinChangeScene;
    IInputPin* m_pinSpeedAnimation;
	IInputPin* m_pinVanish;
};

#endif
    // _CONFIG_GUI_H_
