/////////////////////////////////////////////////////////////////////////////
// Name:        langutils.cpp
// Author:      Cesar Mauri Loba
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "sphost/langutils.h"

#ifdef ENABLE_NLS

#include "sphost/configutils.h"

#include "spcore/coreruntime.h"
#include "spcore/language.h"

#include <wx/choicdlg.h>

using namespace spcore;
using namespace std;

#define LANGUAGE_CONFIG_FILE "language.cfg"

namespace sphost {

bool LanguageSelectionDialog(std::string& lang)
{
	// Let user choose
	wxArrayString languages;
	unsigned int numLangs= spGetAvailableLanguages();
	for (unsigned int i= 0; i< numLangs; ++i) {
		languages.Add(wxString(::spGetLanguageNativeName(i, "C"), wxConvUTF8));
	}

	wxSingleChoiceDialog langDlg(NULL, _T("Language"), _T("Language"), languages);
	if (langDlg.ShowModal()== wxID_OK) {
		lang= spGetLanguageId((unsigned int) langDlg.GetSelection());
		return true;
	}
	
	return false;
}

bool LoadLanguageFromConfiguration(string& lang)
{
	SmartPtr<spcore::IConfiguration> cfg= LoadConfiguration(LANGUAGE_CONFIG_FILE);
	if (!cfg.get()) return false;
	
	const char* langPtr= NULL;
	if (!cfg->ReadString("language", &langPtr)) return false;

	lang= langPtr;
	return true;
}

bool SaveLanguageToConfiguration(const string& lang)
{
	SmartPtr<IConfiguration> cfg= getSpCoreRuntime()->GetConfiguration();
	cfg->WriteString("language", lang.c_str());
	
	return SaveConfiguration(*cfg, LANGUAGE_CONFIG_FILE);
}

};

#endif