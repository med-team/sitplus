/////////////////////////////////////////////////////////////////////////////
// Name:        wwiimotesconfiguration.cpp
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2011 Cesar Mauri Loba - CREA Software Systems
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////

#include "wwiimotesconfiguration.h"
#include "wwiimotesproperties.h"

////@begin includes
////@end includes

#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/icon.h>
#include <wx/bitmap.h>
#include <wx/msgdlg.h>
#include <stdlib.h>

////@begin XPM images
////@end XPM images
namespace mod_wiimotes {

using namespace spcore;

/*!
 * WiimotesConfiguration type definition
 */

IMPLEMENT_DYNAMIC_CLASS( WiimotesConfiguration, wxPanel )

// New event to update GUI when status changed
DECLARE_LOCAL_EVENT_TYPE(wxEVT_REFRESH_STATUS, -1)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_REFRESH_STATUS)



/*!
 * WiimotesConfiguration event table definition
 */

BEGIN_EVENT_TABLE( WiimotesConfiguration, wxPanel )

////@begin WiimotesConfiguration event table entries
    EVT_BUTTON( ID_BUTTON_RECONNECT, WiimotesConfiguration::OnButtonReconnectClick )

////@end WiimotesConfiguration event table entries
	EVT_COMMAND  (wxID_ANY, wxEVT_REFRESH_STATUS, WiimotesConfiguration::StatusNotificationGUI)	
END_EVENT_TABLE()


/*!
 * WiimotesConfiguration constructors
 */

WiimotesConfiguration::WiimotesConfiguration()
{
    Init();
}

WiimotesConfiguration::WiimotesConfiguration( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
    Init();
    Create(parent, id, pos, size, style, name);
}


/*!
 * CCameraConfiguration creator
 */

bool WiimotesConfiguration::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
{
////@begin WiimotesConfiguration creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end WiimotesConfiguration creation
    return true;
}


/*!
 * WiimotesConfiguration destructor
 */

WiimotesConfiguration::~WiimotesConfiguration()
{
	WiiuseThreadController::getInstance()->UnregisterListener(*this);
////@begin WiimotesConfiguration destruction
////@end WiimotesConfiguration destruction	
}


/*!
 * Member initialisation
 */

void WiimotesConfiguration::Init()
{
////@begin WiimotesConfiguration member initialisation
    m_panProperties_1 = NULL;
    m_panProperties_2 = NULL;
    m_panProperties_3 = NULL;
    m_panProperties_4 = NULL;
////@end WiimotesConfiguration member initialisation
	m_sharedStatus= CTypeWiimotesStatus::CreateInstance();
	m_privateStatus= CTypeWiimotesStatus::CreateInstance();
}


/*!
 * Control creation for CCameraConfiguration
 */

void WiimotesConfiguration::CreateControls()
{    
////@begin WiimotesConfiguration content construction
    WiimotesConfiguration* itemPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemPanel1->SetSizer(itemBoxSizer2);

    wxStaticBox* itemStaticBoxSizer3Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Wiimote device 1"));
    wxStaticBoxSizer* itemStaticBoxSizer3 = new wxStaticBoxSizer(itemStaticBoxSizer3Static, wxHORIZONTAL);
    itemBoxSizer2->Add(itemStaticBoxSizer3, 0, wxALIGN_LEFT|wxALL, 5);

    m_panProperties_1 = new Wiimotesproperties;
    m_panProperties_1->Create( itemPanel1, ID_WIIMOTESPROPERTIES1, wxDefaultPosition, wxDefaultSize, 0 );
    itemStaticBoxSizer3->Add(m_panProperties_1, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticBox* itemStaticBoxSizer5Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Wiimote device 2"));
    wxStaticBoxSizer* itemStaticBoxSizer5 = new wxStaticBoxSizer(itemStaticBoxSizer5Static, wxHORIZONTAL);
    itemStaticBoxSizer5Static->Show(false);
    itemBoxSizer2->Add(itemStaticBoxSizer5, 0, wxALIGN_LEFT|wxALL, 5);

    m_panProperties_2 = new Wiimotesproperties;
    m_panProperties_2->Create( itemPanel1, ID_WIIMOTESPROPERTIES2, wxDefaultPosition, wxDefaultSize, 0 );
    m_panProperties_2->Show(false);
    itemStaticBoxSizer5->Add(m_panProperties_2, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticBox* itemStaticBoxSizer7Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Wiimote device 3"));
    wxStaticBoxSizer* itemStaticBoxSizer7 = new wxStaticBoxSizer(itemStaticBoxSizer7Static, wxHORIZONTAL);
    itemStaticBoxSizer7Static->Show(false);
    itemBoxSizer2->Add(itemStaticBoxSizer7, 0, wxALIGN_LEFT|wxALL, 5);

    m_panProperties_3 = new Wiimotesproperties;
    m_panProperties_3->Create( itemPanel1, ID_WIIMOTESPROPERTIES3, wxDefaultPosition, wxDefaultSize, 0 );
    m_panProperties_3->Show(false);
    itemStaticBoxSizer7->Add(m_panProperties_3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxStaticBox* itemStaticBoxSizer9Static = new wxStaticBox(itemPanel1, wxID_ANY, _("Wiimote device 4"));
    wxStaticBoxSizer* itemStaticBoxSizer9 = new wxStaticBoxSizer(itemStaticBoxSizer9Static, wxHORIZONTAL);
    itemStaticBoxSizer9Static->Show(false);
    itemBoxSizer2->Add(itemStaticBoxSizer9, 0, wxALIGN_LEFT|wxALL, 5);

    m_panProperties_4 = new Wiimotesproperties;
    m_panProperties_4->Create( itemPanel1, ID_WIIMOTESPROPERTIES4, wxDefaultPosition, wxDefaultSize, 0 );
    m_panProperties_4->Show(false);
    itemStaticBoxSizer9->Add(m_panProperties_4, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    wxButton* itemButton11 = new wxButton;
    itemButton11->Create( itemPanel1, ID_BUTTON_RECONNECT, _("Reconnect"), wxDefaultPosition, wxDefaultSize, 0 );
    itemBoxSizer2->Add(itemButton11, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);

////@end WiimotesConfiguration content construction

	WiiuseThreadController::getInstance()->RegisterListener(*this, 0, 0);
	WiiuseThreadController::getInstance()->ReqStatus();
}

void WiimotesConfiguration::StatusNotification (const CTypeWiimotesStatus & status)
{
	m_mutex.lock();
	status.Clone (m_sharedStatus.get(), true);
	m_mutex.unlock();

	wxCommandEvent event(wxEVT_REFRESH_STATUS);
	wxPostEvent(this, event);
}

void WiimotesConfiguration::StatusNotificationGUI( wxCommandEvent& WXUNUSED(event) )
{
	m_mutex.lock();
	m_sharedStatus->Clone(m_privateStatus.get(), true);
	m_mutex.unlock();

	m_panProperties_1->Update (*m_privateStatus, 0);
	m_panProperties_2->Update (*m_privateStatus, 1);
	m_panProperties_3->Update (*m_privateStatus, 2);
	m_panProperties_4->Update (*m_privateStatus, 3);	
}

/*!
 * Should we show tooltips?
 */

bool WiimotesConfiguration::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap WiimotesConfiguration::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin WiimotesConfiguration bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end WiimotesConfiguration bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon WiimotesConfiguration::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin WiimotesConfiguration icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end WiimotesConfiguration icon retrieval
}

/*!
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON_RECONNECT
 */

void WiimotesConfiguration::OnButtonReconnectClick( wxCommandEvent& event )
{
	WiiuseThreadController::getInstance()->Reconnect();
    event.Skip(false);
}

}
