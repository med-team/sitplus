/////////////////////////////////////////////////////////////////////////////
// File:        textfiledump.h
// Author:      Cesar Mauri Loba (cesar at crea-si dot com)
// Copyright:   (C) 2010-11 Cesar Mauri Loba - CREA Software Systems
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
/////////////////////////////////////////////////////////////////////////////
#pragma once

#include "spcore/basictypes.h"
#include "spcore/pinimpl.h"
#include "spcore/component.h"
#include <string>

namespace mod_io {

/**
	textfile_dump component

	Dumps the contets of a text file as a string

	Input pins:
		path (string)	
			Path to explore. If path exists the lists of files is dumped.

		refresh (any)
			Re-reads the contents of the file and dumps the result

	Output pins:
		contents (string)		Contents of the text file

	Command line:
		[-p <path>]		Set initial path

	Notes:
*/
class TextFileDumpComponent : public spcore::CComponentAdapter {
public:
	static const char* getTypeName() { return "textfile_dump"; };
	virtual const char* GetTypeName() const { return TextFileDumpComponent::getTypeName(); };

	//
	// Component methods
	//
	TextFileDumpComponent(const char * name, int argc, const char * argv[]);
	
	virtual int DoInitialize();

private:
	// Set a reasonable maximum size
	enum { MAX_FILE_SIZE= 0xFFFFFF };
	//
	// Data members
	//
	SmartPtr<spcore::IOutputPin> m_oPinContents;
	SmartPtr<spcore::CTypeString> m_contents;
	std::string m_path;
	
	//
	// Methods
	//

	void OnPinPath (const spcore::CTypeString & msg);
	void Refresh ();

	// 
	// Input pins classes
	//
	class InputPinPath
		: public spcore::CInputPinWriteOnly<spcore::CTypeString, TextFileDumpComponent> {
	public:
		InputPinPath (TextFileDumpComponent & component)
		: spcore::CInputPinWriteOnly<spcore::CTypeString, TextFileDumpComponent>("path", component) { }

		virtual int DoSend(const spcore::CTypeString & msg) {
			m_component->OnPinPath(msg);
			return 0;
		}
	};

	class InputPinRefresh
		: public spcore::CInputPinWriteOnly<spcore::CTypeAny, TextFileDumpComponent> {
	public:
		InputPinRefresh (TextFileDumpComponent & component)
		: spcore::CInputPinWriteOnly<spcore::CTypeAny, TextFileDumpComponent>("refresh", component) { }

		virtual int DoSend(const spcore::CTypeAny &) {
			m_component->Refresh();
			return 0;
		}
	};

};

// Component factory
typedef spcore::ComponentFactory<TextFileDumpComponent> TextFileDumpComponentFactory;

} // namespace spcore
