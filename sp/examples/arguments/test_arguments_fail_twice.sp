#
# Test and sample on how to use arguments
#

type test_arguments_main
name "test"

import test_arguments.sps

# Error not enough arguments
create test_arguments test_arguments -a "" -b bb -c cc -a fg

