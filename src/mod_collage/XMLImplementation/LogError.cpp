/*
    LogError.cpp -- Log of errors on loading XML file
    Copyright (C) 2011 Marcos Alba Soler (marcos dot alba at estudiants dot urv dot cat)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "InterfaceXML.h"
#include <boost/lexical_cast.hpp>

using std::string;
using boost::shared_ptr;

namespace XMLImplementation{  
	/*
	* Error codes
	* -1 --> warning: Element default missing.
	* -2 --> warning: Attribute lapseAnimation has an invalid value, listSrcBackground ignored.
	* -3 --> warning: Attribute listSrcBackground has no sources, although lapseAnimation has been specified, so no animation.
	* -4 --> warning: Source image of listSrcBackground doesn't exist.
	* -5 --> warning: No modules defined in activity.
	* -6 --> warning: No pictures defined in module.
	* -7 --> warning: Transition type unknown, getting the default if exists.
	* -8 --> warning: Attribute malformed , it should be: position="(x,y)", default position: (0,0).
	* -9 --> warning: Attribute delayType has an invalid value, changed to 'NODELAY'.
	* -10 --> warning: Attribute scale has an invalid value, changed to default (0.25).
	* -11 --> warning: Attribute quantity has an invalid value, changed to default (1).
	* -12 --> error: Requierd attribute missing.
	* -13 --> error: File source doesn't exist.
	* -14 --> error: Element default missing, having pictures without transitions defined.
	* -15 --> error: Attribute moduleType has an invalid value, module ignored.
	* -16 --> fatal error: File configuration doesn't exist.
	* -17 --> fatal error: File configuration malformed.
	* -18 --> fatal error: Unknown error.
	* 
	* */
	
	/*
	 * Constructor of the Error class. Initializes attributes.
	 */
	Error::Error(int code, string element, string attribute, unsigned int line, string message){
		m_code = code;
		m_element = element;
		m_attribute = attribute;
		m_line = line;
		m_message = message;
	}
	/*
	 * destructor from classs Error
	 */
	Error::~Error(){}
	/*
	 * Returns a string representation of the error
	 */
	string Error::toString(){
		string s = m_message;
		string l = boost::lexical_cast<string>(m_line);
		s += " line: ";
		s += l;
		s += " Element: ";
		s += m_element;
		s += " Attributte: ";
		s += m_attribute;
		return s;
	}
	/*
	 * Returns the error code
	 */
	int Error::getCode(){ return m_code;}
	/*
	 * Destructor from class LogError
	 */
	LogError::~LogError(){}
	/*
	 * Adds an Error to LogError
	 */
	void LogError::addError(shared_ptr<Error> err){
		m_errorLog.push_back(err);
	}
	/*
	 * Returns a vector of Error.
	 */
	std::vector<shared_ptr<Error> > LogError::getErrors(){
		return m_errorLog;
	}
	/*
	 * Returns true if there is any fatal error (if there is a fatal error, it will be only one in the first element).
	 * Returns false if there is no fatal errors.
	 */
	bool LogError::hasFatalErrors(){
		return (m_errorLog.size()>0&&m_errorLog[0]->getCode()<-15);
	}
}